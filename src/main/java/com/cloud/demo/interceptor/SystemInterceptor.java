package com.cloud.demo.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class SystemInterceptor extends HandlerInterceptorAdapter {
//	private static Logger logger = LoggerFactory.getLogger(SystemInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
//		BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(),"UTF-8"));
//	    String data = null;
//		StringBuffer input = new StringBuffer();
//		while ((data = br.readLine()) != null) {
//			input.append(data);
//		}
//		br.close();
//		String reqInput=input.toString();
//		logger.info("reqInput:"+reqInput);
		
		return true;
	}
	
	public void postHandle(HttpServletRequest request,HttpServletResponse response,  
	        Object handler,ModelAndView modelAndView)throws Exception{  
	   
	}  
	public void afterCompletion(HttpServletRequest request,  
	        HttpServletResponse response,Object handler,Exception ex)throws Exception{  

	}  
	
}
