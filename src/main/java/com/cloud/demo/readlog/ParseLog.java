/**
 * Project Name:demo
 * File Name:ParseLog.java
 * Package Name:com.cloud.demo.readlog
 * Date:2016年3月18日上午9:26:06
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.readlog;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.print.DocFlavor.STRING;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ClassName:ParseLog <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年3月18日 上午9:26:06 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class ParseLog {
	private static Logger logger = LoggerFactory.getLogger(ParseLog.class);
	public static void readFileByLines(String fileName,String num) {  
        File file = new File(fileName); 
        File filew =new File("E:\\log\\031718\\dataLog.txt");
        BufferedReader reader = null;  
        try {  
            System.out.println("以行为单位读取文件内容，一次读一整行：");  
            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "UTF-8");  
            reader = new BufferedReader(isr);  
            String tempString = null;  
            Map<String, Object> map = new HashMap<String, Object>();  
            // 一次读入一行，直到读入null为文件结束  
            while ((tempString = reader.readLine()) != null) {  
                if (tempString.contains("\"tel\":\""+num+"\",\"stat\":\"")&&tempString.contains("/shzl/report")) {  
                	System.out.println(tempString);
                	String content = tempString.split("msg:")[1];
                	System.out.println(content);
                	FileOutputStream fop = new FileOutputStream(filew,true);
                	byte[] contentInBytes = (content+"\n").getBytes();
                	fop.write(contentInBytes);
                	fop.flush();
                	fop.close();
                	break;
                }
            }  
            reader.close();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            if (reader != null) {  
                try {  
                    reader.close();  
                } catch (IOException e1) {  
                }  
            }  
        }  
    }  
	public static void readFileByLines(String fileName) {  
        File file = new File(fileName); 
        File filew =new File("E:\\log\\031718\\dataLog.txt");
        BufferedReader reader = null;  
        try {  
            System.out.println("以行为单位读取文件内容，一次读一整行：");  
            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "UTF-8");  
            reader = new BufferedReader(isr);  
            String tempString = null;  
//            Map<String, Object> map = new HashMap<String, Object>();  
            // 一次读入一行，直到读入null为文件结束  
            FileOutputStream fop = new FileOutputStream(filew,true);
            while ((tempString = reader.readLine()) != null) {  
                if (tempString.contains("cmpp回响应】tel:")) {  
                	System.out.println(tempString);
//                	String content = tempString.split("msg:")[1];
//                	System.out.println(content);
                	if(tempString.length()>200){
                	byte[] contentInBytes = (tempString+"\n").getBytes();
                	fop.write(contentInBytes);
                	}
                }
            }  
            fop.flush();
        	fop.close();
            reader.close();  

        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            if (reader != null) {  
                try {  
                    reader.close();  
                } catch (IOException e1) {  
                }  
            }  
        }  
    }   
	public static void readLine(String fileName) {
		File file = new File(fileName); 
        File filew =new File("E:\\log\\dataOk.txt");
        File fileLog = new File("E:\\log\\dataLog.txt"); 
        BufferedReader reader = null;  
        try {  
            System.out.println("以行为单位读取文件内容，一次读一整行：");  
            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "UTF-8");  
            reader = new BufferedReader(isr);  
            String tempString = null;  
            FileOutputStream fop = new FileOutputStream(filew,true);
            while ((tempString = reader.readLine()) != null) {  
            	String tel=tempString.split("\"tel\":\"")[1].substring(0,11);
            	System.out.println(tel);
            	String msgId=readMsgIdByLines(fileLog,tel); //找msgId
            	logger.info("tel:"+tel+",msgId:"+msgId);
            	String line=tempString.replaceAll("(?<=msgId)(.*?)(?=tel)", "\":\""+msgId+"\",\"");
            	byte[] contentInBytes = (line+"\n").getBytes();
            	fop.write(contentInBytes);
            }  
            fop.flush();
        	fop.close();
            reader.close();  

        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            if (reader != null) {  
                try {  
                    reader.close();  
                } catch (IOException e1) {  
                }  
            }  
        }  
	}
	public static String readMsgIdByLines(File file ,String num) {  
	       boolean flag=false;
	       BufferedReader reader =null;
	       String msgId =null;
	        try {  
	        	 InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "UTF-8");  
	        	  reader = new BufferedReader(isr); 
	            String tempString = null;  
	            // 一次读入一行，直到读入null为文件结束  
	            while ((tempString = reader.readLine()) != null) {  
	                if (tempString.contains(num)) {  
	                	logger.info(tempString);
	                	msgId = tempString.split("msgId\":\"")[1];
	                	
	                	flag=true;
	                	break;
	                }
	            }  
	            if(!flag){
	            	logger.warn("未找到"+num);
	            }
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        } finally {  
	            if (reader != null) {  
	                try {  
	                    reader.close();  
	                } catch (IOException e1) {  
	                }  
	            }  
	        }  
	        return msgId ;
	    }
	public static void readSend() {  
        File file = new File("E:\\log\\dataOk.txt"); 
        BufferedReader reader = null;  
        try {  
            System.out.println("以行为单位读取文件内容，一次读一整行：");  
            InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "UTF-8");  
            reader = new BufferedReader(isr);  
            String tempString = null;  
            while ((tempString = reader.readLine()) != null) {  
//            	HttpClient.postDataJson("http://223.6.255.104:4100/sms/notify/shzl/report", tempString,"cmpp");
            	HttpClient.postDataJson("http://192.168.178.62:8060/ccopsms/sms/notify/shzl/report", tempString,"cmpp");
            }  
            reader.close();  

        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            if (reader != null) {  
                try {  
                    reader.close();  
                } catch (IOException e1) {  
                }  
            }  
        }  
    } 
	public static void main(String[] args) {  
//        ParseLog.readLine("E:\\log\\data.txt");
        ParseLog.readSend();
//		String s="2016-03-17 18:33:42.764 [http-bio-9000-exec-40] INFO  c.ccop.sgw.controller.CmppController - 【cmpp回响应】tel:18245129311,rs:{\"result\":0,\"msgId\":\"4092112349874159600\"}";
//		System.out.println(s.length());
//		String msgId="abc";
//		String tempString="{\"type\":1,\"msgId\":\"4092151107658978077\",\"tel\":\"15857697416\",\"stat\":\"DELIVRD\",\"submitTime\":\"1603171835\",\"doneTime\":\"160317183558\"}";
//		String line=tempString.replaceAll("(?<=msgId)(.*?)(?=tel)", "\":\""+msgId+"\",\"");
//		System.out.println(line);
	}  
	
}

