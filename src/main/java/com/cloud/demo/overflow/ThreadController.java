/**
 * Project Name:demo
 * File Name:ThreadController.java
 * Package Name:com.cloud.demo.overflow
 * Date:2016年3月9日下午2:31:03
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.overflow;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cloud.demo.bean.TestSendMsgReq;
import com.cloud.demo.httpclient.HttpClient;
import com.cloud.demo.httpclient.Send;
import com.cloud.demo.httpclient.Send1;
import com.cloud.demo.thread.SpringThreadPool;
import com.cloud.demo.utils.DateUtils;
import com.cloud.demo.utils.Encrypt;
import com.cloud.demo.utils.JacksonUtil;

/**
 * ClassName:ThreadController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年3月9日 下午2:31:03 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class ThreadController {
	private static Logger logger = LoggerFactory.getLogger(ThreadController.class);
	
    // 模拟并发访问控制  
    private static void simulateAccess(final int m, final int n,final SpringThreadPool tp)  
            throws Exception { // m : 线程数；n : 调用数  
        /*ExecutorService pool = Executors.newFixedThreadPool(100);  
        for (int i = m; i > 0; i--) {  
            final int x = i;  
            pool.submit(new Runnable() {  
                @Override  
                public void run() {  
                	long start=System.currentTimeMillis();
                    for (int j = n; j > 0; j--) { 
//                    	logger.debug("j"+j);
//                        try {  
//                            Thread.sleep(5);  
//                        } catch (InterruptedException e) {  
//                            e.printStackTrace();  
//                        }  
                       Send.testSmpp();
                    }  
                    long end=System.currentTimeMillis();
                    System.out.println("cost total time:"+(end-start));
                }  
            });  
        }  
  
        pool.shutdown();  
        pool.awaitTermination(1, TimeUnit.HOURS);  //当执行线程中断、超时，或调用了shutdown方法后，阻塞直到所有的Task都执行结束
        logger.info("完成");*/
    	ScheduledExecutorService pool = Executors.newScheduledThreadPool(1);
    	final AtomicInteger i=new AtomicInteger(0);
    	pool.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				i.incrementAndGet();
				long start=System.currentTimeMillis();
                for (int j = 20; j > 0; j--) { 
//                	logger.debug("j"+j);
//                    try {  
//                        Thread.sleep(5);  
//                    } catch (InterruptedException e) {  
//                        e.printStackTrace();  
//                    }  
                   Send1.testSmpp();
                }  
                long end=System.currentTimeMillis();
                System.out.println("cost total time:"+(end-start));
			}
        	
        }, 0, 1, TimeUnit.SECONDS);
    	while(true) {
    		if(i.get()==1) {
    			pool.shutdown();
    			break;
    		}
    	}
    	//pool.shutdown();  
    	pool.awaitTermination(1, TimeUnit.HOURS);
    }  
  
    public static void main(String[] args) throws Exception {  
          
        // 开始模拟lots of concurrent calls: 100 * 1000  
         
//        ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");     
//	    SpringThreadPool tp = (SpringThreadPool)appContext.getBean("springThread");  
//	    tp.releaseWork();  
	    simulateAccess(1,20,null); 
    }  
}

