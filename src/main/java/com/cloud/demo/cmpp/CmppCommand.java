package com.cloud.demo.cmpp;

public class CmppCommand {
	public static final int PeripheralID=5000;
	
	/**
	 * 包头长度。（协议采用定长包头，长度为4+4+4)
	 * 
     *Total_Length	4	Unsigned Integer	消息总长度(含消息头及消息体)
     *Command_Id	4	Unsigned Integer	命令或响应类型
     *Sequence_Id	4	Unsigned Integer	消息流水号,顺序累加,步长为1,循环使用（一对请求和应答消息的流水号必须相同）
	 */
	public static final int MsgHeaderLength = 12;
	public static final int MsgTotalLength=4;
	public static final int MsgCommandId=4;
	public static final int MsgSequenceId=4;
	
	public static final int CMPP_CONNECT=0x00000001;//CMPP_CONNECT请求连接
	public static final int CMPP_CONNECT_RESP=0x80000001;//请求连接应答
	public static final int CMPP_TERMINATE=0x00000002;//	终止连接
	public static final int CMPP_TERMINATE_RESP=0x80000002;	//终止连接应答
	public static final int CMPP_SUBMIT=0x00000004;	//提交短信
	public static final int CMPP_SUBMIT_RESP=0x80000004;	//提交短信应答
	public static final int CMPP_DELIVER=0x00000005;	//短信下发
	public static final int CMPP_DELIVER_RESP=0x80000005;	//下发短信应答
	public static final int CMPP_QUERY=0x00000006;	//发送短信状态查询
	public static final int CMPP_QUERY_RESP=0x80000006;	//发送短信状态查询应答
	public static final int CMPP_CANCEL=0x00000007;	//删除短信
	public static final int CMPP_CANCEL_RESP=0x80000007;	//删除短信应答
	public static final int CMPP_ACTIVE_TEST=0x00000008;	//激活测试
	public static final int CMPP_ACTIVE_TEST_RESP=0x80000008;	//激活测试应答
}
