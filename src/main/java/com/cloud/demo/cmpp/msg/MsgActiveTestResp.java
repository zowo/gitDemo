package com.cloud.demo.cmpp.msg;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * ClassName: MsgActiveTestResp <br/>
 * Function: 链路检查消息结构定义 <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2016年2月23日 下午6:46:12 <br/>
 *
 * @author LiHao
 * @version 
 * @since JDK 1.6
 */
public class MsgActiveTestResp extends MsgHead {
	private static Logger logger=Logger.getLogger(MsgActiveTestResp.class);
	private byte reserved;//
	public MsgActiveTestResp(int totalLength,int commandId,int sequenceId,byte[] data){
		if(data.length==1){
			ByteArrayInputStream bins=new ByteArrayInputStream(data);
			DataInputStream dins=new DataInputStream(bins);
			try {
				this.setTotalLength(totalLength);
				this.setCommandId(commandId);
				this.setSequenceId(sequenceId);
				this.reserved=dins.readByte();
				dins.close();
				bins.close();
			} catch (IOException e){}
		}else{
			logger.info("链路检查,解析数据包出错，包长度不一致。长度为:"+data.length);
		}
	}
	public byte getReserved() {
		return reserved;
	}

	public void setReserved(byte reserved) {
		this.reserved = reserved;
	}
}
