package com.cloud.demo.cmpp.msg;

import com.cloud.demo.utils.ByteConvert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * ClassName: MsgDeliver <br/>
 * Function: CMPP_DELIVER操作的目的是ISMG把从短信中心或其它ISMG转发来的短信送交SP，SP以CMPP_DELIVER_RESP消息回应。 <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2016年2月23日 下午6:47:04 <br/>
 *
 * @author LiHao
 * @version 
 * @since JDK 1.6
 */
public class MsgDeliver2 extends MsgHead {
	private static Logger logger = LoggerFactory.getLogger(MsgDeliver2.class);
	private long msg_Id;
	private String dest_Id;//21 目的号码 String
	private String service_Id;//10 业务标识  String
	private byte tP_pid = 0;
	private byte tP_udhi = 0;
	private byte msg_Fmt = 15;
	private String src_terminal_Id;//源终端MSISDN号码
	private byte src_terminal_type = 0;//源终端号码类型，0：真实号码；1：伪码
	private byte registered_Delivery = 0;//是否为状态报告 0：非状态报告1：状态报告
	private int msg_Length;//消息长度
	private String msg_Content;//消息长度
	private String linkID;

	private long msg_Id_report;
	private String stat;
	private String submit_time;
	private String done_time;
	private String dest_terminal_Id;
	private int sMSC_sequence;
	private int result;//解析结果
	public MsgDeliver2(int totalLength, int commandId, int sequenceId, byte[] data){
		if(data.length>8+21+10+1+1+1+21+1+1+8){//+Msg_length+
			String fmtStr="gb2312";
			try {
				this.setTotalLength(totalLength);
				this.setCommandId(commandId);
				this.setSequenceId(sequenceId);
				byte[] msgIdByte=new byte[8];
				System.arraycopy(data,0,msgIdByte,0,8);
				this.msg_Id=ByteConvert.bytesToLong(msgIdByte);//Msg_Id
				byte[] destIdByte=new byte[21];
				System.arraycopy(data,8,destIdByte,0,21);
				this.dest_Id=new String(destIdByte);//21 目的号码 String 
				byte[] service_IdByte=new byte[10];
				System.arraycopy(data,29,service_IdByte,0,10);
				this.service_Id=new String(service_IdByte);//10 业务标识  String 
				this.tP_pid =data[39];
				this.tP_udhi =data[40];
				this.msg_Fmt = data[41];
				fmtStr=this.msg_Fmt==8?"UTF-16BE":"gb2312";
				byte[] src_terminal_IdByte=new byte[21];
				System.arraycopy(data,42,src_terminal_IdByte,0,21);
				this.src_terminal_Id=new String(src_terminal_IdByte);//源终端MSISDN号码
				logger.debug("src_terminal_Id:"+src_terminal_Id);
				//this.src_terminal_type = data[74];//源终端号码类型，0：真实号码；1：伪码
				this.registered_Delivery =data[63];//是否为状态报告 0：非状态报告1：状态报告
				System.out.println(data[63]);
				this.msg_Length=ByteConvert.oneByte2Int(data[64]);//消息长度
				byte[] msg_ContentByte=new byte[msg_Length];
				System.arraycopy(data,65,msg_ContentByte,0,msg_Length);
				if(registered_Delivery==1){//状态报告
//					this.msg_Content=new String(msg_ContentByte,fmtStr);//消息长度
					if(msg_Length==8+7+10+10+21+4){
						byte[] msgIdReportByte=new byte[8];
						System.arraycopy(msg_ContentByte,0,msgIdReportByte,0,8);
//						String s=new String(msgIdReportByte,fmtStr);
//						System.out.println("s"+s);
						this.msg_Id_report=ByteConvert.bytesToLong(msgIdReportByte);
						byte[] statByte=new byte[7];
						System.arraycopy(msg_ContentByte,8,statByte,0,7);
						this.stat=new String(statByte,fmtStr);
						byte[] submit_timeByte=new byte[10];
						System.arraycopy(msg_ContentByte,15,submit_timeByte,0,10);
						this.submit_time=new String(submit_timeByte,fmtStr);
						byte[] done_timeByte=new byte[10];
						System.arraycopy(msg_ContentByte,25,done_timeByte,0,10);
						this.done_time=new String(done_timeByte,fmtStr);
						byte[] dest_terminal_IdByte=new byte[21];
						System.arraycopy(msg_ContentByte,35,dest_terminal_IdByte,0,21);
						this.dest_terminal_Id=new String(dest_terminal_IdByte,fmtStr);
						byte[] sMSC_sequenceByte=new byte[4];
						System.arraycopy(msg_ContentByte,56,sMSC_sequenceByte,0,4);
//						this.sMSC_sequence=dinsC.readInt();
//						dinsC.close();
//						binsC.close();
						this.result=0;//正确
					}else{
						logger.debug("cmpp消息结构错,msg_Length:"+msg_Length);
						this.result=1;//消息结构错
					}
				}else{
					logger.debug("上行内容解析,msg_Fmt:{},fmtStr:{}",new Object[]{msg_Fmt,fmtStr});
					if(msg_Fmt==8){
						this.msg_Content=new String(msg_ContentByte, "UTF-16BE");
					}else{
						this.msg_Content=new String(msg_ContentByte,fmtStr);//消息长度
					}
				}
				byte[] linkIDByte=new byte[8];
				this.linkID=new String(linkIDByte);
				this.result=0;//正确
			} catch (Exception e){
				this.result=8;//消息结构错
				logger.error("CMPP_DELIVER,解析数据包出错,msg_Fmt:"+msg_Fmt+",msg_Length:"+msg_Length+",registered_Delivery:"+registered_Delivery);
				e.printStackTrace();
			}
		}else{
			this.result=1;//消息结构错
			logger.info("短信网关CMPP_DELIVER,解析数据包出错，包长度不一致。长度为:"+data.length);
		}
	}
	public long getMsg_Id() {
		return msg_Id;
	}

	public void setMsg_Id(long msg_Id) {
		this.msg_Id = msg_Id;
	}

	public String getDest_Id() {
		return dest_Id;
	}

	public void setDest_Id(String dest_Id) {
		this.dest_Id = dest_Id;
	}

	public String getService_Id() {
		return service_Id;
	}

	public void setService_Id(String service_Id) {
		this.service_Id = service_Id;
	}

	public byte getTP_pid() {
		return tP_pid;
	}

	public void setTP_pid(byte tp_pid) {
		tP_pid = tp_pid;
	}

	public byte getTP_udhi() {
		return tP_udhi;
	}

	public void setTP_udhi(byte tp_udhi) {
		tP_udhi = tp_udhi;
	}

	public byte getMsg_Fmt() {
		return msg_Fmt;
	}

	public void setMsg_Fmt(byte msg_Fmt) {
		this.msg_Fmt = msg_Fmt;
	}

	public String getSrc_terminal_Id() {
		return src_terminal_Id;
	}

	public void setSrc_terminal_Id(String src_terminal_Id) {
		this.src_terminal_Id = src_terminal_Id;
	}

	public byte getSrc_terminal_type() {
		return src_terminal_type;
	}

	public void setSrc_terminal_type(byte src_terminal_type) {
		this.src_terminal_type = src_terminal_type;
	}

	public byte getRegistered_Delivery() {
		return registered_Delivery;
	}

	public void setRegistered_Delivery(byte registered_Delivery) {
		this.registered_Delivery = registered_Delivery;
	}

	public int getMsg_Length() {
		return msg_Length;
	}

	public void setMsg_Length(int msg_Length) {
		this.msg_Length = msg_Length;
	}

	public String getMsg_Content() {
		return msg_Content;
	}

	public void setMsg_Content(String msg_Content) {
		this.msg_Content = msg_Content;
	}

	public String getLinkID() {
		return linkID;
	}

	public void setLinkID(String linkID) {
		this.linkID = linkID;
	}

	public long getMsg_Id_report() {
		return msg_Id_report;
	}

	public void setMsg_Id_report(long msg_Id_report) {
		this.msg_Id_report = msg_Id_report;
	}

	public String getStat() {
		return stat;
	}

	public void setStat(String stat) {
		this.stat = stat;
	}

	public String getSubmit_time() {
		return submit_time;
	}

	public void setSubmit_time(String submit_time) {
		this.submit_time = submit_time;
	}

	public String getDone_time() {
		return done_time;
	}

	public void setDone_time(String done_time) {
		this.done_time = done_time;
	}

	public String getDest_terminal_Id() {
		return dest_terminal_Id;
	}

	public void setDest_terminal_Id(String dest_terminal_Id) {
		this.dest_terminal_Id = dest_terminal_Id;
	}

	public int getSMSC_sequence() {
		return sMSC_sequence;
	}

	public void setSMSC_sequence(int smsc_sequence) {
		sMSC_sequence = smsc_sequence;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public static void main(String[] args) {
		byte bodyBytes[]=new byte[]{(byte)0x4d,(byte)0xcd,(byte)0xc8,(byte)0x01,
				   (byte)0x2d ,(byte)0x32 ,(byte)0x42 ,(byte)0xa9 ,(byte)0x31 ,(byte)0x30 ,(byte)0x36 ,(byte)0x39 ,(byte)0x30 ,(byte)0x37 ,(byte)0x39 ,(byte)0x33 ,(byte)0x31 ,(byte)0x31 ,(byte)0x30 ,(byte)0x38, 
				   (byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x4d ,(byte)0x53 ,(byte)0x48 ,(byte)0x36 ,(byte)0x39 ,(byte)0x31 ,(byte)0x30, 
				   (byte)0x31 ,(byte)0x30 ,(byte)0x30 ,(byte)0x00 ,(byte)0x40 ,(byte)0x08 ,(byte)0x38 ,(byte)0x36 ,(byte)0x31 ,(byte)0x38 ,(byte)0x37 ,(byte)0x30 ,(byte)0x37 ,(byte)0x38 ,(byte)0x36 ,(byte)0x37, 
				   (byte)0x39 ,(byte)0x30 ,(byte)0x32 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00, 
				   (byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x8c ,(byte)0x05 ,(byte)0x00 ,(byte)0x03 ,(byte)0xbc ,(byte)0x0c ,(byte)0x05 ,(byte)0x67, 
				   (byte)0x00 ,(byte)0x7f ,(byte)0x8e ,(byte)0x4e ,(byte)0x3d ,(byte)0x76 ,(byte)0x84 ,(byte)0x4e ,(byte)0xba ,(byte)0x30 ,(byte)0x02 ,(byte)0x00 ,(byte)0x0a ,(byte)0x53 ,(byte)0xef ,(byte)0x66, 
				   (byte)0x2f ,(byte)0x67 ,(byte)0x09 ,(byte)0x4e ,(byte)0x00 ,(byte)0x59 ,(byte)0x29 ,(byte)0xff ,(byte)0x0c ,(byte)0x59 ,(byte)0x79 ,(byte)0x4e ,(byte)0xec ,(byte)0x4e ,(byte)0x24 ,(byte)0x4e, 
				   (byte)0x2a ,(byte)0x53 ,(byte)0xc8 ,(byte)0x51 ,(byte)0x8d ,(byte)0x6b ,(byte)0x21 ,(byte)0x89 ,(byte)0xc1 ,(byte)0x97 ,(byte)0x62 ,(byte)0x4e ,(byte)0x86 ,(byte)0xff ,(byte)0x0c ,(byte)0x57, 
				   (byte)0x28 ,(byte)0x88 ,(byte)0x57 ,(byte)0x89 ,(byte)0xd2 ,(byte)0xff ,(byte)0x0c ,(byte)0x65 ,(byte)0xc1 ,(byte)0x8f ,(byte)0xb9 ,(byte)0x67 ,(byte)0x09 ,(byte)0x4e ,(byte)0x00 ,(byte)0x4f, 
				   (byte)0x4d ,(byte)0x5b ,(byte)0x64 ,(byte)0x5b ,(byte)0xe1 ,(byte)0x80 ,(byte)0x01 ,(byte)0x4e ,(byte)0xba ,(byte)0xff ,(byte)0x0c ,(byte)0x5d ,(byte)0xf2 ,(byte)0x7e ,(byte)0xcf ,(byte)0x59, 
				   (byte)0x7d ,(byte)0x51 ,(byte)0xe0 ,(byte)0x59 ,(byte)0x29 ,(byte)0x6c ,(byte)0xa1 ,(byte)0x54 ,(byte)0x03 ,(byte)0x4e ,(byte)0x1c ,(byte)0x89 ,(byte)0x7f ,(byte)0x4e ,(byte)0x86 ,(byte)0xff, 
				   (byte)0x0c ,(byte)0x80 ,(byte)0x01 ,(byte)0x4e ,(byte)0xba ,(byte)0x72 ,(byte)0x2c ,(byte)0x8f ,(byte)0xc7 ,(byte)0x67 ,(byte)0x65 ,(byte)0x62 ,(byte)0xfd ,(byte)0x4f ,(byte)0x4f ,(byte)0x90, 
				   (byte)0xa3 ,(byte)0x4e ,(byte)0x2a ,(byte)0x7f ,(byte)0x8e ,(byte)0x82 ,(byte)0xe5 ,(byte)0x59 ,(byte)0x29 ,(byte)0x4e ,(byte)0xd9 ,(byte)0x59 ,(byte)0x73 ,(byte)0x4e ,(byte)0xba ,(byte)0x76, 
				   (byte)0x84 ,(byte)0x88 ,(byte)0x63 ,(byte)0x67 ,(byte)0x0d ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00, 
				   (byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 
				};
		MsgDeliver2 m =new MsgDeliver2(249, 5, 137569171,bodyBytes);
	}
}
