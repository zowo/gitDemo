package com.cloud.demo.cmpp.msg;

import com.cloud.demo.utils.ByteConvert;
import org.apache.log4j.Logger;


/**
 * ClassName: MsgSubmitResp <br/>
 * Function: Submit消息结构应答定义 <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2016年2月23日 下午6:48:22 <br/>
 *
 * @author LiHao
 * @version 
 * @since JDK 1.6
 */
public class MsgSubmitResp2 extends MsgHead {
	private static Logger logger=Logger.getLogger(MsgSubmitResp2.class);
	private long msgId;
	private int result;//结果 0：正确 1：消息结构错 2：命令字错 3：消息序号重复 4：消息长度错 5：资费代码错 6：超过最大信息长 7：业务代码错 8：流量控制错 9：本网关不负责服务此计费号码 10：Src_Id错误 11：Msg_src错误 12：Fee_terminal_Id错误 13：Dest_terminal_Id错误
	public MsgSubmitResp2(int totalLength, int commandId, int sequenceId, byte[] data){
		if(data.length==8+1){
				this.setTotalLength(totalLength);
				this.setCommandId(commandId);
				this.setSequenceId(sequenceId);
//				this.msgId=dins.readLong();
//				this.result=dins.readInt();
//				dins.close();
//				bins.close();
				byte[] msgIdByte=new byte[8];
				System.arraycopy(data,0,msgIdByte,0,8);
				byte[] resultByte=new byte[1];
				System.arraycopy(data,8,resultByte,0,1);
				this.msgId=ByteConvert.bytesToLong(msgIdByte);
				this.result=ByteConvert.oneByte2Int(resultByte[0]);
			
		}else{
			logger.info("发送短信IMSP回复,解析数据包出错，包长度不一致。长度为:"+data.length);
		}
	}
	public long getMsgId() {
		return msgId;
	}
	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
}
