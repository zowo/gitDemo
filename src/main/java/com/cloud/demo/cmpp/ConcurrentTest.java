/**
 * Project Name:ccopsms
 * File Name:ConcurrentTest.java
 * Package Name:com.ccop.common.cmpp
 * Date:2016年2月18日上午11:50:31
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.cmpp;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import com.cloud.demo.cmppsh.socket.CmppShContainer;

/**
 * ClassName:ConcurrentTest <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON. <br/>
 * Date: 2016年2月18日 上午11:50:31 <br/>
 * 
 * @author LiHao
 * @version
 * @since JDK 1.6
 * @see
 */
public class ConcurrentTest {
	private static int thread_num = 10;
	private static int client_num = 500;

	public static void main(String[] args) {
		CmppShContainer.getInstance();
		ExecutorService exec = Executors.newCachedThreadPool();
		final Semaphore semp = new Semaphore(thread_num);
		for (int index = 0; index < client_num; index++) {
			final int NO = index;
			Runnable run = new Runnable() {
				public void run() {
					try {
						semp.acquire();
						System.out.println("Thread:" + NO);
//						Container.sendMsg("测试", "1352000731"+NO,"");
						// 业务逻辑
						semp.release();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			exec.execute(run);
		}
		
		try {
			Thread.sleep(300000);
		} catch (InterruptedException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		exec.shutdown();
	}
}
