package com.cloud.demo.smgp.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.cloud.demo.utils.ByteConvert;
import com.cloud.demo.utils.MsgUtils;



/**
 * ClassName: MsgSubmitResp <br/>
 * Function: Submit消息结构应答定义 <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2016年2月23日 下午6:48:22 <br/>
 *
 * @author LiHao
 * @version 
 * @since JDK 1.6
 */
public class SmgpMsgSubmitResp extends SmgpMsgHead {
	private static Logger logger=Logger.getLogger(SmgpMsgSubmitResp.class);
	private String msgId;
	private int status;//结果 0：正确 1：消息结构错 2：命令字错 3：消息序号重复 4：消息长度错 5：资费代码错 6：超过最大信息长 7：业务代码错 8：流量控制错 9：本网关不负责服务此计费号码 10：Src_Id错误 11：Msg_src错误 12：Fee_terminal_Id错误 13：Dest_terminal_Id错误
	public SmgpMsgSubmitResp(){
		
	}
	public SmgpMsgSubmitResp(int packetLength,int requestId,int sequenceId,byte[] data){
		if(data.length==10+4){
				this.setPacketLength(packetLength);
				this.setRequestId(requestId);
				this.setSequenceId(sequenceId);

				byte[] msgIdByte=new byte[10];
				System.arraycopy(data,0,msgIdByte,0,10);
				byte[] resultByte=new byte[4];
				System.arraycopy(data,10,resultByte,0,4);
				this.msgId=ByteConvert.getHexStr(msgIdByte);
				this.status=ByteConvert.bytesToInt(resultByte);
			
		}else{
			logger.info("发送短信回复,解析数据包出错，包长度不一致。长度为:"+data.length);
		}
	}
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			dous.writeInt(this.getPacketLength());
			dous.writeInt(this.getRequestId());
			dous.writeInt(this.getSequenceId());
			MsgUtils.writeString(dous,this.msgId,10);
			dous.writeInt(status);
			dous.close();
		} catch (IOException e) {
			logger.error("封装链接二进制数组失败。");
			e.printStackTrace();
		}
		return bous.toByteArray();
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
