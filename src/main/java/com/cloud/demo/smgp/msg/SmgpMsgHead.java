package com.cloud.demo.smgp.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * ClassName: MsgHead <br/>
 * Function: 所有请求的消息头 <br/>
 * Unsigned Integer  	无符号整数<br/>
 * Integer	整数，可为正整数、负整数或零<br/>
 * Octet String	定长字符串，位数不足时，如果左补0则补ASCII表示的零以填充，如果右补0则补二进制的零以表示字符串的结束符
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2016年2月18日 下午5:22:41 <br/>
 * 
 * @author LiHao
 * @version 
 * @since JDK 1.6
 */
public class SmgpMsgHead {
	private Logger logger=Logger.getLogger(SmgpMsgHead.class);
	private int packetLength;//指包头和包体的长度之和。单位是“字节”
	private int requestId;//请求标识表示SMGP数据包的类型
	private int sequenceId;//取值范围为0x00000000－0xFFFFFFFF，顺序累加（一对请求和应答消息的流水号必须相同）
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			dous.writeInt(this.getPacketLength());
			dous.writeInt(this.getRequestId());
			dous.writeInt(this.getSequenceId());
			dous.close();
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("【封装SMGP消息头二进制数组失败】");
		}
		return bous.toByteArray();
	}

	public SmgpMsgHead(){
		super();
	}

	/**
	 * packetLength.
	 *
	 * @return  the packetLength
	 * @since   JDK 1.6
	 */
	public int getPacketLength() {
		return packetLength;
	}

	/**
	 * requestId.
	 *
	 * @return  the requestId
	 * @since   JDK 1.6
	 */
	public int getRequestId() {
		return requestId;
	}

	/**
	 * sequenceId.
	 *
	 * @return  the sequenceId
	 * @since   JDK 1.6
	 */
	public int getSequenceId() {
		return sequenceId;
	}

	/**
	 * packetLength.
	 *
	 * @param   packetLength    the packetLength to set
	 * @since   JDK 1.6
	 */
	public void setPacketLength(int packetLength) {
		this.packetLength = packetLength;
	}

	/**
	 * requestId.
	 *
	 * @param   requestId    the requestId to set
	 * @since   JDK 1.6
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	/**
	 * sequenceId.
	 *
	 * @param   sequenceId    the sequenceId to set
	 * @since   JDK 1.6
	 */
	public void setSequenceId(int sequenceId) {
		this.sequenceId = sequenceId;
	}
}
