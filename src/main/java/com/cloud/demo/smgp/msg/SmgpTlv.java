/**
 * Project Name:ccopsgw
 * File Name:SmgpTlv.java
 * Package Name:com.ccop.common.smgp.msg
 * Date:2016年2月27日下午6:39:41
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.smgp.msg;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloud.demo.utils.ByteConvert;


/**
 * ClassName:SmgpTlv <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON. <br/>
 * Date: 2016年2月27日 下午6:39:41 <br/>
 * 
 * @author LiHao
 * @version
 * @since JDK 1.6
 * @see
 */
public class SmgpTlv {
	private static Logger logger = LoggerFactory.getLogger(SmgpTlv.class);
	public byte cPid; // PID
	public boolean b_cPid = false; // cPid是否使用
	public byte cUdhi; // UDHI
	public boolean b_cUdhi = false; // cUdhi是否使用
	public String strLinkId; // DSMP的LinkId
	public boolean b_strLinkId = false; //
	public byte cFeeFlag; // 计费方式
	public boolean b_cFeeFlag = false;
	// 0：对目的终端MSISDN计费
	// 1：对源终端MSISDN计费
	// 2：对SP计费
	// 3：表示本字段无效，对谁计费参见被计费用户的号码
	public byte cFeeMaskFlag; // 号码类型
	public boolean b_cFeeMaskFlag = false;
	// 计费用户的号码类型。
	// 0＝真实号码；
	// 1＝伪码；
	// 其它保留
	public String strFeeNumberMask; // 计费号码伪码
	public boolean b_strFeeNumberMask = false;
	public byte cDestMaskFlag; // 号码类型
	public boolean b_cDestMaskFlag = false;
	// 目的用户的号码类型。
	// 0＝真实号码；
	// 1＝伪码；
	// 其它保留
	public String strDestNumberMask; // 目的号码伪码
	public boolean b_strDestNumberMask = false;
	public byte cPkTotal; // SP协议版本
	public boolean b_cPkTotal = false;
	public byte cPkNumber; // SP协议版本
	public boolean b_cPkNumber = false;
	public byte cMsgType;
	public boolean b_cMsgType = false;
	public byte cSpDealResult;
	public boolean b_cSpDealResult = false;
	public byte cSrcMaskFlag; // 号码类型
	public boolean b_cSrcMaskFlag = false;
	// 源用户的号码类型。
	// 0＝真实号码；
	// 1＝伪码；
	// 其它保留
	public String strSrcNumberMask; // 源号码伪码
	public boolean b_strSrcNumberMask = false;
	public byte cNodesCount;
	public boolean b_cNodesCount = false;
	public String strMsgSrc; // 信息来源164
	public boolean b_strMsgSrc = false;
	public byte cSpMaskFlag;
	public boolean b_cSpMaskFlag = false;
//	public byte cMServiceID;
	public boolean b_cMServiceID = false;
	public String strMServiceID;

	public byte[] getBytes() {

		ArrayList alist = new ArrayList();
		int pos = 0;
//6.3.2	TP_pid
		if (b_cPid) {
			byte[] tempBuf = new byte[5];
			short tag = 0x001;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			tempBuf[4] = cPid;

			alist.add(tempBuf);
		}
//6.3.3	TP_udhi
		if (b_cUdhi) {
			byte[] tempBuf = new byte[5];
			short tag = 0x002;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			tempBuf[4] = cUdhi;
			alist.add(tempBuf);
		}
//6.3.4	LinkID
		if (b_strLinkId) {
			byte[] tempBuf = new byte[4 + 20];
			short tag = 0x003;
			short length = 20;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			System.arraycopy(strLinkId.getBytes(), 0, tempBuf, 4, strLinkId.getBytes().length);
			alist.add(tempBuf);
		}

		if (b_cFeeFlag) {
			byte[] tempBuf = new byte[5];
			short tag = 0x004;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			tempBuf[4] = cFeeFlag;
			alist.add(tempBuf);
		}

		if (b_cFeeMaskFlag) {
			byte[] tempBuf = new byte[5];
			short tag = 0x005;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			tempBuf[4] = cFeeMaskFlag;
			alist.add(tempBuf);
		}

		if (b_strFeeNumberMask) {
			byte[] tempBuf = new byte[4 + 32];
			short tag = 0x006;
			short length = 32;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			System.arraycopy(strFeeNumberMask.getBytes(), 0, tempBuf, 4, strFeeNumberMask.getBytes().length);
			alist.add(tempBuf);
		}

		if (b_cDestMaskFlag) {
			byte[] tempBuf = new byte[5];
			short tag = 0x007;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			tempBuf[4] = cDestMaskFlag;
			alist.add(tempBuf);
		}

		if (b_strDestNumberMask) {
			byte[] tempBuf = new byte[4 + 32];
			short tag = 0x008;
			short length = 32;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			System.arraycopy(strDestNumberMask.getBytes(), 0, tempBuf, 4, strDestNumberMask.getBytes().length);
			alist.add(tempBuf);
		}
//6.3.10	PkTotal
		if (b_cPkTotal) {
			byte[] tempBuf = new byte[5];
			short tag = 0x009;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			tempBuf[4] = cPkTotal;
			alist.add(tempBuf);
		}
//6.3.11	PkNumber
		if (b_cPkNumber) {
			byte[] tempBuf = new byte[5];
			short tag = 0x00a;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			tempBuf[4] = cPkNumber;
			alist.add(tempBuf);
		}
//
		if (b_cMsgType) {
			byte[] tempBuf = new byte[5];
			short tag = 0x00b;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			tempBuf[4] = cMsgType;
			alist.add(tempBuf);
		}
//6.3.13	SPDealResult
		if (b_cSpDealResult) {
			byte[] tempBuf = new byte[5];
			short tag = 0x00c;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			tempBuf[4] = cSpDealResult;
			alist.add(tempBuf);
		}

		if (b_cSrcMaskFlag) {
			byte[] tempBuf = new byte[5];
			short tag = 0x00d;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			tempBuf[4] = cSrcMaskFlag;
			alist.add(tempBuf);
		}

		if (b_strSrcNumberMask) {
			byte[] tempBuf = new byte[4 + 32];
			short tag = 0x00e;
			short length = 32;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			System.arraycopy(strSrcNumberMask.getBytes(), 0, tempBuf, 4, strSrcNumberMask.getBytes().length);
			alist.add(tempBuf);
		}
//6.3.16	NodesCount
		if (b_cNodesCount) {
			byte[] tempBuf = new byte[5];
			short tag = 0x00f;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			tempBuf[4] = cNodesCount;
			alist.add(tempBuf);
		}

		if (b_strMsgSrc) {
			byte[] tempBuf = new byte[4 + 8];
			short tag = 0x010;
			short length = 21;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			logger.debug("【smgp】strMsgSrc:"+strMsgSrc);
			System.arraycopy(strMsgSrc.getBytes(), 0, tempBuf, 4, strMsgSrc.getBytes().length);
			alist.add(tempBuf);
		}

		if (b_cSpMaskFlag) {
			byte[] tempBuf = new byte[5];
			short tag = 0x011;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
			tempBuf[4] = cSpMaskFlag;
			alist.add(tempBuf);
		}
//6.3.19	MServiceID
		if (b_cMServiceID) {
			byte[] tempBuf = new byte[4+21];
			short tag = 0x012;
			short length = 1;
			System.arraycopy(ByteConvert.shortToBytes(tag), 0, tempBuf, 0, 2);
			System.arraycopy(ByteConvert.shortToBytes(length), 0, tempBuf, 2, 2);
//			tempBuf[4] = cMServiceID;
			System.arraycopy(strMServiceID.getBytes(), 0, tempBuf, 4, strMServiceID.getBytes().length);
			alist.add(tempBuf);
		}

		int bytesLen = 0;
		for (int i = 0; i < alist.size(); i++) {
			byte[] bs = (byte[]) alist.get(i);
			bytesLen += bs.length;
		}

		logger.debug("TLV 包长度=" + bytesLen);

		byte[] ret = new byte[bytesLen];
		pos = 0;
		for (int i = 0; i < alist.size(); i++) {
			byte[] bs = (byte[]) alist.get(i);

			System.arraycopy(bs, 0, ret, pos, bs.length);

			pos += bs.length;
		}
		return ret;

	}

	public void parseBytes(byte[] packet) {
		int pos = 0;

		while (pos < packet.length) {
			byte[] tagBytes = new byte[2];
			System.arraycopy(packet, pos, tagBytes, 0, 2);
			pos += 2;
			byte[] lenBytes = new byte[2];
			System.arraycopy(packet, pos, lenBytes, 0, 2);
			pos += 2;
			short tag = ByteConvert.bytesToShort(tagBytes);
			short len = ByteConvert.bytesToShort(lenBytes);

			switch (tag) {
			case 0x001: {
				cPid = packet[pos];
				pos += len;
				b_cPid = true;
			}
				break;
			case 0x002: {
				cUdhi = packet[pos];
				pos += len;
				b_cUdhi = true;
			}
				break;
			case 0x003: {
				byte[] tempbytes = new byte[len];
				System.arraycopy(packet, pos, tempbytes, 0, len);
				pos += len;
				strLinkId = ByteConvert.getStr(tempbytes);
				b_strLinkId = true;
			}
				break;
			case 0x004: {
				cFeeFlag = packet[pos];
				pos += len;
				b_cFeeFlag = true;
			}
				break;
			case 0x005: {
				cFeeMaskFlag = packet[pos];
				pos += len;
				b_cFeeMaskFlag = true;
			}
				break;
			case 0x006: {
				byte[] tempbytes = new byte[len];
				System.arraycopy(packet, pos, tempbytes, 0, len);
				pos += len;
				strFeeNumberMask = ByteConvert.getStr(tempbytes);
				b_strFeeNumberMask = true;
			}
				break;
			case 0x007: {
				cDestMaskFlag = packet[pos];
				pos += len;
				b_cDestMaskFlag = true;
			}
				break;
			case 0x008: {
				byte[] tempbytes = new byte[len];
				System.arraycopy(packet, pos, tempbytes, 0, len);
				pos += len;
				strDestNumberMask = ByteConvert.getStr(tempbytes);
				b_strDestNumberMask = true;
			}
				break;
			case 0x009: {
				cPkTotal = packet[pos];
				pos += len;
				b_cPkTotal = true;
			}
				break;
			case 0x00a: {
				cPkNumber = packet[pos];
				pos += len;
				b_cPkNumber = true;
			}
				break;
			case 0x00b: {
				cMsgType = packet[pos];
				pos += len;
				b_cMsgType = true;
			}
				break;
			case 0x00c: {
				cSpDealResult = packet[pos];
				pos += len;
				b_cSpDealResult = true;
			}
				break;
			case 0x00d: {
				cSrcMaskFlag = packet[pos];
				pos += len;
				b_cSrcMaskFlag = true;
			}
				break;
			case 0x00e: {
				byte[] tempbytes = new byte[len];
				System.arraycopy(packet, pos, tempbytes, 0, len);
				pos += len;
				strSrcNumberMask = ByteConvert.getStr(tempbytes);
				b_strSrcNumberMask = true;
			}
				break;
			case 0x00f: {
				cNodesCount = packet[pos];
				pos += len;
				b_cNodesCount = true;
			}
				break;
			case 0x010: {
				byte[] tempbytes = new byte[len];
				System.arraycopy(packet, pos, tempbytes, 0, len);
				pos += len;
				strMsgSrc = ByteConvert.getStr(tempbytes);
				b_strMsgSrc = true;
			}
				break;
			case 0x011: {
				cSpMaskFlag = packet[pos];
				pos += len;
				b_cSpMaskFlag = true;
			}
				break;
			case 0x012: {
				byte[] tempbytes = new byte[len];
				System.arraycopy(packet, pos, tempbytes, 0, len);
				pos += len;
				strMServiceID = ByteConvert.getStr(tempbytes);
				b_cMServiceID = true;
			}
				break;
			default: {
				logger.warn("无效TLV标记:" + tag);
				pos += len;
			}
				break;

			}
		}

	}
}
