package com.cloud.demo.smgp.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloud.demo.utils.ByteConvert;
import com.cloud.demo.utils.MsgUtils;


/**
 * ClassName: MsgDeliver <br/>
 * Function:
 * CMPP_DELIVER操作的目的是ISMG把从短信中心或其它ISMG转发来的短信送交SP，SP以CMPP_DELIVER_RESP消息回应。 <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2016年2月23日 下午6:47:04 <br/>
 *
 * @author LiHao
 * @version
 * @since JDK 1.6
 */
public class SmgpMsgDeliver extends SmgpMsgHead {
	private static Logger logger = LoggerFactory.getLogger(SmgpMsgDeliver.class);
	private String sMsgID; // 短消息标识
	private byte cIsReport; // 是否状态报告(0：不是 1：是)
	private byte ucMsgFormat; // 短消息格式(参见短消息格式表)
	private String sRecvTime; // 短消息接收时间(格式：yyyymmddhhmmss)
	private String sSrcTermID; // 短消息发送号码
	private String sDestTermID; // 短消息接收号码
	private byte ucMsgLength; // 短消息长度
	private String sMsgContent; // 短消息内容 String
	private String sReserve; // 保留
//	private String sIsmgMsgID; // 短消息标识
	
	private String msg_Id_report;
	private String sub;
	private String vrd;
	private String submit_time;
	private String done_time;
	private String stat;
	private String err;
	private String txt;
	private int result;// 解析结果
	public SmgpMsgDeliver(){
		
	}
	public SmgpMsgDeliver(int packetLength, int requestId, int sequenceId, byte[] data) {
		if (data.length >= 10 + 1 + 1 + 14 + 21 + 21 + 1 + 8) {// +Msg_length+
			try {
			String fmtStr="gbk";
			this.setPacketLength(packetLength);
			this.setRequestId(requestId);
			this.setSequenceId(sequenceId);
			int cur = 0;

			byte[] msgid = new byte[10];
			System.arraycopy(data, cur, msgid, 0, 10);

			this.sMsgID = ByteConvert.getHexStr(msgid);

			cur += 10;

			this.cIsReport = data[cur];
			cur += 1;

			this.ucMsgFormat = data[cur];
			cur += 1;
			fmtStr=this.ucMsgFormat==8?"UTF-16BE":"gbk";
			byte[] recvtime = new byte[14];

			System.arraycopy(data, cur, recvtime, 0, 14);
			this.sRecvTime = new String(recvtime);
			cur += 14;

			byte[] srcterid = new byte[21];
			System.arraycopy(data, cur, srcterid, 0, 21);
			this.sSrcTermID = new String(srcterid);
			cur += 21;

			byte[] disttermid = new byte[21];
			System.arraycopy(data, cur, disttermid, 0, 21);
			this.sDestTermID = new String(disttermid);
			cur += 21;

			this.ucMsgLength = data[cur];
			cur += 1;

			int msglen = this.ucMsgLength >= 0 ? this.ucMsgLength : 256 + this.ucMsgLength;
			logger.debug("this.ucMsgLength===" + this.ucMsgLength);

			byte[] msgcontent = new byte[msglen];
			System.arraycopy(data, cur, msgcontent, 0, msglen);
//			this.sMsgContent = new String(msgcontent);
			cur += msglen;
			if(cIsReport==1){//状态报告
				if(msglen>=10+3+3+10+10+7+3+20){
					
					byte[] msgIdReportByte=new byte[10];
					System.arraycopy(msgcontent,3,msgIdReportByte,0,10);
					this.msg_Id_report=ByteConvert.rhex(msgIdReportByte);
					byte[] subByte=new byte[3];//
					System.arraycopy(msgcontent,13+5,subByte,0,3);
					this.sub=new String(subByte,fmtStr);
					byte[] vrdByte=new byte[3]; //Dlvrd
					System.arraycopy(msgcontent,21+7,vrdByte,0,3);
					this.vrd=new String(vrdByte,fmtStr);
					byte[] submit_timeByte=new byte[10];//Submit_date
					System.arraycopy(msgcontent,31+13,submit_timeByte,0,10);
					this.submit_time=new String(submit_timeByte,fmtStr);
					byte[] done_timeByte=new byte[10];//done_date
					System.arraycopy(msgcontent,54+11,done_timeByte,0,10);
					this.done_time=new String(done_timeByte,fmtStr);
					byte[] statByte=new byte[7]; //stat
					System.arraycopy(msgcontent,75+6,statByte,0,7);
					this.stat=new String(statByte,fmtStr);
					byte[] errByte=new byte[3];
					System.arraycopy(msgcontent,88+5,errByte,0,3);
					this.err=new String(errByte,fmtStr);
					byte[] txtByte=new byte[20];//Txt
					System.arraycopy(msgcontent,96+5,txtByte,0,20);
					this.txt=new String(txtByte,fmtStr);
					this.result=0;//正确
					
				}else{
					logger.warn("smgp状态报告消息结构错,msglen:"+msglen);
					this.result=1;//消息结构错
				}
			}else{
				logger.debug("上行内容解析,msg_Fmt:{},fmtStr:{}",new Object[]{ucMsgFormat,fmtStr});
//				if(ucMsgFormat==8){
//					this.sMsgContent=new String(msgcontent, "UTF-16BE");
//				}else{
//					this.sMsgContent=new String(msgcontent,fmtStr);//消息长度
//				}
				byte byte1 = 0;
                byte byte2 = 0;
                byte byte3 = 0;
                int len= msgcontent.length;
                if (len >= 3) {
                    byte1 = msgcontent[0];
                    byte2 = msgcontent[1];
                    byte3 = msgcontent[2];
                }
                if (byte1 == 5 && byte2 == 0 && byte3 == 3) {
                	 byte byte4 = msgcontent[3];
                     byte byte5 = msgcontent[4];
                     byte byte6 = msgcontent[5];
                  // 短信内容从第7位开始取
                     byte[] msgBytes = new byte[msgcontent.length - 6];
                     System.arraycopy(msgcontent, 6, msgBytes, 0, len - 6);
                     if(ucMsgFormat==8){
     					this.sMsgContent=new String(msgBytes, "UTF-16BE");
 					}else{
 						this.sMsgContent=new String(msgBytes,fmtStr);//消息长度
 					}
                    this.sMsgContent="("+byte6+"/"+byte5+"):"+sMsgContent;
                    logger.info("收到一个长短信，共" + byte5 + "条，此条是第" + byte6 + "条,content:"+this.sMsgContent);
                }else{
    				if(ucMsgFormat==8){
    					this.sMsgContent=new String(msgcontent, "UTF-16BE");
					}else{
						this.sMsgContent=new String(msgcontent,fmtStr);//消息长度
					}
                }
			}
			byte[] reserve = new byte[8];
			System.arraycopy(data, cur, reserve, 0, 8);
			this.sReserve = new String(reserve);
			// cur+=8;
			this.result = 0;// 正确
			}catch (IOException e){
				this.result=8;//消息结构错
				logger.warn("短信网关SMGP_DELIVER,解析数据包异常" + e.getMessage());
			}
		} else {
			this.result = 1;// 消息结构错
			logger.warn("短信网关SMGP_DELIVER,解析数据包出错，包长度不足。长度为:" + data.length);
		}
		
	}
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			dous.writeInt(this.getPacketLength());
			dous.writeInt(this.getRequestId());
			dous.writeInt(this.getSequenceId());
			
			dous.close();
		} catch (IOException e) {
			logger.error("封装链接二进制数组失败。");
			e.printStackTrace();
		}
		return bous.toByteArray();
	}
	/**
	 * msg_Id_report.
	 *
	 * @return  the msg_Id_report
	 * @since   JDK 1.6
	 */
	public String getMsg_Id_report() {
		return msg_Id_report;
	}

	/**
	 * sub.
	 *
	 * @return  the sub
	 * @since   JDK 1.6
	 */
	public String getSub() {
		return sub;
	}

	/**
	 * vrd.
	 *
	 * @return  the vrd
	 * @since   JDK 1.6
	 */
	public String getVrd() {
		return vrd;
	}

	/**
	 * submit_time.
	 *
	 * @return  the submit_time
	 * @since   JDK 1.6
	 */
	public String getSubmit_time() {
		return submit_time;
	}

	/**
	 * done_time.
	 *
	 * @return  the done_time
	 * @since   JDK 1.6
	 */
	public String getDone_time() {
		return done_time;
	}

	/**
	 * stat.
	 *
	 * @return  the stat
	 * @since   JDK 1.6
	 */
	public String getStat() {
		return stat;
	}

	/**
	 * err.
	 *
	 * @return  the err
	 * @since   JDK 1.6
	 */
	public String getErr() {
		return err;
	}

	/**
	 * txt.
	 *
	 * @return  the txt
	 * @since   JDK 1.6
	 */
	public String getTxt() {
		return txt;
	}

	/**
	 * msg_Id_report.
	 *
	 * @param   msg_Id_report    the msg_Id_report to set
	 * @since   JDK 1.6
	 */
	public void setMsg_Id_report(String msg_Id_report) {
		this.msg_Id_report = msg_Id_report;
	}

	/**
	 * sub.
	 *
	 * @param   sub    the sub to set
	 * @since   JDK 1.6
	 */
	public void setSub(String sub) {
		this.sub = sub;
	}

	/**
	 * vrd.
	 *
	 * @param   vrd    the vrd to set
	 * @since   JDK 1.6
	 */
	public void setVrd(String vrd) {
		this.vrd = vrd;
	}

	/**
	 * submit_time.
	 *
	 * @param   submit_time    the submit_time to set
	 * @since   JDK 1.6
	 */
	public void setSubmit_time(String submit_time) {
		this.submit_time = submit_time;
	}

	/**
	 * done_time.
	 *
	 * @param   done_time    the done_time to set
	 * @since   JDK 1.6
	 */
	public void setDone_time(String done_time) {
		this.done_time = done_time;
	}

	/**
	 * stat.
	 *
	 * @param   stat    the stat to set
	 * @since   JDK 1.6
	 */
	public void setStat(String stat) {
		this.stat = stat;
	}

	/**
	 * err.
	 *
	 * @param   err    the err to set
	 * @since   JDK 1.6
	 */
	public void setErr(String err) {
		this.err = err;
	}

	/**
	 * txt.
	 *
	 * @param   txt    the txt to set
	 * @since   JDK 1.6
	 */
	public void setTxt(String txt) {
		this.txt = txt;
	}

	/**
	 * result.
	 *
	 * @return the result
	 * @since JDK 1.6
	 */
	public int getResult() {
		return result;
	}

	/**
	 * result.
	 *
	 * @param result
	 *            the result to set
	 * @since JDK 1.6
	 */
	public void setResult(int result) {
		this.result = result;
	}

	/**
	 * sMsgID.
	 *
	 * @return the sMsgID
	 * @since JDK 1.6
	 */
	public String getsMsgID() {
		return sMsgID;
	}

	/**
	 * cIsReport.
	 *
	 * @return the cIsReport
	 * @since JDK 1.6
	 */
	public byte getcIsReport() {
		return cIsReport;
	}

	/**
	 * ucMsgFormat.
	 *
	 * @return the ucMsgFormat
	 * @since JDK 1.6
	 */
	public byte getUcMsgFormat() {
		return ucMsgFormat;
	}

	/**
	 * sRecvTime.
	 *
	 * @return the sRecvTime
	 * @since JDK 1.6
	 */
	public String getsRecvTime() {
		return sRecvTime;
	}

	/**
	 * sSrcTermID.
	 *
	 * @return the sSrcTermID
	 * @since JDK 1.6
	 */
	public String getsSrcTermID() {
		return sSrcTermID;
	}

	/**
	 * sDestTermID.
	 *
	 * @return the sDestTermID
	 * @since JDK 1.6
	 */
	public String getsDestTermID() {
		return sDestTermID;
	}

	/**
	 * ucMsgLength.
	 *
	 * @return the ucMsgLength
	 * @since JDK 1.6
	 */
	public byte getUcMsgLength() {
		return ucMsgLength;
	}

	/**
	 * sMsgContent.
	 *
	 * @return the sMsgContent
	 * @since JDK 1.6
	 */
	public String getsMsgContent() {
		return sMsgContent;
	}

	

	/**
	 * sReserve.
	 *
	 * @return the sReserve
	 * @since JDK 1.6
	 */
	public String getsReserve() {
		return sReserve;
	}



	/**
	 * sMsgID.
	 *
	 * @param sMsgID
	 *            the sMsgID to set
	 * @since JDK 1.6
	 */
	public void setsMsgID(String sMsgID) {
		this.sMsgID = sMsgID;
	}

	/**
	 * cIsReport.
	 *
	 * @param cIsReport
	 *            the cIsReport to set
	 * @since JDK 1.6
	 */
	public void setcIsReport(byte cIsReport) {
		this.cIsReport = cIsReport;
	}

	/**
	 * ucMsgFormat.
	 *
	 * @param ucMsgFormat
	 *            the ucMsgFormat to set
	 * @since JDK 1.6
	 */
	public void setUcMsgFormat(byte ucMsgFormat) {
		this.ucMsgFormat = ucMsgFormat;
	}

	/**
	 * sRecvTime.
	 *
	 * @param sRecvTime
	 *            the sRecvTime to set
	 * @since JDK 1.6
	 */
	public void setsRecvTime(String sRecvTime) {
		this.sRecvTime = sRecvTime;
	}

	/**
	 * sSrcTermID.
	 *
	 * @param sSrcTermID
	 *            the sSrcTermID to set
	 * @since JDK 1.6
	 */
	public void setsSrcTermID(String sSrcTermID) {
		this.sSrcTermID = sSrcTermID;
	}

	/**
	 * sDestTermID.
	 *
	 * @param sDestTermID
	 *            the sDestTermID to set
	 * @since JDK 1.6
	 */
	public void setsDestTermID(String sDestTermID) {
		this.sDestTermID = sDestTermID;
	}

	/**
	 * ucMsgLength.
	 *
	 * @param ucMsgLength
	 *            the ucMsgLength to set
	 * @since JDK 1.6
	 */
	public void setUcMsgLength(byte ucMsgLength) {
		this.ucMsgLength = ucMsgLength;
	}

	/**
	 * sMsgContent.
	 *
	 * @param sMsgContent
	 *            the sMsgContent to set
	 * @since JDK 1.6
	 */
	public void setsMsgContent(String sMsgContent) {
		this.sMsgContent = sMsgContent;
	}


	/**
	 * sReserve.
	 *
	 * @param sReserve
	 *            the sReserve to set
	 * @since JDK 1.6
	 */
	public void setsReserve(String sReserve) {
		this.sReserve = sReserve;
	}



}
