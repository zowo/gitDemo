/**
 * Project Name:ccopsgw
 * File Name:SmgpMsgLoginResp.java
 * Package Name:com.ccop.common.smgp.msg
 * Date:2016年2月25日下午7:24:02
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.smgp.msg;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.cloud.demo.utils.MsgUtils;

/**
 * ClassName:SmgpMsgLoginResp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年2月25日 下午7:24:02 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SmgpMsgLoginResp extends SmgpMsgHead {
	private static Logger logger=Logger.getLogger(SmgpMsgLoginResp.class);
	private int status;//响应状态状态 0：正确 1：消息结构错 2：非法源地址 3：认证错 4：版本太高 5~ ：其他错误
	private String statusStr;//响应状态状态 0：正确 1：消息结构错 2：非法源地址 3：认证错 4：版本太高 5~ ：其他错误
	private byte[] authenticatorServer;//	ISMG认证码，用于鉴别ISMG。 其值通过单向MD5 hash计算得出，表示如下： AuthenticatorISMG =MD5（Status+AuthenticatorSource+shared secret），Shared secret 由中国移动与源地址实体事先商定，AuthenticatorSource为源地址实体发送给ISMG的对应消息CMPP_Connect中的值。 认证出错时，此项为空。
	private byte version;//	服务器支持的最高版本号，对于3.0的版本，高4bit为3，低4位为0
	public SmgpMsgLoginResp(){
		
	}
	public SmgpMsgLoginResp(int packetLength,int requestId,int sequenceId,byte[] data){
		if(data.length==4+16+1){
			ByteArrayInputStream bins=new ByteArrayInputStream(data);
			DataInputStream dins=new DataInputStream(bins);
			try {
				this.setPacketLength(packetLength);
				this.setRequestId(requestId);
				this.setSequenceId(sequenceId);
				this.setStatus(dins.readInt());
				byte[] aiByte=new byte[16];
				dins.read(aiByte);
				this.authenticatorServer=aiByte;
				this.version=dins.readByte();
				dins.close();
				bins.close();
			} catch (IOException e){}
		}else{
			logger.info("smgp登录,解析数据包出错，包长度不一致。长度为:"+data.length);
		}
	}
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			dous.writeInt(this.getPacketLength());
			dous.writeInt(this.getRequestId());
			dous.writeInt(this.getSequenceId());
			dous.writeInt(this.getStatus());
			dous.write(authenticatorServer);
			dous.writeByte(version);
			dous.close();
		} catch (IOException e) {
			logger.error("封装链接二进制数组失败。");
			e.printStackTrace();
		}
		return bous.toByteArray();
	}
	/**
	 * status.
	 *
	 * @return  the status
	 * @since   JDK 1.6
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * statusStr.
	 *
	 * @return  the statusStr
	 * @since   JDK 1.6
	 */
	public String getStatusStr() {
		return statusStr;
	}
	/**
	 * authenticatorServer.
	 *
	 * @return  the authenticatorServer
	 * @since   JDK 1.6
	 */
	public byte[] getAuthenticatorServer() {
		return authenticatorServer;
	}
	/**
	 * version.
	 *
	 * @return  the version
	 * @since   JDK 1.6
	 */
	public byte getVersion() {
		return version;
	}
	/**
	 * status.
	 *
	 * @param   status    the status to set
	 * @since   JDK 1.6
	 */
	public void setStatus(int status) {
		this.status = status;
		switch(status){
		case 0 : statusStr="成功";break;
		case 1 : statusStr="系统忙";break;
		case 2 : statusStr="超过最大连接数";break;
		case 10 : statusStr="消息结构错";break;
		case 11 : statusStr="命令字错";break;
		case 12 : statusStr="序列号重复";break;
		case 20 : statusStr="IP地址错";break;
		case 21 : statusStr="认证错";break;
		case 22 : statusStr="版本太高";break;
		default:statusStr=status+":未知";break;
		}
	}
	/**
	 * statusStr.
	 *
	 * @param   statusStr    the statusStr to set
	 * @since   JDK 1.6
	 */
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	/**
	 * authenticatorServer.
	 *
	 * @param   authenticatorServer    the authenticatorServer to set
	 * @since   JDK 1.6
	 */
	public void setAuthenticatorServer(byte[] authenticatorServer) {
		this.authenticatorServer = authenticatorServer;
	}
	/**
	 * version.
	 *
	 * @param   version    the version to set
	 * @since   JDK 1.6
	 */
	public void setVersion(byte version) {
		this.version = version;
	}
	
	
}

