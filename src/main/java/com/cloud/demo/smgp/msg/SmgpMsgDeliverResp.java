package com.cloud.demo.smgp.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.cloud.demo.smgp.SmgpCommand;
import com.cloud.demo.utils.ByteConvert;



/**
 * ClassName: MsgDeliverResp <br/>
 * Function: CMPP_DELIVER_RESP结构 <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2016年2月23日 下午6:47:23 <br/>
 *
 * @author LiHao
 * @version 
 * @since JDK 1.6
 */
public class SmgpMsgDeliverResp extends SmgpMsgHead {
	private static Logger logger=Logger.getLogger(SmgpMsgDeliverResp.class);
	private String msg_Id;//信息标识（CMPP_DELIVER中的Msg_Id字段）
	private int status;//结果 0：正确 1：消息结构错 2：命令字错 3：消息序号重复 4：消息长度错 5：资费代码错 6：超过最大信息长 7：业务代码错8: 流量控制错9~ ：其他错误
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			dous.writeInt(this.getPacketLength());
			dous.writeInt(this.getRequestId());
			dous.writeInt(this.getSequenceId());
//			MsgUtils.writeString(dous,this.msg_Id,10);
			byte[] b=ByteConvert.rstr(this.msg_Id);
			dous.write(b);
			dous.writeInt(this.status);
			dous.close();
		} catch (IOException e) {
			logger.error("封装链接二进制数组失败。");
		}
		return bous.toByteArray();
	}
	public String getMsg_Id() {
		return msg_Id;
	}

	public void setMsg_Id(String msg_Id) {
		this.msg_Id = msg_Id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	public static void main(String[] args) {
		SmgpMsgDeliverResp msgDeliverResp=new SmgpMsgDeliverResp();
		msgDeliverResp.setPacketLength(12+10+4);
		msgDeliverResp.setRequestId(SmgpCommand.CMD_SMGP_DELIVER_RESP);
		msgDeliverResp.setSequenceId(1840173393);
		msgDeliverResp.setMsg_Id("02510002280203094599");
		msgDeliverResp.setStatus(0);
		byte[] b=msgDeliverResp.toByteArry();
		logger.debug("b:"+b.length);
	}
}
