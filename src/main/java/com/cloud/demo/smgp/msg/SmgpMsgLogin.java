/**
 * Project Name:ccopsgw
 * File Name:SmgpMsgLogin.java
 * Package Name:com.ccop.common.smgp.msg
 * Date:2016年2月25日下午6:21:02
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.smgp.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.cloud.demo.utils.MsgUtils;


/**
 * ClassName:SmgpMsgLogin <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年2月25日 下午6:21:02 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SmgpMsgLogin extends SmgpMsgHead {
	private static Logger logger=Logger.getLogger(SmgpMsgLogin.class);
	private String clientId;
	private byte[] authenticatorClient;//MD5（ClientID+7 字节的二进制0（0x00） + Shared secret+Timestamp）
	private byte loginMode;
	private int timestamp;//时间戳的明文,由客户端产生,格式为MMDDHHMMSS，即月日时分秒，10位数字的整型，右对齐 。
	private byte clientVersion;
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			dous.writeInt(this.getPacketLength());
			dous.writeInt(this.getRequestId());
			dous.writeInt(this.getSequenceId());
			MsgUtils.writeString(dous,this.clientId,8);
			dous.write(authenticatorClient);
			dous.writeByte(loginMode);
			dous.writeInt(Integer.parseInt(MsgUtils.getTimestamp()));
			dous.writeByte(clientVersion);
			dous.close();
		} catch (IOException e) {
			logger.error("封装链接二进制数组失败。");
			e.printStackTrace();
		}
		return bous.toByteArray();
	}
	/**
	 * logger.
	 *
	 * @return  the logger
	 * @since   JDK 1.6
	 */
	public static Logger getLogger() {
		return logger;
	}
	/**
	 * clientId.
	 *
	 * @return  the clientId
	 * @since   JDK 1.6
	 */
	public String getClientId() {
		return clientId;
	}
	/**
	 * authenticatorClient.
	 *
	 * @return  the authenticatorClient
	 * @since   JDK 1.6
	 */
	public byte[] getAuthenticatorClient() {
		return authenticatorClient;
	}
	/**
	 * loginMode.
	 *
	 * @return  the loginMode
	 * @since   JDK 1.6
	 */
	public byte getLoginMode() {
		return loginMode;
	}
	/**
	 * timestamp.
	 *
	 * @return  the timestamp
	 * @since   JDK 1.6
	 */
	public int getTimestamp() {
		return timestamp;
	}
	/**
	 * clientVersion.
	 *
	 * @return  the clientVersion
	 * @since   JDK 1.6
	 */
	public byte getClientVersion() {
		return clientVersion;
	}
	/**
	 * logger.
	 *
	 * @param   logger    the logger to set
	 * @since   JDK 1.6
	 */
	public static void setLogger(Logger logger) {
		SmgpMsgLogin.logger = logger;
	}
	/**
	 * clientId.
	 *
	 * @param   clientId    the clientId to set
	 * @since   JDK 1.6
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	/**
	 * authenticatorClient.
	 *
	 * @param   authenticatorClient    the authenticatorClient to set
	 * @since   JDK 1.6
	 */
	public void setAuthenticatorClient(byte[] authenticatorClient) {
		this.authenticatorClient = authenticatorClient;
	}
	/**
	 * loginMode.
	 *
	 * @param   loginMode    the loginMode to set
	 * @since   JDK 1.6
	 */
	public void setLoginMode(byte loginMode) {
		this.loginMode = loginMode;
	}
	/**
	 * timestamp.
	 *
	 * @param   timestamp    the timestamp to set
	 * @since   JDK 1.6
	 */
	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}
	/**
	 * clientVersion.
	 *
	 * @param   clientVersion    the clientVersion to set
	 * @since   JDK 1.6
	 */
	public void setClientVersion(byte clientVersion) {
		this.clientVersion = clientVersion;
	}
	
}

