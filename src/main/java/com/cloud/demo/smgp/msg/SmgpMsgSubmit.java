package com.cloud.demo.smgp.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.cloud.demo.utils.MsgUtils;

/**
 * ClassName: SmgpMsgSubmit <br/>
 * Function: smgp Submit消息结构定义 <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2016年2月17日 下午3:08:51 <br/>
 *
 * @author LiHao
 * @version 
 * @since JDK 1.6
 */
public class SmgpMsgSubmit extends SmgpMsgHead {
	private static Logger logger=Logger.getLogger(SmgpMsgSubmit.class);
	private byte msgType=0x06; //6＝MT消息（SP发给终端，包括WEB上发送的点对点短消息）；
	private byte needReport=0x01; //0＝不要求返回状态报告 1＝要求返回状态报告；
	private byte priority=0x03; //短消息发送优先级 0＝低优先级；1＝普通优先级；2＝较高优先级；3＝高优先级；
	private String serviceId;
	private String feeType="";
	private String feeCode="";
	private String fixedFee="";
	private byte msgFormat=0x0f;  //15＝GB18030编码
	private String validTime = "";
	private String atTime = "";
	private String srcTermId="";//短消息发送方号码
	private String chargeTermId=""; //计费用户号码
	private	byte destTermIDCount;
	private	String[] destTermId;
	private	byte msgLength;
	private	byte[] msgContent;
	private	String reserve="";
	private SmgpTlv smgpTlv;
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			if(smgpTlv==null){
				smgpTlv=new SmgpTlv();
			}
			byte[] tlv = smgpTlv.getBytes();
			logger.debug("tlv.length:"+tlv.length+",this.getPacketLength():"+this.getPacketLength());
			dous.writeInt(this.getPacketLength()+tlv.length);
			dous.writeInt(this.getRequestId());
			dous.writeInt(this.getSequenceId());
			dous.writeByte(this.msgType);//短消息类型
			dous.writeByte(this.needReport);
			dous.writeByte(this.priority);
			MsgUtils.writeString(dous,this.serviceId,10);
			MsgUtils.writeString(dous,this.feeType,2);
			MsgUtils.writeString(dous,this.feeCode,6);
			MsgUtils.writeString(dous,this.fixedFee,6);
			dous.writeByte(this.msgFormat);
			MsgUtils.writeString(dous,this.validTime,17);//存活有效期
			MsgUtils.writeString(dous,this.atTime,17);//定时发送时间
			MsgUtils.writeString(dous,this.srcTermId,21);
			MsgUtils.writeString(dous,this.chargeTermId,21);
			dous.writeByte(this.destTermIDCount);
			for (int i = 0; i < destTermId.length; i++) {
				MsgUtils.writeString(dous,this.destTermId[i],21);
			}
			dous.writeByte(this.msgLength);//Msg_Length
			dous.write(this.msgContent);//信息内容		
			
			MsgUtils.writeString(dous,this.reserve,8);
			
			dous.write(tlv);
			dous.close();
			
		} catch (IOException e) {
			logger.error("smgp封装短信发送二进制数组失败。");
		}
		return bous.toByteArray();
	}
	/**
	 * msgType.
	 *
	 * @return  the msgType
	 * @since   JDK 1.6
	 */
	public byte getMsgType() {
		return msgType;
	}
	/**
	 * needReport.
	 *
	 * @return  the needReport
	 * @since   JDK 1.6
	 */
	public byte getNeedReport() {
		return needReport;
	}
	/**
	 * priority.
	 *
	 * @return  the priority
	 * @since   JDK 1.6
	 */
	public byte getPriority() {
		return priority;
	}
	/**
	 * serviceId.
	 *
	 * @return  the serviceId
	 * @since   JDK 1.6
	 */
	public String getServiceId() {
		return serviceId;
	}
	/**
	 * feeType.
	 *
	 * @return  the feeType
	 * @since   JDK 1.6
	 */
	public String getFeeType() {
		return feeType;
	}
	/**
	 * feeCode.
	 *
	 * @return  the feeCode
	 * @since   JDK 1.6
	 */
	public String getFeeCode() {
		return feeCode;
	}
	/**
	 * fixedFee.
	 *
	 * @return  the fixedFee
	 * @since   JDK 1.6
	 */
	public String getFixedFee() {
		return fixedFee;
	}
	/**
	 * msgFormat.
	 *
	 * @return  the msgFormat
	 * @since   JDK 1.6
	 */
	public byte getMsgFormat() {
		return msgFormat;
	}
	/**
	 * valIdTime.
	 *
	 * @return  the valIdTime
	 * @since   JDK 1.6
	 */
	public String getValidTime() {
		return validTime;
	}
	/**
	 * atTime.
	 *
	 * @return  the atTime
	 * @since   JDK 1.6
	 */
	public String getAtTime() {
		return atTime;
	}
	/**
	 * srcTermId.
	 *
	 * @return  the srcTermId
	 * @since   JDK 1.6
	 */
	public String getSrcTermId() {
		return srcTermId;
	}
	/**
	 * chargeTermId.
	 *
	 * @return  the chargeTermId
	 * @since   JDK 1.6
	 */
	public String getChargeTermId() {
		return chargeTermId;
	}
	/**
	 * destTermIDCount.
	 *
	 * @return  the destTermIDCount
	 * @since   JDK 1.6
	 */
	public byte getDestTermIDCount() {
		return destTermIDCount;
	}
	/**
	 * destTermId.
	 *
	 * @return  the destTermId
	 * @since   JDK 1.6
	 */
	public String[] getDestTermId() {
		return destTermId;
	}
	/**
	 * msgLength.
	 *
	 * @return  the msgLength
	 * @since   JDK 1.6
	 */
	public byte getMsgLength() {
		return msgLength;
	}
	/**
	 * msgContent.
	 *
	 * @return  the msgContent
	 * @since   JDK 1.6
	 */
	public byte[] getMsgContent() {
		return msgContent;
	}
	/**
	 * reserve.
	 *
	 * @return  the reserve
	 * @since   JDK 1.6
	 */
	public String getReserve() {
		return reserve;
	}
	
	/**
	 * smgpTlv.
	 *
	 * @return  the smgpTlv
	 * @since   JDK 1.6
	 */
	public SmgpTlv getSmgpTlv() {
		return smgpTlv;
	}
	/**
	 * smgpTlv.
	 *
	 * @param   smgpTlv    the smgpTlv to set
	 * @since   JDK 1.6
	 */
	public void setSmgpTlv(SmgpTlv smgpTlv) {
		this.smgpTlv = smgpTlv;
	}
	/**
	 * msgType.
	 *
	 * @param   msgType    the msgType to set
	 * @since   JDK 1.6
	 */
	public void setMsgType(byte msgType) {
		this.msgType = msgType;
	}
	/**
	 * needReport.
	 *
	 * @param   needReport    the needReport to set
	 * @since   JDK 1.6
	 */
	public void setNeedReport(byte needReport) {
		this.needReport = needReport;
	}
	/**
	 * priority.
	 *
	 * @param   priority    the priority to set
	 * @since   JDK 1.6
	 */
	public void setPriority(byte priority) {
		this.priority = priority;
	}
	/**
	 * serviceId.
	 *
	 * @param   serviceId    the serviceId to set
	 * @since   JDK 1.6
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	/**
	 * feeType.
	 *
	 * @param   feeType    the feeType to set
	 * @since   JDK 1.6
	 */
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}
	/**
	 * feeCode.
	 *
	 * @param   feeCode    the feeCode to set
	 * @since   JDK 1.6
	 */
	public void setFeeCode(String feeCode) {
		this.feeCode = feeCode;
	}
	/**
	 * fixedFee.
	 *
	 * @param   fixedFee    the fixedFee to set
	 * @since   JDK 1.6
	 */
	public void setFixedFee(String fixedFee) {
		this.fixedFee = fixedFee;
	}
	/**
	 * msgFormat.
	 *
	 * @param   msgFormat    the msgFormat to set
	 * @since   JDK 1.6
	 */
	public void setMsgFormat(byte msgFormat) {
		this.msgFormat = msgFormat;
	}
	/**
	 * valIdTime.
	 *
	 * @param   valIdTime    the valIdTime to set
	 * @since   JDK 1.6
	 */
	public void setValidTime(String validTime) {
		this.validTime = validTime;
	}
	/**
	 * atTime.
	 *
	 * @param   atTime    the atTime to set
	 * @since   JDK 1.6
	 */
	public void setAtTime(String atTime) {
		this.atTime = atTime;
	}
	/**
	 * srcTermId.
	 *
	 * @param   srcTermId    the srcTermId to set
	 * @since   JDK 1.6
	 */
	public void setSrcTermId(String srcTermId) {
		this.srcTermId = srcTermId;
	}
	/**
	 * chargeTermId.
	 *
	 * @param   chargeTermId    the chargeTermId to set
	 * @since   JDK 1.6
	 */
	public void setChargeTermId(String chargeTermId) {
		this.chargeTermId = chargeTermId;
	}
	/**
	 * destTermIDCount.
	 *
	 * @param   destTermIDCount    the destTermIDCount to set
	 * @since   JDK 1.6
	 */
	public void setDestTermIDCount(byte destTermIDCount) {
		this.destTermIDCount = destTermIDCount;
	}
	/**
	 * destTermId.
	 *
	 * @param   destTermId    the destTermId to set
	 * @since   JDK 1.6
	 */
	public void setDestTermId(String[] destTermId) {
		this.destTermId = destTermId;
	}
	/**
	 * msgLength.
	 *
	 * @param   msgLength    the msgLength to set
	 * @since   JDK 1.6
	 */
	public void setMsgLength(byte msgLength) {
		this.msgLength = msgLength;
	}
	/**
	 * msgContent.
	 *
	 * @param   msgContent    the msgContent to set
	 * @since   JDK 1.6
	 */
	public void setMsgContent(byte[] msgContent) {
		this.msgContent = msgContent;
	}
	/**
	 * reserve.
	 *
	 * @param   reserve    the reserve to set
	 * @since   JDK 1.6
	 */
	public void setReserve(String reserve) {
		this.reserve = reserve;
	}
	
}
