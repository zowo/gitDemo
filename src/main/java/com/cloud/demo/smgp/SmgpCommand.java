package com.cloud.demo.smgp;

public class SmgpCommand {
	public static final int PeripheralID=5000;
	
	/**
	 * 包头长度。（协议采用定长包头，长度为4+4+4)
	 * 
     *Total_Length	4	Unsigned Integer	消息总长度(含消息头及消息体)
     *Request_Id	4	Unsigned Integer	命令或响应类型
     *Sequence_Id	4	Unsigned Integer	消息流水号,顺序累加,步长为1,循环使用（一对请求和应答消息的流水号必须相同）
	 */
	public static final int MsgHeaderLength = 12;
	public static final int MsgTotalLength=4;
	public static final int MsgRequestId=4;
	public static final int MsgSequenceId=4;
	
	public static final int CMD_SMGP_LOGIN	                  = 0x00000001;    // 客户端登录请求
	public static final int CMD_SMGP_LOGIN_RESP	          = 0x80000001;    // 请求连接应答 
	public static final int CMD_SMGP_SUBMIT                  = 0x00000002;    // SP发送短信请求
	public static final int CMD_SMGP_SUBMIT_RESP             = 0x80000002;    // SP发送短信应答
	public static final int CMD_SMGP_DELIVER                 = 0x00000003;    // SMGW向SP发送短信请求
	public static final int CMD_SMGP_DELIVER_RESP            = 0x80000003;    // SMGW向SP发送短信应答
	public static final int CMD_SMGP_ACTIVE_TEST             = 0x00000004;    // 测试链路
	public static final int CMD_SMGP_ACTIVE_TEST_RESP        = 0x80000004;    // 测试链路应答
	public static final int CMD_SMGP_FORWARD                 = 0x00000005;    // SMGW转发短信请求
	public static final int CMD_SMGP_FORWARD_RESP            = 0x80000005;    // SMGW转发短信应答 
	public static final int CMD_SMGP_EXIT		              = 0x00000006;    // 退出请求
	public static final int CMD_SMGP_EXIT_RESP	              = 0x80000006;    // 退出应答
	public static final int CMD_SMGP_QUERY		              = 0x00000007;    // sp统计查询请求
	public static final int CMD_SMGP_QUERY_RESP	          = 0x80000007;	   // sp统计查询应答
	public static final int CMD_SMGP_MT_ROUTE_UPDATE         = 0x00000008;	   // MT路由更新请求
	public static final int CMD_SMGP_MT_ROUTE_UPDATE_RESP    = 0x80000008;	   // MT路由更新应答
	public static final int CMD_SMGP_MO_ROUTE_UPDATE	      = 0x00000009;    // MO路由更新请求
	public static final int CMD_SMGP_MO_ROUTE_UPDATE_RESP    = 0x80000009;	   // MO路由更新应答
}
