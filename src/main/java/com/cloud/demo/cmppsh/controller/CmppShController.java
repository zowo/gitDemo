/**
 * Project Name:ccopsgw
 * File Name:CmppController.java
 * Package Name:com.ccop.sgw.controller
 * Date:2016年2月19日下午1:33:57
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.cmppsh.controller;

import com.cloud.demo.bean.SendMsgReq;
import com.cloud.demo.bean.SendMsgResp;
import com.cloud.demo.cmppsh.socket.CmppShContainer;
import com.cloud.demo.utils.JacksonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * ClassName:CmppController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年2月19日 下午1:33:57 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
@Controller
@RequestMapping("/sgw")
public class CmppShController {
	public final Logger logger = LoggerFactory.getLogger(CmppShController.class);
	@RequestMapping("/cmpp/sendMsg")
	@ResponseBody
	//@RequestBody
	public String sendMsg( SendMsgReq sendMsgReq) throws Exception{
		sendMsgReq =new SendMsgReq();
		String req=JacksonUtil.writeValueAsString(sendMsgReq);
		logger.info("【cmpp接收到短信发送请求】:"+req);
		int result=-3;
		String msgId=null;
		//if(StringUtils.isNotBlank(sendMsgReq.getMsg())&&StringUtils.isNotBlank(sendMsgReq.getTel())&&StringUtils.isNotBlank(sendMsgReq.getAppendCode())
		//		&&StringUtils.isNotBlank(sendMsgReq.getTimestamp())){
			//if(StringUtils.isNotBlank(sendMsgReq.getEncrypted())&&encryptStr.equals(sendMsgReq.getEncrypted())){
//				String [] telArr=sendMsgReq.getTel().split(","); 
//				MsgSubmitResp resp=CmppContainer.sendMsg(sendMsgReq.getMsg(), telArr,sendMsgReq.getAppendCode());
//				MsgSubmitResp resp=CmppShContainer.sendMsg(sendMsgReq);
//				if(resp!=null){
//					result=resp.getResult();
//					msgId=resp.getMsgId()+"";
//				}else{
//					result=-5;
//				}
				/*String sign_white=ReadConfigation.getConfigItem("sign_white");
				String[] signArray=sign_white.split(",");
				boolean inFlag=Utils.isIn(sendMsgReq.getSignature(), signArray);
				boolean checkFlag=true;
				if(!inFlag){
					checkFlag=Utils.checkContent(sendMsgReq.getMsg());
				}*///容联云通讯2
				sendMsgReq.setMsg("【云融天下】快点来贷款啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊你好成功未来嗯嗯嗯!");
				//sendMsgReq.setMsg("【杏仁医生】快点来贷款啊啊啊啊啊啊啊");
				sendMsgReq.setMsg("【容联云通讯2】快点来贷款啊啊啊啊啊啊啊");//308810
				sendMsgReq.setMsg("【容联云通讯2】快点来贷款啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊你好成功未来嗯嗯嗯!");
				//sendMsgReq.setMsg("【云融天下】快点来贷款啊啊啊啊啊啊啊");
				sendMsgReq.setAppendCode("12345300009");//300009 26886
				sendMsgReq.setSignature("证大速贷");
				sendMsgReq.setTel("18601322881");
				sendMsgReq.setOutId("1234567a");
				if(true){
					Map<String, String> map=CmppShContainer.putSendMsg(sendMsgReq);
					if(map!=null){
						msgId=map.get("respId");
						result=Integer.parseInt(map.get("result"));
					}else{
						result=-6;
					}
				}else{
					logger.warn("【cmpp 检测短信含有http】"+req);
					result=-7;
				}
			/*}else{
				logger.warn("【cmpp 接收到短信发送验证失败】");
				result=-4;
			}*/
		//}
		SendMsgResp sendMsgResp =new SendMsgResp();
		sendMsgResp.setResult(result);
		sendMsgResp.setMsgId(msgId);
		String rs=JacksonUtil.writeValueAsString(sendMsgResp);
		logger.info("【cmpp回响应】tel:{},rs:{}",new Object[]{sendMsgReq.getTel(),rs});
		return rs;
	}
}

