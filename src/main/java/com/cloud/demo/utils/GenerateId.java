package com.cloud.demo.utils;

import java.util.UUID;

import org.apache.commons.lang.StringUtils;

public class GenerateId {
	public GenerateId(){
	}
	public static String getUUID(){ 
        String str = UUID.randomUUID().toString(); 
        //去掉“-”符号 
        return StringUtils.remove(str, '-'); 
    }
	public static void main(String[] args) {
		System.out.println(getUUID());
	}
}
