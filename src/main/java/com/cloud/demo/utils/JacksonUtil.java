/**
 * Project Name:ccop.callout
 * File Name:JacksonUtil.java
 * Package Name:com.ccop.common.util
 * Date:2015年8月27日上午10:49:21
 * Copyright (c) 2015, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * ClassName:JacksonUtil <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2015年8月27日 上午10:49:21 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class JacksonUtil {
	public static String writeValueAsString(Object obj) {
		ObjectMapper objectMapper= new ObjectMapper();
		String rs = null;
		try {
			rs = objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			//转换json异常
			rs="{\"StatusCode\":\"900001\"}";
		}
		return rs;
	}
}

