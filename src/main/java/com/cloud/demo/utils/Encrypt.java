package com.cloud.demo.utils;

import java.security.MessageDigest;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * 加密类
 */
public class Encrypt {
	public static String encryptMD5(String strInput) {
	    StringBuffer buf = null;
	    try {
	      MessageDigest md = MessageDigest.getInstance("MD5");
	      md.update(strInput.getBytes());
	      byte[] b = md.digest();
	      buf = new StringBuffer(b.length * 2);
	      for (int i = 0; i < b.length; ++i) {
	        if ((b[i] & 0xFF) < 16)
	          buf.append("0");

	        buf.append(Long.toHexString(b[i] & 0xFF));
	      }
	    } catch (Exception ex) {
	      ex.printStackTrace();
	    }
	    return buf.toString();
	}
	
	public static String generatePwd(String pwd,String salt) {
		   String mergePwd=pwd+"{"+salt+"}"; 
		   return encryptMD5(mergePwd);	   
	}
	/** 
     * BASE64编码
     * @param src 
     * @return 
     * @throws Exception 
     */  
    public static String base64Encoder(String src) throws Exception {  
        BASE64Encoder encoder = new BASE64Encoder();  
        return encoder.encode(src.getBytes("utf-8"));  
    }  
      
    /** 
     * BASE64解码
     * @param dest 
     * @return 
     * @throws Exception 
     */  
    public static String base64Decoder(String dest) throws Exception {  
        BASE64Decoder decoder = new BASE64Decoder();  
        return new String(decoder.decodeBuffer(dest), "utf-8");  
    }  
	public static void main(String[] args) {
		//String str=encryptMD5("ff8080813adc720e013adcbd5e1c0000zray");
//		String str="";
//		try {
//			str = base64Decoder("ZmY4MDgwODEzYmJjYWUzZjAxM2JjYzM5YzE4YTAwMjI6MjAxMzA4MDgxNDI3MDE=");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println(str);
		//0a0a607531654c420edeabffc32ca58e
		String str=encryptMD5("13791820726");
		//String str=encryptMD5("ccop_bbs_web101");
		System.out.println("str>>"+str);
		//str=encryptMD5(str+"171ade");
		//System.out.println("11>>"+str);
	}
}
