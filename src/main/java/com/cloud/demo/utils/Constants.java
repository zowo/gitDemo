package com.cloud.demo.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 系统常量类
 */
public class Constants {
	private static final Logger logger = LoggerFactory.getLogger(Constants.class);
	private static Constants _instance=new Constants();
	/**
	 * redis数据有效时间
	 */
	public static final long SMS_EXPIRE_SECONDS=24*60*60;
	/**
	 * redis存储报告数据有效时间
	 */
	public static final long SMS_EXPIRE_REPORT=60*60;
	
	//返回给网关通道序号前缀
	//上海移动
//	public static final String SMS_SEQ_PREFIX_CMPPSH = "sqcpsh";
//	//江苏电信号百
//	public static final String SMS_SEQ_PREFIX_SMGPHB = "sqsghb";
//	//北京电信
//	public static final String SMS_SEQ_PREFIX_SMGPBJ = "sqsgbj";
	
	synchronized public static Constants getInstance(){
		return _instance;
	}
	
	static {
		logger.info("常量赋值");
	}
	public static int SMS_INDB_MAX_COUNT   = 1000;	// 批量入库条数
	public static int SMS_BATCH_MAX_TIME   = 5000;	// 5秒 如果在给定的等待时间内此信号量有可用的所有许可，并且当前线程未被中断，则从此信号量获取给定数目的许可。
	
	public static final int TEST_CONN_COUNT=3;
	
	
	public static final int SEQ_MIN = 1;
	public static final int SEQ_MAX = 99999999;
	
	/***序号和返给网关的消息id的关系****/
	public static final String SMS_REDIS_SEQ_SMGP="k390SG_";
	public static final String SMS_REDIS_SEQ_CMPP2="K390CP2_";
	
	public static final String SMS_REDIS_SEQ_SMPP="k390SP_";
	/***通道id和返给网关的消息id的关系*****START****/
	public static final String SMS_REDIS_MSGID_SMGP="k391SG_";
	public static final String SMS_REDIS_MSGID_CMPP2="k391CP2_";
	
	public static final String SMS_REDIS_MSGID_SMPP="k391SP_";
	
	/*******自增序号 *START****/
	public static final String SMS_REDIS_INCR_SMGP="k392SG_";
	public static final String SMS_REDIS_INCR_CMPP2="k392CP2_";
	
	public static final String SMS_REDIS_INCR_SMPP="k392SP_";
	/***归避submitResp返回滞后 存储状态报告*****START****/
	public static final String SMS_REDIS_REPORT_SMGP= "k393SG_";
	public static final String SMS_REDIS_REPORT_CMPP2= "k393CP2_";
	
	public static final String SMS_REDIS_REPORT_SMPP= "k393SP_";
	
	//上海移动
	public static final String SMS_REDIS_SEQ_CMPPSH = "k390CP_SHYD";
	public static final String SMS_REDIS_MSGID_CMPPSH = "k391CP_SHYD";
	public static final int SMS_CARRIER_TYPE_CMPPSH=1;
	//上海移动
		public static final int CMPP_QPS_SHYD = Integer.parseInt("10");
}
