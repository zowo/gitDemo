package com.cloud.demo.utils;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.UnsupportedEncodingException;

public class ByteConvert {

	// 以下 是整型数 和 网络字节序的 byte[] 数组之间的转换
	public static byte[] longToBytes(long n) {
		byte[] b = new byte[8];
		b[7] = (byte) (n & 0xff);
		b[6] = (byte) (n >> 8 & 0xff);
		b[5] = (byte) (n >> 16 & 0xff);
		b[4] = (byte) (n >> 24 & 0xff);
		b[3] = (byte) (n >> 32 & 0xff);
		b[2] = (byte) (n >> 40 & 0xff);
		b[1] = (byte) (n >> 48 & 0xff);
		b[0] = (byte) (n >> 56 & 0xff);
		return b;
	}

	public static void longToBytes(long n, byte[] array, int offset) {
		array[7 + offset] = (byte) (n & 0xff);
		array[6 + offset] = (byte) (n >> 8 & 0xff);
		array[5 + offset] = (byte) (n >> 16 & 0xff);
		array[4 + offset] = (byte) (n >> 24 & 0xff);
		array[3 + offset] = (byte) (n >> 32 & 0xff);
		array[2 + offset] = (byte) (n >> 40 & 0xff);
		array[1 + offset] = (byte) (n >> 48 & 0xff);
		array[0 + offset] = (byte) (n >> 56 & 0xff);
	}

	public static long bytesToLong(byte[] array) {
		return ((((long) array[0] & 0xff) << 56) | (((long) array[1] & 0xff) << 48) | (((long) array[2] & 0xff) << 40)
				| (((long) array[3] & 0xff) << 32) | (((long) array[4] & 0xff) << 24) | (((long) array[5] & 0xff) << 16)
				| (((long) array[6] & 0xff) << 8) | (((long) array[7] & 0xff) << 0));
	}

	public static long bytesToLong(byte[] array, int offset) {
		return ((((long) array[offset + 0] & 0xff) << 56) | (((long) array[offset + 1] & 0xff) << 48)
				| (((long) array[offset + 2] & 0xff) << 40) | (((long) array[offset + 3] & 0xff) << 32)
				| (((long) array[offset + 4] & 0xff) << 24) | (((long) array[offset + 5] & 0xff) << 16)
				| (((long) array[offset + 6] & 0xff) << 8) | (((long) array[offset + 7] & 0xff) << 0));
	}

	public static byte[] intToBytes(int n) {
		byte[] b = new byte[4];
		b[3] = (byte) (n & 0xff);
		b[2] = (byte) (n >> 8 & 0xff);
		b[1] = (byte) (n >> 16 & 0xff);
		b[0] = (byte) (n >> 24 & 0xff);
		return b;
	}

	public static void intToBytes(int n, byte[] array, int offset) {
		array[3 + offset] = (byte) (n & 0xff);
		array[2 + offset] = (byte) (n >> 8 & 0xff);
		array[1 + offset] = (byte) (n >> 16 & 0xff);
		array[offset] = (byte) (n >> 24 & 0xff);
	}

	public static int bytesToInt(byte b[]) {
		return b[3] & 0xff | (b[2] & 0xff) << 8 | (b[1] & 0xff) << 16 | (b[0] & 0xff) << 24;
	}

	public static int bytesToInt(byte b[], int offset) {
		return b[offset + 3] & 0xff | (b[offset + 2] & 0xff) << 8 | (b[offset + 1] & 0xff) << 16
				| (b[offset] & 0xff) << 24;
	}

	public static byte[] uintToBytes(long n) {
		byte[] b = new byte[4];
		b[3] = (byte) (n & 0xff);
		b[2] = (byte) (n >> 8 & 0xff);
		b[1] = (byte) (n >> 16 & 0xff);
		b[0] = (byte) (n >> 24 & 0xff);

		return b;
	}

	public static byte[] uintToLHBytes(long n) {
		byte[] b = new byte[4];
		b[0] = (byte) (n & 0xff);
		b[1] = (byte) (n >> 8 & 0xff);
		b[2] = (byte) (n >> 16 & 0xff);
		b[3] = (byte) (n >> 24 & 0xff);

		return b;
	}

	public static void uintToBytes(long n, byte[] array, int offset) {
		array[3 + offset] = (byte) (n);
		array[2 + offset] = (byte) (n >> 8 & 0xff);
		array[1 + offset] = (byte) (n >> 16 & 0xff);
		array[offset] = (byte) (n >> 24 & 0xff);
	}

	public static long bytesToUint(byte[] array) {
		return ((long) (array[3] & 0xff)) | ((long) (array[2] & 0xff)) << 8 | ((long) (array[1] & 0xff)) << 16
				| ((long) (array[0] & 0xff)) << 24;
	}

	public static long bytesToUint_LH(byte[] array) {
		return ((long) (array[0] & 0xff)) | ((long) (array[1] & 0xff)) << 8 | ((long) (array[2] & 0xff)) << 16
				| ((long) (array[3] & 0xff)) << 24;
	}

	public static long bytesToUint(byte[] array, int offset) {
		return ((long) (array[offset + 3] & 0xff)) | ((long) (array[offset + 2] & 0xff)) << 8
				| ((long) (array[offset + 1] & 0xff)) << 16 | ((long) (array[offset] & 0xff)) << 24;
	}

	public static byte[] shortToBytes(short n) {
		byte[] b = new byte[2];
		b[1] = (byte) (n & 0xff);
		b[0] = (byte) ((n >> 8) & 0xff);
		return b;
	}

	public static void shortToBytes(short n, byte[] array, int offset) {
		array[offset + 1] = (byte) (n & 0xff);
		array[offset] = (byte) ((n >> 8) & 0xff);
	}

	public static short bytesToShort(byte[] b) {
		return (short) (b[1] & 0xff | (b[0] & 0xff) << 8);
	}

	public static short bytesToShort(byte[] b, int offset) {
		return (short) (b[offset + 1] & 0xff | (b[offset] & 0xff) << 8);
	}

	public static byte[] ushortToBytes(int n) {
		byte[] b = new byte[2];
		b[1] = (byte) (n & 0xff);
		b[0] = (byte) ((n >> 8) & 0xff);
		return b;
	}

	public static void ushortToBytes(int n, byte[] array, int offset) {
		array[offset + 1] = (byte) (n & 0xff);
		array[offset] = (byte) ((n >> 8) & 0xff);
	}

	public static int bytesToUshort(byte b[]) {
		return b[1] & 0xff | (b[0] & 0xff) << 8;
	}

	public static int bytesToUshort(byte b[], int offset) {
		return b[offset + 1] & 0xff | (b[offset] & 0xff) << 8;
	}

	public static byte[] ubyteToBytes(int n) {
		byte[] b = new byte[1];
		b[0] = (byte) (n & 0xff);
		return b;
	}

	public static void ubyteToBytes(int n, byte[] array, int offset) {
		array[0] = (byte) (n & 0xff);
	}

	public static int bytesToUbyte(byte[] array) {
		return array[0] & 0xff;
	}

	public static int bytesToUbyte(byte[] array, int offset) {
		return array[offset] & 0xff;
	}

	public static String longToIp(long ipLong) {
		StringBuilder sb = new StringBuilder();
		sb.append(ipLong >>> 24);
		sb.append(".");
		sb.append(String.valueOf((ipLong & 0x00FFFFFF) >>> 16));
		sb.append(".");
		sb.append(String.valueOf((ipLong & 0x0000FFFF) >>> 8));
		sb.append(".");
		sb.append(String.valueOf(ipLong & 0x000000FF));
		return sb.toString();
	}

	public static long ipToLong(String ipString) {
		long result = 0;
		java.util.StringTokenizer token = new java.util.StringTokenizer(ipString, ".");
		result += Long.parseLong(token.nextToken()) << 24;
		result += Long.parseLong(token.nextToken()) << 16;
		result += Long.parseLong(token.nextToken()) << 8;
		result += Long.parseLong(token.nextToken());
		return result;
	}

	// 取字节的16进制字符串
	public static String getHexStr(byte[] bs) {
		String retStr = "";
		for (int i = 0; i < bs.length; i++) {
			if (Integer.toHexString((int) bs[i]).length() > 1) {
				retStr += Integer.toHexString((int) bs[i]).substring(Integer.toHexString((int) bs[i]).length() - 2);
			} else {
				retStr += "0"
						+ Integer.toHexString((int) bs[i]).substring(Integer.toHexString((int) bs[i]).length() - 1);
			}
		}
		return retStr;
	}

	public static String getStr(byte[] bRefArr) { 
    	int length=0;
    	for (length=0;length<bRefArr.length;length++)
    	{
    		if (bRefArr[length]==0)
    	    break;
    	}
        byte[] temp = new byte[length];
        for (int i=0;i<length;i++)
        {
        	temp[i]=bRefArr[i];
        }
        String tempStr="";
		try {
			tempStr = new String(temp,0,temp.length,"GBK");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return tempStr; 
    } 

	public static byte[] hexStringToBytes(String hexString) {
		if (hexString == null || hexString.equals("")) {
			return null;
		}
		hexString = hexString.toUpperCase();
		int length = hexString.length() / 2;
		char[] hexChars = hexString.toCharArray();
		byte[] d = new byte[length];
		for (int i = 0; i < length; i++) {
			int pos = i * 2;
			d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
		}
		return d;
	}
	
	/**
	 * rstr:smgp用到 bcd编码 <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br/>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br/>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br/>
	 *
	 * @author LiHao
	 * @param hex
	 * @return
	 * @since JDK 1.6
	 */
	public static byte[] rstr(String hex) {
        int length = hex.length();
        byte[] bHex = new byte[length/2];
        String temp = null;
        int t = 0;
        for (int i=0; i<length; i++) {
            temp = "" + hex.charAt(i) + hex.charAt(++i);
            bHex[t++] = (byte)Integer.parseInt(temp, 16);
        }
        return bHex;
    }
	public static String rhex(byte[] in) {
		DataInputStream data = new DataInputStream(new ByteArrayInputStream(in));
		String str = "";
		try {
			for (int j = 0; j < in.length; j++) {
				String tmp = Integer.toHexString(data.readUnsignedByte());
				if (tmp.length() == 1) {
					tmp = "0" + tmp;
				}
				str = str + tmp;
			}
		} catch (Exception ex) {
		}
		return str;
	}
	private static byte charToByte(char c) {
		return (byte) "0123456789ABCDEF".indexOf(c);
	}

	public static String stringToAscii(String value) {
		StringBuffer sbu = new StringBuffer();
		char[] chars = value.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			if (i != chars.length - 1) {
				sbu.append((int) chars[i]).append(",");
			} else {
				sbu.append((int) chars[i]);
			}
		}
		return sbu.toString();
	}
    public static int oneByte2Int(byte byteNum) {  
                   //针对正数的int  
        return byteNum >= 0 ? byteNum : (128 + (128 + byteNum));
    }  
	// char 类型、 float、double 类型和 byte[] 数组之间的转换关系还需继续研究实现。
	public static void main(String args[]) {
		/*
		 * if(args.length!=1) { System.out.println("请按下列方式执行："+
		 * "java ByteConvert 123456789"); System.exit(0); } System.out.println(
		 * "java ByteConvert begein ! agrs.length="
		 * +args.length+",agrs="+args[0]); // String a = args[0];
		 * 
		 * // String a ="3704525714"; //
		 * System.out.println("ip="+longToIp(Long.parseLong(a))+",int="+a);
		 */
		String a = "FFFFFFFF";
		// System.out.println("ip=" + longToIp(Long.parseLong(a)) + ",int=" +
		// a);
		// String b = "0.255.255.255";
		// String b ="1.0.0.0";
		// System.out.println("ip=" + b + ",int=" + ipToLong(b));
		// System.out.println(String.);
	}
}
