/**
 * Project Name:demo
 * File Name:SmppMsgDeliver.java
 * Package Name:com.cloud.demo.smpp.msg
 * Date:2016年5月3日下午7:39:37
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.smpp.msg;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloud.demo.smgp.SMPPIO;
import com.cloud.demo.utils.ByteConvert;

/**
 * ClassName:SmppMsgDeliver <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年5月3日 下午7:39:37 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SmppMsgDeliver extends SmppMsgHead {
	private static Logger logger = LoggerFactory.getLogger(SmppMsgDeliver.class);
	private int result;
	private String serviceType;
	private int sourceAddrTon;
	private int sourceAddrNpi;
	private String sourceAddr;
	private int destAddrTon;
	private int destAddrNpi;
	private String destAddr;
	private int esmClass;
	private int protocolId;
	private int priorityFlag;
	private String scheduleDeliveryTime; //NULL
	private String validityPeriod; //NULL
	private Integer registeredDelivery;
	private Integer replaceIfPresentFlag;
	private Integer dataCoding; 
	private Integer smDefaultMsgId;
	private Integer smLength ;
	private String shortMessage;
	
	
	
	private byte msgState;
	private String msgIdReport;
	
	
	/**
	 * serviceType.
	 *
	 * @return  the serviceType
	 * @since   JDK 1.6
	 */
	public String getServiceType() {
		return serviceType;
	}
	/**
	 * sourceAddrTon.
	 *
	 * @return  the sourceAddrTon
	 * @since   JDK 1.6
	 */
	public int getSourceAddrTon() {
		return sourceAddrTon;
	}
	/**
	 * sourceAddrNpi.
	 *
	 * @return  the sourceAddrNpi
	 * @since   JDK 1.6
	 */
	public int getSourceAddrNpi() {
		return sourceAddrNpi;
	}
	/**
	 * sourceAddr.
	 *
	 * @return  the sourceAddr
	 * @since   JDK 1.6
	 */
	public String getSourceAddr() {
		return sourceAddr;
	}
	/**
	 * destAddrTon.
	 *
	 * @return  the destAddrTon
	 * @since   JDK 1.6
	 */
	public int getDestAddrTon() {
		return destAddrTon;
	}
	/**
	 * destAddrNpi.
	 *
	 * @return  the destAddrNpi
	 * @since   JDK 1.6
	 */
	public int getDestAddrNpi() {
		return destAddrNpi;
	}
	/**
	 * destAddr.
	 *
	 * @return  the destAddr
	 * @since   JDK 1.6
	 */
	public String getDestAddr() {
		return destAddr;
	}
	/**
	 * esmClass.
	 *
	 * @return  the esmClass
	 * @since   JDK 1.6
	 */
	public int getEsmClass() {
		return esmClass;
	}
	/**
	 * protocolId.
	 *
	 * @return  the protocolId
	 * @since   JDK 1.6
	 */
	public int getProtocolId() {
		return protocolId;
	}
	/**
	 * priorityFlag.
	 *
	 * @return  the priorityFlag
	 * @since   JDK 1.6
	 */
	public int getPriorityFlag() {
		return priorityFlag;
	}
	/**
	 * scheduleDeliveryTime.
	 *
	 * @return  the scheduleDeliveryTime
	 * @since   JDK 1.6
	 */
	public String getScheduleDeliveryTime() {
		return scheduleDeliveryTime;
	}
	/**
	 * validityPeriod.
	 *
	 * @return  the validityPeriod
	 * @since   JDK 1.6
	 */
	public String getValidityPeriod() {
		return validityPeriod;
	}
	/**
	 * registeredDelivery.
	 *
	 * @return  the registeredDelivery
	 * @since   JDK 1.6
	 */
	public Integer getRegisteredDelivery() {
		return registeredDelivery;
	}
	/**
	 * replaceIfPresentFlag.
	 *
	 * @return  the replaceIfPresentFlag
	 * @since   JDK 1.6
	 */
	public Integer getReplaceIfPresentFlag() {
		return replaceIfPresentFlag;
	}
	/**
	 * dataCoding.
	 *
	 * @return  the dataCoding
	 * @since   JDK 1.6
	 */
	public Integer getDataCoding() {
		return dataCoding;
	}
	/**
	 * smDefaultMsgId.
	 *
	 * @return  the smDefaultMsgId
	 * @since   JDK 1.6
	 */
	public Integer getSmDefaultMsgId() {
		return smDefaultMsgId;
	}
	/**
	 * smLength.
	 *
	 * @return  the smLength
	 * @since   JDK 1.6
	 */
	public Integer getSmLength() {
		return smLength;
	}
	/**
	 * shortMessage.
	 *
	 * @return  the shortMessage
	 * @since   JDK 1.6
	 */
	public String getShortMessage() {
		return shortMessage;
	}
	/**
	 * serviceType.
	 *
	 * @param   serviceType    the serviceType to set
	 * @since   JDK 1.6
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	/**
	 * sourceAddrTon.
	 *
	 * @param   sourceAddrTon    the sourceAddrTon to set
	 * @since   JDK 1.6
	 */
	public void setSourceAddrTon(int sourceAddrTon) {
		this.sourceAddrTon = sourceAddrTon;
	}
	/**
	 * sourceAddrNpi.
	 *
	 * @param   sourceAddrNpi    the sourceAddrNpi to set
	 * @since   JDK 1.6
	 */
	public void setSourceAddrNpi(int sourceAddrNpi) {
		this.sourceAddrNpi = sourceAddrNpi;
	}
	/**
	 * sourceAddr.
	 *
	 * @param   sourceAddr    the sourceAddr to set
	 * @since   JDK 1.6
	 */
	public void setSourceAddr(String sourceAddr) {
		this.sourceAddr = sourceAddr;
	}
	/**
	 * destAddrTon.
	 *
	 * @param   destAddrTon    the destAddrTon to set
	 * @since   JDK 1.6
	 */
	public void setDestAddrTon(int destAddrTon) {
		this.destAddrTon = destAddrTon;
	}
	/**
	 * destAddrNpi.
	 *
	 * @param   destAddrNpi    the destAddrNpi to set
	 * @since   JDK 1.6
	 */
	public void setDestAddrNpi(int destAddrNpi) {
		this.destAddrNpi = destAddrNpi;
	}
	/**
	 * destAddr.
	 *
	 * @param   destAddr    the destAddr to set
	 * @since   JDK 1.6
	 */
	public void setDestAddr(String destAddr) {
		this.destAddr = destAddr;
	}
	/**
	 * esmClass.
	 *
	 * @param   esmClass    the esmClass to set
	 * @since   JDK 1.6
	 */
	public void setEsmClass(int esmClass) {
		this.esmClass = esmClass;
	}
	/**
	 * protocolId.
	 *
	 * @param   protocolId    the protocolId to set
	 * @since   JDK 1.6
	 */
	public void setProtocolId(int protocolId) {
		this.protocolId = protocolId;
	}
	/**
	 * priorityFlag.
	 *
	 * @param   priorityFlag    the priorityFlag to set
	 * @since   JDK 1.6
	 */
	public void setPriorityFlag(int priorityFlag) {
		this.priorityFlag = priorityFlag;
	}
	/**
	 * scheduleDeliveryTime.
	 *
	 * @param   scheduleDeliveryTime    the scheduleDeliveryTime to set
	 * @since   JDK 1.6
	 */
	public void setScheduleDeliveryTime(String scheduleDeliveryTime) {
		this.scheduleDeliveryTime = scheduleDeliveryTime;
	}
	/**
	 * validityPeriod.
	 *
	 * @param   validityPeriod    the validityPeriod to set
	 * @since   JDK 1.6
	 */
	public void setValidityPeriod(String validityPeriod) {
		this.validityPeriod = validityPeriod;
	}
	/**
	 * registeredDelivery.
	 *
	 * @param   registeredDelivery    the registeredDelivery to set
	 * @since   JDK 1.6
	 */
	public void setRegisteredDelivery(Integer registeredDelivery) {
		this.registeredDelivery = registeredDelivery;
	}
	/**
	 * replaceIfPresentFlag.
	 *
	 * @param   replaceIfPresentFlag    the replaceIfPresentFlag to set
	 * @since   JDK 1.6
	 */
	public void setReplaceIfPresentFlag(Integer replaceIfPresentFlag) {
		this.replaceIfPresentFlag = replaceIfPresentFlag;
	}
	/**
	 * dataCoding.
	 *
	 * @param   dataCoding    the dataCoding to set
	 * @since   JDK 1.6
	 */
	public void setDataCoding(Integer dataCoding) {
		this.dataCoding = dataCoding;
	}
	/**
	 * smDefaultMsgId.
	 *
	 * @param   smDefaultMsgId    the smDefaultMsgId to set
	 * @since   JDK 1.6
	 */
	public void setSmDefaultMsgId(Integer smDefaultMsgId) {
		this.smDefaultMsgId = smDefaultMsgId;
	}
	/**
	 * smLength.
	 *
	 * @param   smLength    the smLength to set
	 * @since   JDK 1.6
	 */
	public void setSmLength(Integer smLength) {
		this.smLength = smLength;
	}
	/**
	 * shortMessage.
	 *
	 * @param   shortMessage    the shortMessage to set
	 * @since   JDK 1.6
	 */
	public void setShortMessage(String shortMessage) {
		this.shortMessage = shortMessage;
	}
	/**
	 * result.
	 *
	 * @return  the result
	 * @since   JDK 1.6
	 */
	public int getResult() {
		return result;
	}
	/**
	 * result.
	 *
	 * @param   result    the result to set
	 * @since   JDK 1.6
	 */
	public void setResult(int result) {
		this.result = result;
	}
	
	
	/**
	 * msgState.
	 *
	 * @return  the msgState
	 * @since   JDK 1.6
	 */
	public byte getMsgState() {
		return msgState;
	}
	/**
	 * msgIdReport.
	 *
	 * @return  the msgIdReport
	 * @since   JDK 1.6
	 */
	public String getMsgIdReport() {
		return msgIdReport;
	}
	/**
	 * msgState.
	 *
	 * @param   msgState    the msgState to set
	 * @since   JDK 1.6
	 */
	public void setMsgState(byte msgState) {
		this.msgState = msgState;
	}
	/**
	 * msgIdReport.
	 *
	 * @param   msgIdReport    the msgIdReport to set
	 * @since   JDK 1.6
	 */
	public void setMsgIdReport(String msgIdReport) {
		this.msgIdReport = msgIdReport;
	}
	public SmppMsgDeliver(){
		
	}
	public SmppMsgDeliver(int packetLength,int commandId,int commandStatus,int sequenceId,byte[] data) throws UnsupportedEncodingException{
		this.setTotalLength(packetLength);
		this.setCommandId(commandId);
		this.setCommandStatus(commandStatus);
		this.setSequenceId(sequenceId);
		int offset=0;
		serviceType=SMPPIO.readCString(data, offset);
		logger.debug(offset+"serviceType:"+serviceType);
		offset+=serviceType.length()+1;
		sourceAddrTon=SMPPIO.bytesToInt(data, offset++, 1);
		logger.debug(offset+"sourceAddrTon:"+sourceAddrTon);
		sourceAddrNpi=SMPPIO.bytesToInt(data, offset++, 1);
		logger.debug(offset+"sourceAddrNpi:"+sourceAddrNpi);
		sourceAddr=SMPPIO.readCString(data, offset);
		offset+=sourceAddr.length()+1;
		logger.debug(offset+"sourceAddr:"+sourceAddr);
		destAddrTon=SMPPIO.bytesToInt(data, offset++, 1);
		logger.debug(offset+"destAddrTon:"+destAddrTon);
		destAddrNpi=SMPPIO.bytesToInt(data, offset++, 1);
		logger.debug(offset+"destAddrNpi:"+destAddrNpi);
		destAddr=SMPPIO.readCString(data, offset);
		offset+=destAddr.length()+1;
		logger.debug(offset+"destAddr:"+destAddr);
		esmClass=SMPPIO.bytesToInt(data, offset++, 1);
		logger.debug(offset+"esmClass:"+esmClass);
		protocolId=SMPPIO.bytesToInt(data, offset++, 1);
		logger.debug(offset+"protocolId:"+protocolId);
		priorityFlag=SMPPIO.bytesToInt(data, offset++, 1);
		logger.debug(offset+"priorityFlag:"+priorityFlag);
		scheduleDeliveryTime=SMPPIO.readCString(data, offset);
		logger.debug(offset+"scheduleDeliveryTime:"+scheduleDeliveryTime);
		validityPeriod=SMPPIO.readCString(data, offset++);
		offset+=validityPeriod.length()+1;
		logger.debug(offset+"validityPeriod:"+validityPeriod);
		registeredDelivery=SMPPIO.bytesToInt(data, offset++, 1);
		logger.debug(offset+"registeredDelivery:"+registeredDelivery);
		replaceIfPresentFlag=SMPPIO.bytesToInt(data, offset++, 1);
		logger.debug(offset+"replaceIfPresentFlag:"+replaceIfPresentFlag);
		dataCoding=SMPPIO.bytesToInt(data, offset++, 1);
		logger.debug(offset+"dataCoding:"+dataCoding);
		smDefaultMsgId=SMPPIO.bytesToInt(data, offset++, 1);
		logger.debug(offset+"smDefaultMsgId:"+smDefaultMsgId);
		smLength=SMPPIO.bytesToInt(data, offset++, 1);
		logger.debug(offset+"smLength:"+smLength);
		if(smLength>0){
			if(esmClass==4) {
				String fmtStr=this.dataCoding==8?"UTF-16BE":"gbk";
				String msg_Id_report="";
				String sub="";
				String vrd="";
				String submit_time="";
				String done_time="";
				String stat="";
				String err="";
				String txt="";
				byte[] msgcontent=new byte[smLength];
				System.arraycopy(data,offset,msgcontent,0,smLength);
				String msg=SMPPIO.readString(msgcontent, 0, smLength);
				logger.info("msg:"+msg);
				byte[] msgIdReportByte=new byte[32];
				System.arraycopy(msgcontent,3,msgIdReportByte,0,32);
				msg_Id_report=new String(msgIdReportByte,fmtStr);
				logger.debug("msg_Id_report:"+msg_Id_report);
				this.msgIdReport=msg_Id_report;
				byte[] subByte=new byte[3];//
				System.arraycopy(msgcontent,36+4,subByte,0,3);
				sub=new String(subByte,fmtStr);
				logger.debug("sub:"+sub);
				byte[] vrdByte=new byte[3]; //Dlvrd
				System.arraycopy(msgcontent,44+6,vrdByte,0,3);
				vrd=new String(vrdByte,fmtStr);
				logger.debug("vrd:"+vrd);
				byte[] submit_timeByte=new byte[10];//Submit_date
				System.arraycopy(msgcontent,54+12,submit_timeByte,0,10);
				submit_time=new String(submit_timeByte,fmtStr);
				logger.debug("submit_time:"+submit_time);
				byte[] done_timeByte=new byte[10];//done_date
				System.arraycopy(msgcontent,77+10,done_timeByte,0,10);
				done_time=new String(done_timeByte,fmtStr);
				logger.debug("done_time:"+done_time);
				byte[] statByte=new byte[7]; //stat
				System.arraycopy(msgcontent,98+5,statByte,0,7);
				stat=new String(statByte,fmtStr);
				logger.debug("stat:"+stat);
				byte[] errByte=new byte[3];
				System.arraycopy(msgcontent,111+4,errByte,0,3);
				err=new String(errByte,fmtStr);
				logger.debug("err:"+err);
				byte[] txtByte=new byte[20];//Txt
				System.arraycopy(msgcontent,119+5,txtByte,0,20);
				txt=new String(txtByte,fmtStr);
				this.result=0;//正确

				/*int msg_offset=0;
				String msgId=SMPPIO.readCString(msg_ContentByte, msg_offset);
				msg_offset+=msgId.length()+1;
				logger.debug(msg_offset+"msgId:"+msgId);
				String sub=SMPPIO.readString(msg_ContentByte, msg_offset, msg_offset+3);
				msg_offset+=3;
				logger.debug(msg_offset+"sub:"+sub);
				String dlvrd=SMPPIO.readString(msg_ContentByte, msg_offset, msg_offset+3);
				msg_offset+=3;
				logger.debug(msg_offset+"dlvrd:"+dlvrd);
				String submitDate=SMPPIO.readString(msg_ContentByte, msg_offset, msg_offset+10);
				msg_offset+=10;
				logger.debug(msg_offset+"submitDate:"+submitDate);
				String doneDate=SMPPIO.readString(msg_ContentByte, msg_offset, msg_offset+10);
				msg_offset+=10;
				logger.debug(msg_offset+"doneDate:"+doneDate);
				String stat=SMPPIO.readString(msg_ContentByte, msg_offset, msg_offset+7);
				msg_offset+=7;
				logger.debug(msg_offset+"stat:"+stat);
				String err=SMPPIO.readString(msg_ContentByte, msg_offset, msg_offset+3);
				msg_offset+=3;
				logger.debug(msg_offset+"err"+err);
				String text=SMPPIO.readString(msg_ContentByte, msg_offset, msg_offset+20);
				logger.debug(msg_offset+"text"+text+";#");*/
			}else {
				byte[] msgContentByte = new byte[smLength];
				byte byte1 = 0;
				byte byte2 = 0;
				byte byte3 = 0;
				if (smLength >= 3) {
					byte1 = msgContentByte[0];
					byte2 = msgContentByte[1];
					byte3 = msgContentByte[2];
				}
				if (byte1 == 5 && byte2 == 0 && byte3 == 3) { // 判断是否为长短信
					//byte sequenceByte = msgContentByte[3];
					byte totalSeqNumByte = msgContentByte[4];
					byte segNumByte = msgContentByte[5];
					// 去除消息头
					byte[] msgBytes = new byte[msgContentByte.length - 6];
					System.arraycopy(msgContentByte, 6, msgBytes, 0, smLength - 6);
					String msgContent = "(" + segNumByte + "/" + totalSeqNumByte + "):" +new String(msgBytes, "ISO-10646-UCS-2");;
					logger.info("收到一个上行长短信，共" + totalSeqNumByte + "条，此条是第" + segNumByte + "条,content:" + msgContent);
				} else {
					//shortMessage=SMPPIO.readString(data, offset, smLength);
					byte[] msg_ContentByte=new byte[smLength];
					System.arraycopy(data,offset,msg_ContentByte,0,smLength);
					try {
						this.shortMessage=new String(msg_ContentByte, "ISO-10646-UCS-2");
					} catch (UnsupportedEncodingException e) {

						// TODO Auto-generated catch block
						e.printStackTrace();

					}
					logger.debug(offset+"shortMessage:"+shortMessage);
				}
//
				offset+=smLength+1;

			}
		}
		int tlvLen=packetLength-16-offset;
		if(tlvLen>0){
			byte[] tlv=new byte[tlvLen];
	    	while(offset<data.length){
	    		byte[] tagBytes=new byte[2];
				System.arraycopy(data,offset, tagBytes, 0,2);
				offset+=2;
				byte[] lenBytes=new byte[2];
				System.arraycopy(data,offset, lenBytes, 0,2);
				offset+=2;
				short tag= ByteConvert.bytesToShort(tagBytes);
				short len=ByteConvert.bytesToShort(lenBytes);
				switch(tag){
					case 0x0204:	
						byte[] umrBytes=new byte[len];
						System.arraycopy(data,offset, umrBytes, 0,len);
						ByteConvert.bytesToShort(lenBytes);
						offset+=len;
						break;
					case 0x020A:	//source_port
						offset+=len;
						break;	
					case 0x0427:	//message_state
						msgState=data[offset];
						offset+=len;
						break;
					case 0x001E:	//receipted_message_id 
						msgIdReport=SMPPIO.readCString(data, offset);
						offset+=len;
						break;
					default:
						offset+=len;
						break;
				}
	    	}
		}
		
		if(esmClass==4){
			//状态报告
			
		}else{
			//上行内容
			
		}
	}
	
	public static void  main(String[] args) {
		byte[] b=new byte[4];
		b[0]=-128;
		b[1]=0;
		b[2]=0;
		b[3]=5;
		int a=SMPPIO.bytesToInt(b, 0, 4);
		System.out.println("dsa:"+a);
	}
}

