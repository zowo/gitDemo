/**
 * Project Name:demo
 * File Name:SmppMsgSubmit.java
 * Package Name:com.cloud.demo.smpp.msg
 * Date:2016年4月28日下午6:31:50
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.smpp.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloud.demo.smgp.SMPPIO;

/**
 * ClassName:SmppMsgSubmit <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月28日 下午6:31:50 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SmppMsgSubmit extends SmppMsgHead {
	private static Logger logger = LoggerFactory.getLogger(SmppMsgSubmit.class);
	private String serviceType;  //6位
	private byte sourceAddrTon;
	private byte sourceAddrNpi;
	private String sourceAddr;   //21位
	private byte destAddrTon;
	private byte destAddrNpi;
	private String destinationAddr;  //21位
	
	private byte esmClass;
	private byte protocolId;
	private byte priorityFlag;
	private String scheduleDeliveryTime;
	private String validityPeriod;
	private byte registeredDelivery;
	private byte replaceIfPresentFlag;
	private byte dataCoding;
	private byte smDefaultMsgId;
	private byte smLength;
	private byte[] shortMessage;
	public int getBodyLength() {
		int len=((serviceType != null) ? serviceType.length() : 0)
				+((sourceAddr != null) ? sourceAddr.length() : 0)
				+((destinationAddr != null) ? destinationAddr.length() : 0)
				+((scheduleDeliveryTime != null) ? scheduleDeliveryTime.length() : 0)
				+((validityPeriod != null) ? validityPeriod.length() : 0)
				+((shortMessage != null) ? shortMessage.length : 0);
		// 3 1-byte integers, 6 c-strings
		return len+12+6;
	}
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			SMPPIO.writeInt(16+this.getBodyLength(), 4, dous);
	        SMPPIO.writeInt(this.getCommandId(), 4, dous);
	        SMPPIO.writeInt(this.getCommandStatus(), 4, dous);
	        SMPPIO.writeInt(this.getSequenceId(), 4, dous);
	        SMPPIO.writeCString(this.serviceType, dous);
	        SMPPIO.writeInt(this.sourceAddrTon, 1, dous);
	        SMPPIO.writeInt(this.sourceAddrNpi, 1, dous);
	        SMPPIO.writeCString(this.sourceAddr, dous);
	        SMPPIO.writeInt(this.destAddrTon,1, dous);
	        SMPPIO.writeInt(this.destAddrNpi,1,dous);
	        SMPPIO.writeCString(this.destinationAddr, dous);
	        SMPPIO.writeInt(this.esmClass,1,dous);
	        SMPPIO.writeInt(this.protocolId,1,dous);
	        SMPPIO.writeInt(this.priorityFlag,1,dous);
	        SMPPIO.writeCString(this.scheduleDeliveryTime, dous);
	        SMPPIO.writeCString(this.validityPeriod, dous);
	        SMPPIO.writeInt(this.registeredDelivery,1,dous);
	        SMPPIO.writeInt(this.replaceIfPresentFlag,1,dous);
	        SMPPIO.writeInt(this.dataCoding,1,dous);
	        SMPPIO.writeInt(this.smDefaultMsgId,1,dous);
	        SMPPIO.writeInt(this.smLength,1,dous);
	        SMPPIO.writeByte(shortMessage, dous);
			dous.close();
		} catch (IOException e) {
			logger.error("封装链接二进制数组失败。");
			e.printStackTrace();
		}
		return bous.toByteArray();
	}
	/**
	 * serviceType.
	 *
	 * @return  the serviceType
	 * @since   JDK 1.6
	 */
	public String getServiceType() {
		return serviceType;
	}
	/**
	 * sourceAddrTon.
	 *
	 * @return  the sourceAddrTon
	 * @since   JDK 1.6
	 */
	public byte getSourceAddrTon() {
		return sourceAddrTon;
	}
	/**
	 * sourceAddrNpi.
	 *
	 * @return  the sourceAddrNpi
	 * @since   JDK 1.6
	 */
	public byte getSourceAddrNpi() {
		return sourceAddrNpi;
	}
	/**
	 * sourceAddr.
	 *
	 * @return  the sourceAddr
	 * @since   JDK 1.6
	 */
	public String getSourceAddr() {
		return sourceAddr;
	}
	/**
	 * destAddrTon.
	 *
	 * @return  the destAddrTon
	 * @since   JDK 1.6
	 */
	public byte getDestAddrTon() {
		return destAddrTon;
	}
	/**
	 * destAddrNpi.
	 *
	 * @return  the destAddrNpi
	 * @since   JDK 1.6
	 */
	public byte getDestAddrNpi() {
		return destAddrNpi;
	}
	/**
	 * destinationAddr.
	 *
	 * @return  the destinationAddr
	 * @since   JDK 1.6
	 */
	public String getDestinationAddr() {
		return destinationAddr;
	}
	/**
	 * esmClass.
	 *
	 * @return  the esmClass
	 * @since   JDK 1.6
	 */
	public byte getEsmClass() {
		return esmClass;
	}
	/**
	 * protocolId.
	 *
	 * @return  the protocolId
	 * @since   JDK 1.6
	 */
	public byte getProtocolId() {
		return protocolId;
	}
	/**
	 * priorityFlag.
	 *
	 * @return  the priorityFlag
	 * @since   JDK 1.6
	 */
	public byte getPriorityFlag() {
		return priorityFlag;
	}
	/**
	 * scheduleDeliveryTime.
	 *
	 * @return  the scheduleDeliveryTime
	 * @since   JDK 1.6
	 */
	public String getScheduleDeliveryTime() {
		return scheduleDeliveryTime;
	}
	/**
	 * validityPeriod.
	 *
	 * @return  the validityPeriod
	 * @since   JDK 1.6
	 */
	public String getValidityPeriod() {
		return validityPeriod;
	}
	/**
	 * registeredDelivery.
	 *
	 * @return  the registeredDelivery
	 * @since   JDK 1.6
	 */
	public byte getRegisteredDelivery() {
		return registeredDelivery;
	}
	/**
	 * replaceIfPresentFlag.
	 *
	 * @return  the replaceIfPresentFlag
	 * @since   JDK 1.6
	 */
	public byte getReplaceIfPresentFlag() {
		return replaceIfPresentFlag;
	}
	/**
	 * dataCoding.
	 *
	 * @return  the dataCoding
	 * @since   JDK 1.6
	 */
	public byte getDataCoding() {
		return dataCoding;
	}
	/**
	 * smDefaultMsgId.
	 *
	 * @return  the smDefaultMsgId
	 * @since   JDK 1.6
	 */
	public byte getSmDefaultMsgId() {
		return smDefaultMsgId;
	}
	/**
	 * smLength.
	 *
	 * @return  the smLength
	 * @since   JDK 1.6
	 */
	public byte getSmLength() {
		return smLength;
	}
	/**
	 * shortMessage.
	 *
	 * @return  the shortMessage
	 * @since   JDK 1.6
	 */
	public byte[] getShortMessage() {
		return shortMessage;
	}
	/**
	 * serviceType.
	 *
	 * @param   serviceType    the serviceType to set
	 * @since   JDK 1.6
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	/**
	 * sourceAddrTon.
	 *
	 * @param   sourceAddrTon    the sourceAddrTon to set
	 * @since   JDK 1.6
	 */
	public void setSourceAddrTon(byte sourceAddrTon) {
		this.sourceAddrTon = sourceAddrTon;
	}
	/**
	 * sourceAddrNpi.
	 *
	 * @param   sourceAddrNpi    the sourceAddrNpi to set
	 * @since   JDK 1.6
	 */
	public void setSourceAddrNpi(byte sourceAddrNpi) {
		this.sourceAddrNpi = sourceAddrNpi;
	}
	/**
	 * sourceAddr.
	 *
	 * @param   sourceAddr    the sourceAddr to set
	 * @since   JDK 1.6
	 */
	public void setSourceAddr(String sourceAddr) {
		this.sourceAddr = sourceAddr;
	}
	/**
	 * destAddrTon.
	 *
	 * @param   destAddrTon    the destAddrTon to set
	 * @since   JDK 1.6
	 */
	public void setDestAddrTon(byte destAddrTon) {
		this.destAddrTon = destAddrTon;
	}
	/**
	 * destAddrNpi.
	 *
	 * @param   destAddrNpi    the destAddrNpi to set
	 * @since   JDK 1.6
	 */
	public void setDestAddrNpi(byte destAddrNpi) {
		this.destAddrNpi = destAddrNpi;
	}
	/**
	 * destinationAddr.
	 *
	 * @param   destinationAddr    the destinationAddr to set
	 * @since   JDK 1.6
	 */
	public void setDestinationAddr(String destinationAddr) {
		this.destinationAddr = destinationAddr;
	}
	/**
	 * esmClass.
	 *
	 * @param   esmClass    the esmClass to set
	 * @since   JDK 1.6
	 */
	public void setEsmClass(byte esmClass) {
		this.esmClass = esmClass;
	}
	/**
	 * protocolId.
	 *
	 * @param   protocolId    the protocolId to set
	 * @since   JDK 1.6
	 */
	public void setProtocolId(byte protocolId) {
		this.protocolId = protocolId;
	}
	/**
	 * priorityFlag.
	 *
	 * @param   priorityFlag    the priorityFlag to set
	 * @since   JDK 1.6
	 */
	public void setPriorityFlag(byte priorityFlag) {
		this.priorityFlag = priorityFlag;
	}
	/**
	 * scheduleDeliveryTime.
	 *
	 * @param   scheduleDeliveryTime    the scheduleDeliveryTime to set
	 * @since   JDK 1.6
	 */
	public void setScheduleDeliveryTime(String scheduleDeliveryTime) {
		this.scheduleDeliveryTime = scheduleDeliveryTime;
	}
	/**
	 * validityPeriod.
	 *
	 * @param   validityPeriod    the validityPeriod to set
	 * @since   JDK 1.6
	 */
	public void setValidityPeriod(String validityPeriod) {
		this.validityPeriod = validityPeriod;
	}
	/**
	 * registeredDelivery.
	 *
	 * @param   registeredDelivery    the registeredDelivery to set
	 * @since   JDK 1.6
	 */
	public void setRegisteredDelivery(byte registeredDelivery) {
		this.registeredDelivery = registeredDelivery;
	}
	/**
	 * replaceIfPresentFlag.
	 *
	 * @param   replaceIfPresentFlag    the replaceIfPresentFlag to set
	 * @since   JDK 1.6
	 */
	public void setReplaceIfPresentFlag(byte replaceIfPresentFlag) {
		this.replaceIfPresentFlag = replaceIfPresentFlag;
	}
	/**
	 * dataCoding.
	 *
	 * @param   dataCoding    the dataCoding to set
	 * @since   JDK 1.6
	 */
	public void setDataCoding(byte dataCoding) {
		this.dataCoding = dataCoding;
	}
	/**
	 * smDefaultMsgId.
	 *
	 * @param   smDefaultMsgId    the smDefaultMsgId to set
	 * @since   JDK 1.6
	 */
	public void setSmDefaultMsgId(byte smDefaultMsgId) {
		this.smDefaultMsgId = smDefaultMsgId;
	}
	/**
	 * smLength.
	 *
	 * @param   smLength    the smLength to set
	 * @since   JDK 1.6
	 */
	public void setSmLength(byte smLength) {
		this.smLength = smLength;
	}
	/**
	 * shortMessage.
	 *
	 * @param   shortMessage    the shortMessage to set
	 * @since   JDK 1.6
	 */
	public void setShortMessage(byte[] shortMessage) {
		this.shortMessage = shortMessage;
	}
	
	
	
}

