/**
 * Project Name:demo
 * File Name:SmppMsgBindResp.java
 * Package Name:com.cloud.demo.smpp.msg
 * Date:2016年4月28日下午5:05:04
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.smpp.msg;

import com.cloud.demo.smgp.SMPPIO;

/**
 * ClassName:SmppMsgBindResp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月28日 下午5:05:04 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SmppMsgBindResp extends SmppMsgHead {
	private String systemId;
	
	/**
	 * systemId.
	 *
	 * @return  the systemId
	 * @since   JDK 1.6
	 */
	public String getSystemId() {
		return systemId;
	}
	/**
	 * systemId.
	 *
	 * @param   systemId    the systemId to set
	 * @since   JDK 1.6
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public SmppMsgBindResp(){
		
	}
	public SmppMsgBindResp(int packetLength,int commandId,int commandStatus,int sequenceId,byte[] data){
		this.setTotalLength(packetLength);
		this.setCommandId(commandId);
		this.setCommandStatus(commandStatus);
		this.setSequenceId(sequenceId);
		int offset=0;
		systemId=SMPPIO.readCString(data, offset);
	}
}

