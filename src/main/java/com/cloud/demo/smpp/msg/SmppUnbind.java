package com.cloud.demo.smpp.msg;

import com.cloud.demo.smgp.SMPPIO;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by dhc on 2017/2/6.
 */
public class SmppUnbind extends SmppMsgHead {

    public byte[] toByteArray(){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        try {
            SMPPIO.writeInt(16, 4, dos);
            SMPPIO.writeInt(this.getCommandId(), 4, dos);
            SMPPIO.writeInt(this.getCommandStatus(), 4, dos);
            SMPPIO.writeInt(this.getSequenceId(), 4, dos);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return baos.toByteArray();
    }
}
