package com.cloud.demo.smpp.msg;

public class SmppCommand {
	public static final int PeripheralID=5000;
	
	/**
	 * 包头长度。（协议采用定长包头，长度为4+4+4)
	 * 
     *Total_Length	4	Unsigned Integer	消息总长度(含消息头及消息体)
     *Command_Id	4	Unsigned Integer	命令或响应类型
     *Sequence_Id	4	Unsigned Integer	消息流水号,顺序累加,步长为1,循环使用（一对请求和应答消息的流水号必须相同）
	 */
	public static final int MsgHeaderLength = 16;
	public static final int MsgTotalLength=4;
	public static final int MsgCommandId=4;
	public static final int MsgCommandStatus=4;
	public static final int MsgSequenceId=4;
	
	public static final int BIND_TRANSCEIVER=0x00000009;
	public static final int BIND_TRANSCEIVER_RESP=0x80000009;//请求bind应答
	public static final int ENQUIRE_LINK=0x00000015;
	public static final int ENQUIRE_LINK_RESP=0x80000015;
	
	public static final int SUBMIT_SM=0x00000004;	//提交短信
	public static final int SUBMIT_SM_RESP=0x80000004;	//提交短信应答
	
	public static final int DELIVER_SM=0x00000005;	//短信下发
	public static final int DELIVER_SM_RESP=0x80000005;	//下发短信应答
	
	public static final int UNBIND=0x00000006;	//发送短信状态查询
	public static final int UNBIND_RESP=0x80000006;	//发送短信状态查询应答
}
