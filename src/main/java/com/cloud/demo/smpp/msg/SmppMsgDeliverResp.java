/**
 * Project Name:demo
 * File Name:SmppMsgDeliverResp.java
 * Package Name:com.cloud.demo.smpp.msg
 * Date:2016年5月3日下午7:40:23
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.smpp.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.cloud.demo.smgp.SMPPIO;

/**
 * ClassName:SmppMsgDeliverResp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年5月3日 下午7:40:23 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SmppMsgDeliverResp extends SmppMsgHead {
	private String messageId;
	public SmppMsgDeliverResp(){
		
	}
	public int getBodyLength() {
		int len=((messageId != null) ? messageId.length() : 0);
		return 1;
	}
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			SMPPIO.writeInt(16+this.getBodyLength(), 4, dous);
	        SMPPIO.writeInt(this.getCommandId(), 4, dous);
	        SMPPIO.writeInt(this.getCommandStatus(), 4, dous);
	        SMPPIO.writeInt(this.getSequenceId(), 4, dous);
	        SMPPIO.writeCString(this.messageId, dous);
			dous.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bous.toByteArray();
	}
	/**
	 * messageId.
	 *
	 * @return  the messageId
	 * @since   JDK 1.6
	 */
	public String getMessageId() {
		return messageId;
	}

	/**
	 * messageId.
	 *
	 * @param   messageId    the messageId to set
	 * @since   JDK 1.6
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	
}

