package com.cloud.demo.smpp.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.cloud.demo.smgp.SMPPIO;

/**
 * ClassName: MsgHead <br/>
 * Function: 所有请求的消息头 <br/>
 * totalLength 消息总长度<br/>
 * commandId 命令类型<br/>
 * sequenceId 消息流水号,顺序累加,步长为1,循环使用（一对请求和应答消息的流水号必须相同）<br/>
 * Unsigned Integer  	无符号整数<br/>
 * Integer	整数，可为正整数、负整数或零<br/>
 * Octet String	定长字符串，位数不足时，如果左补0则补ASCII表示的零以填充，如果右补0则补二进制的零以表示字符串的结束符
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2016年2月18日 下午5:22:41 <br/>
 * 
 * @author LiHao
 * @version 
 * @since JDK 1.6
 */
public class SmppMsgHead {
	private Logger logger=Logger.getLogger(SmppMsgHead.class);
	private int totalLength;//Unsigned Integer 消息总长度
	private int commandId;//Unsigned Integer 命令类型
	private int commandStatus;
	private int sequenceId;//Unsigned Integer 消息流水号,顺序累加,步长为1,循环使用（一对请求和应答消息的流水号必须相同）
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			SMPPIO.writeInt(16, 4, dous);
	        SMPPIO.writeInt(this.getCommandId(), 4, dous);
	        SMPPIO.writeInt(this.getCommandStatus(), 4, dous);
	        SMPPIO.writeInt(this.getSequenceId(), 4, dous);
			dous.close();
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("【封装SMPP消息头二进制数组失败】");
		}
		return bous.toByteArray();
	}

	public SmppMsgHead(){
		super();
	}
	public SmppMsgHead(int packetLength,int commandId,int commandStatus,int sequenceId,byte[] data){
		this.setTotalLength(packetLength);
		this.setCommandId(commandId);
		this.setCommandStatus(commandStatus);
		this.setSequenceId(sequenceId);
	}
	public int getTotalLength() {
		return totalLength;
	}
	public void setTotalLength(int totalLength) {
		this.totalLength = totalLength;
	}
	public int getCommandId() {
		return commandId;
	}
	public void setCommandId(int commandId) {
		this.commandId = commandId;
	}
	public int getSequenceId() {
		return sequenceId;
	}
	public void setSequenceId(int sequenceId) {
		this.sequenceId = sequenceId;
	}

	/**
	 * commandStatus.
	 *
	 * @return  the commandStatus
	 * @since   JDK 1.6
	 */
	public int getCommandStatus() {
		return commandStatus;
	}

	/**
	 * commandStatus.
	 *
	 * @param   commandStatus    the commandStatus to set
	 * @since   JDK 1.6
	 */
	public void setCommandStatus(int commandStatus) {
		this.commandStatus = commandStatus;
	}
	
}
