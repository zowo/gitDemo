/**
 * Project Name:demo
 * File Name:SmppMsgSubmitResp.java
 * Package Name:com.cloud.demo.smpp.msg
 * Date:2016年5月3日下午7:28:26
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.smpp.msg;

import com.cloud.demo.smgp.SMPPIO;

/**
 * ClassName:SmppMsgSubmitResp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年5月3日 下午7:28:26 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SmppMsgSubmitResp extends SmppMsgHead {
	private String messageId;
	
	/**
	 * messageId.
	 *
	 * @return  the messageId
	 * @since   JDK 1.6
	 */
	public String getMessageId() {
		return messageId;
	}
	/**
	 * messageId.
	 *
	 * @param   messageId    the messageId to set
	 * @since   JDK 1.6
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public SmppMsgSubmitResp(){
		
	}
	public SmppMsgSubmitResp(int packetLength,int commandId,int commandStatus,int sequenceId,byte[] data){
		this.setTotalLength(packetLength);
		this.setCommandId(commandId);
		this.setCommandStatus(commandStatus);
		this.setSequenceId(sequenceId);
		int offset=0;
		messageId=SMPPIO.readCString(data, offset);
	}
}

