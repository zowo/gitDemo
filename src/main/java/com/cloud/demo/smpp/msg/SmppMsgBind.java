/**
 * Project Name:demo
 * File Name:SmppMsgBind.java
 * Package Name:com.cloud.demo.smpp.msg
 * Date:2016年4月28日下午2:01:38
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.smpp.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloud.demo.smgp.SMPPIO;

/**
 * ClassName:SmppMsgBind <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月28日 下午2:01:38 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SmppMsgBind extends SmppMsgHead{
	private static Logger logger = LoggerFactory.getLogger(SmppMsgBind.class);
	private String systemId;  //16位
	private String password;  //9位
	private String systemType; //13位
	private byte version;
	private byte addrTon;
	private byte addrNpi;
	private String addrRange; //41位
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			SMPPIO.writeInt(16+this.getBodyLength(), 4, dous);
	        SMPPIO.writeInt(this.getCommandId(), 4, dous);
	        SMPPIO.writeInt(this.getCommandStatus(), 4, dous);
	        SMPPIO.writeInt(this.getSequenceId(), 4, dous);
	        SMPPIO.writeCString(this.systemId, dous);
	        SMPPIO.writeCString(this.password, dous);
	        SMPPIO.writeCString(this.systemType, dous);
	        SMPPIO.writeInt(this.version, 1, dous);
	        SMPPIO.writeInt(this.addrTon, 1, dous);
	        SMPPIO.writeInt(this.addrNpi, 1, dous);
	        SMPPIO.writeCString(this.addrRange, dous);

			dous.close();
		} catch (IOException e) {
			logger.error("封装链接二进制数组失败。");
			e.printStackTrace();
		}
		return bous.toByteArray();
	}
	/**
	 * systemId.
	 *
	 * @return  the systemId
	 * @since   JDK 1.6
	 */
	public String getSystemId() {
		return systemId;
	}
	/**
	 * password.
	 *
	 * @return  the password
	 * @since   JDK 1.6
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * systemType.
	 *
	 * @return  the systemType
	 * @since   JDK 1.6
	 */
	public String getSystemType() {
		return systemType;
	}
	/**
	 * version.
	 *
	 * @return  the version
	 * @since   JDK 1.6
	 */
	public byte getVersion() {
		return version;
	}
	/**
	 * addrTon.
	 *
	 * @return  the addrTon
	 * @since   JDK 1.6
	 */
	public byte getAddrTon() {
		return addrTon;
	}
	/**
	 * addrNpi.
	 *
	 * @return  the addrNpi
	 * @since   JDK 1.6
	 */
	public byte getAddrNpi() {
		return addrNpi;
	}
	/**
	 * systemId.
	 *
	 * @param   systemId    the systemId to set
	 * @since   JDK 1.6
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	/**
	 * password.
	 *
	 * @param   password    the password to set
	 * @since   JDK 1.6
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * systemType.
	 *
	 * @param   systemType    the systemType to set
	 * @since   JDK 1.6
	 */
	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}
	/**
	 * version.
	 *
	 * @param   version    the version to set
	 * @since   JDK 1.6
	 */
	public void setVersion(byte version) {
		this.version = version;
	}
	/**
	 * addrTon.
	 *
	 * @param   addrTon    the addrTon to set
	 * @since   JDK 1.6
	 */
	public void setAddrTon(byte addrTon) {
		this.addrTon = addrTon;
	}
	/**
	 * addrNpi.
	 *
	 * @param   addrNpi    the addrNpi to set
	 * @since   JDK 1.6
	 */
	public void setAddrNpi(byte addrNpi) {
		this.addrNpi = addrNpi;
	}
	public int getBodyLength() {
		int len=((systemId != null) ? systemId.length() : 0)
				+((password != null) ? password.length() : 0)
				+((systemType != null) ? systemType.length() : 0)
				+((addrRange != null) ? addrRange.length() : 0);
		// 3 1-byte integers, 4 c-strings
		return len+3+4;
	}
}

