/**
 * Project Name:ccopsgw
 * File Name:SendMsgMo.java
 * Package Name:com.ccop.sgw.msg
 * Date:2016年2月24日上午10:31:49
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.bean;

import org.apache.commons.lang.StringUtils;

/**
 * ClassName:SendMsgMo <br/>
 * Function: 上行短信消息体 <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年2月24日 上午10:31:49 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SendMsgMo {
	private int type;    //消息类型 0 上行短信 1状态报告
	private String moCode;   //服务号码
	private String tel;      //接收手机号
	private String submitTime; //提交时间
	private String content; //短信内容
	private String codePrefix=""; //服务号前缀
	public SendMsgMo(int type){
		this.type=type;
	}
	/**
	 * moCode.
	 *
	 * @return  the moCode
	 * @since   JDK 1.6
	 */
	public String getMoCode() {
		return moCode;
	}
	/**
	 * tel.
	 *
	 * @return  the tel
	 * @since   JDK 1.6
	 */
	public String getTel() {
		return tel;
	}
	/**
	 * submitTime.
	 *
	 * @return  the submitTime
	 * @since   JDK 1.6
	 */
	public String getSubmitTime() {
		return submitTime;
	}
	/**
	 * content.
	 *
	 * @return  the content
	 * @since   JDK 1.6
	 */
	public String getContent() {
		return content;
	}
	
	/**
	 * type.
	 *
	 * @return  the type
	 * @since   JDK 1.6
	 */
	public int getType() {
		return type;
	}
	/**
	 * type.
	 *
	 * @param   type    the type to set
	 * @since   JDK 1.6
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * moCode.
	 *
	 * @param   moCode    the moCode to set
	 * @since   JDK 1.6
	 */
	public void setMoCode(String moCode) {
		if(StringUtils.isNotBlank(moCode)){
			/*int len=codePrefix.length();
			if(len>8){
				moCode=moCode.substring(len-8);
			}*/
		}
		this.moCode = moCode;
	}
	/**
	 * tel.
	 *
	 * @param   tel    the tel to set
	 * @since   JDK 1.6
	 */
	public void setTel(String tel) {
		if(tel.startsWith("86")){
			this.tel=tel.substring(2);
		}else{
			this.tel = tel;
		}
	}
	/**
	 * submitTime.
	 *
	 * @param   submitTime    the submitTime to set
	 * @since   JDK 1.6
	 */
	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}
	/**
	 * content.
	 *
	 * @param   content    the content to set
	 * @since   JDK 1.6
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * codePrefix.
	 *
	 * @return  the codePrefix
	 * @since   JDK 1.6
	 */
	public String getCodePrefix() {
		return codePrefix;
	}
	/**
	 * codePrefix.
	 *
	 * @param   codePrefix    the codePrefix to set
	 * @since   JDK 1.6
	 */
	public void setCodePrefix(String codePrefix) {
		this.codePrefix = codePrefix;
	}
	
}

