/**
 * Project Name:ccopsgw
 * File Name:SendMsgReq.java
 * Package Name:com.ccop.sgw.msg
 * Date:2016年2月19日下午4:05:51
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.bean;
/**
 * ClassName:SendMsgReq <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年2月19日 下午4:05:51 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class TestSendMsgReq {
	private String msg;        //短信内容
	private String tel;        //手机号
	private String appendCode; //扩展码
	private String timestamp;  //时间戳  YYYYMMDDHHMMSS 14位
	private String encrypted;  //加密串  md5('cmpp'+key+secret+timestamp)
	private String signature;  //签名
	private String outId;
	/**
	 * msg.
	 *
	 * @return  the msg
	 * @since   JDK 1.6
	 */
	public String getMsg() {
		return msg;
	}
	/**
	 * tel.
	 *
	 * @return  the tel
	 * @since   JDK 1.6
	 */
	public String getTel() {
		return tel;
	}
	
	/**
	 * appendCode.
	 *
	 * @return  the appendCode
	 * @since   JDK 1.6
	 */
	public String getAppendCode() {
		return appendCode;
	}
	
	/**
	 * encrypted.
	 *
	 * @return  the encrypted
	 * @since   JDK 1.6
	 */
	public String getEncrypted() {
		return encrypted;
	}
	
	/**
	 * timestamp.
	 *
	 * @return  the timestamp
	 * @since   JDK 1.6
	 */
	public String getTimestamp() {
		return timestamp;
	}
	
	/**
	 * signature.
	 *
	 * @return  the signature
	 * @since   JDK 1.6
	 */
	public String getSignature() {
		return signature;
	}
	/**
	 * signature.
	 *
	 * @param   signature    the signature to set
	 * @since   JDK 1.6
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}
	/**
	 * timestamp.
	 *
	 * @param   timestamp    the timestamp to set
	 * @since   JDK 1.6
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	/**
	 * encrypted.
	 *
	 * @param   encrypted    the encrypted to set
	 * @since   JDK 1.6
	 */
	public void setEncrypted(String encrypted) {
		this.encrypted = encrypted;
	}
	/**
	 * appendCode.
	 *
	 * @param   appendCode    the appendCode to set
	 * @since   JDK 1.6
	 */
	public void setAppendCode(String appendCode) {
		this.appendCode = appendCode;
	}
	/**
	 * msg.
	 *
	 * @param   msg    the msg to set
	 * @since   JDK 1.6
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	/**
	 * tel.
	 *
	 * @param   tel    the tel to set
	 * @since   JDK 1.6
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}
	/**
	 * outId.
	 *
	 * @return  the outId
	 * @since   JDK 1.6
	 */
	public String getOutId() {
		return outId;
	}
	/**
	 * outId.
	 *
	 * @param   outId    the outId to set
	 * @since   JDK 1.6
	 */
	public void setOutId(String outId) {
		this.outId = outId;
	}
	
}

