/**
 * Project Name:ccopsgw
 * File Name:SendMsgResp.java
 * Package Name:com.ccop.sgw.msg
 * Date:2016年2月19日下午4:10:46
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.bean;
/**
 * ClassName:SendMsgResp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年2月19日 下午4:10:46 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SendMsgResp {
	private Integer result;
	private String msgId;
	public SendMsgResp() {
		
	}
	public SendMsgResp(Integer result,String msgId){
		this.result=result;
		this.msgId=msgId;
	}
	/**
	 * result.
	 *
	 * @return  the result
	 * @since   JDK 1.6
	 */
	public Integer getResult() {
		return result;
	}
	
	/**
	 * msgId.
	 *
	 * @return  the msgId
	 * @since   JDK 1.6
	 */
	public String getMsgId() {
		return msgId;
	}

	/**
	 * msgId.
	 *
	 * @param   msgId    the msgId to set
	 * @since   JDK 1.6
	 */
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	/**
	 * result.
	 *
	 * @param   result    the result to set
	 * @since   JDK 1.6
	 */
	public void setResult(Integer result) {
		this.result = result;
	}
	
}

