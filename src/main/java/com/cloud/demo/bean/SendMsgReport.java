/**
 * Project Name:ccopsgw
 * File Name:SendMsgReport.java
 * Package Name:com.ccop.sgw.msg
 * Date:2016年2月22日上午11:13:27
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.bean;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.cloud.demo.utils.DateUtils;


/**
 * ClassName:SendMsgReport <br/>
 * Function: 短信状态报告 <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年2月22日 上午11:13:27 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SendMsgReport {
	private int type;    //消息类型 0 上行短信 1状态报告
	private String msgId; //反给短信网关id
	private String tel;  //终端号码
	private String stat; //应答结果
	private String submitTime;//提交时间
	private String doneTime="";//YYMMDDHHMM
	private String carrierMsgId;
	private String outId;//out表id
	private String seqId;//序号
	private String carDoneTime; //运营商状态报告时间
	private int carrier; // 1上海移动 2江苏电信号百 3北京电信
	public SendMsgReport(int type){
		this.type=type;
	}
	public SendMsgReport(int type,int carrier){
		this.type=type;
		this.carrier=carrier;
	}
	/**
	 * msgId.
	 *
	 * @return  the msgId
	 * @since   JDK 1.6
	 */
	public String getMsgId() {
		return msgId;
	}
	/**
	 * tel.
	 *
	 * @return  the tel
	 * @since   JDK 1.6
	 */
	public String getTel() {
		return tel;
	}
	/**
	 * stat.
	 *
	 * @return  the stat
	 * @since   JDK 1.6
	 */
	public String getStat() {
		return stat;
	}
	/**
	 * submitTime.
	 *
	 * @return  the submitTime
	 * @since   JDK 1.6
	 */
	public String getSubmitTime() {
		return submitTime;
	}
	/**
	 * doneTime.
	 *
	 * @return  the doneTime
	 * @since   JDK 1.6
	 */
	public String getDoneTime() {
		return doneTime;
	}
	
	/**
	 * type.
	 *
	 * @return  the type
	 * @since   JDK 1.6
	 */
	public int getType() {
		return type;
	}
	
	/**
	 * carrierMsgId.
	 *
	 * @return  the carrierMsgId
	 * @since   JDK 1.6
	 */
	public String getCarrierMsgId() {
		return carrierMsgId;
	}
	
	/**
	 * outId.
	 *
	 * @return  the outId
	 * @since   JDK 1.6
	 */
	public String getOutId() {
		return outId;
	}
	
	/**
	 * carrier.
	 *
	 * @return  the carrier
	 * @since   JDK 1.6
	 */
	public int getCarrier() {
		return carrier;
	}
	
	/**
	 * seqId.
	 *
	 * @return  the seqId
	 * @since   JDK 1.6
	 */
	public String getSeqId() {
		return seqId;
	}
	
	/**
	 * carDoneTime.
	 *
	 * @return  the carDoneTime
	 * @since   JDK 1.6
	 */
	public String getCarDoneTime() {
		return carDoneTime;
	}
	/**
	 * carDoneTime.
	 *
	 * @param   carDoneTime    the carDoneTime to set
	 * @since   JDK 1.6
	 */
	public void setCarDoneTime(String carDoneTime) {
		this.carDoneTime = carDoneTime;
	}
	/**
	 * seqId.
	 *
	 * @param   seqId    the seqId to set
	 * @since   JDK 1.6
	 */
	public void setSeqId(String seqId) {
		this.seqId = seqId;
	}
	/**
	 * carrier.
	 *
	 * @param   carrier    the carrier to set
	 * @since   JDK 1.6
	 */
	public void setCarrier(int carrier) {
		this.carrier = carrier;
	}
	/**
	 * outId.
	 *
	 * @param   outId    the outId to set
	 * @since   JDK 1.6
	 */
	public void setOutId(String outId) {
		this.outId = outId;
	}
	/**
	 * carrierMsgId.
	 *
	 * @param   carrierMsgId    the carrierMsgId to set
	 * @since   JDK 1.6
	 */
	public void setCarrierMsgId(String carrierMsgId) {
		this.carrierMsgId = carrierMsgId;
	}
	/**
	 * type.
	 *
	 * @param   type    the type to set
	 * @since   JDK 1.6
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * msgId.
	 *
	 * @param   msgId    the msgId to set
	 * @since   JDK 1.6
	 */
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	/**
	 * tel.
	 *
	 * @param   tel    the tel to set
	 * @since   JDK 1.6
	 */
	public void setTel(String tel) {
		if(tel.startsWith("86")){
			tel=tel.substring(2);
		}
		this.tel = tel;
	}
	/**
	 * stat.
	 *
	 * @param   stat    the stat to set
	 * @since   JDK 1.6
	 */
	public void setStat(String stat) {
		this.stat = stat;
	}
	/**
	 * submitTime.
	 *
	 * @param   submitTime    the submitTime to set
	 * @since   JDK 1.6
	 */
	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}
	/**
	 * doneTime.
	 *
	 * @param   doneTime    the doneTime to set
	 * @since   JDK 1.6
	 */
	public void setDoneTime(String doneTime) {
		if(StringUtils.isNotBlank(doneTime)){
		int second=Calendar.getInstance().get(Calendar.SECOND);
		String str=String.format("%02d",second);
		doneTime=doneTime+str;
		Date carrTime=DateUtils.parseDate(doneTime,"yyMMddHHmmss");
		Date curTime=new Date();
		boolean flag=carrTime.before(curTime);
		if(flag){
			//qu carrTime
			this.doneTime = doneTime;
		}else{
			this.doneTime=DateUtils.formatDate(curTime,"yyMMddHHmmss");
		}
		}
	}
	
}

