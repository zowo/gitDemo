/**
 * Project Name:demo
 * File Name:CorpTextContent.java
 * Package Name:com.cloud.demo.wechat
 * Date:2016年4月19日下午7:07:47
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.wechat;
/**
 * ClassName:CorpTextContent <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月19日 下午7:07:47 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class CorpTextContent {
	private String content;
	public CorpTextContent(String content) {
		this.content=content;
	}
	/**
	 * content.
	 *
	 * @return  the content
	 * @since   JDK 1.6
	 */
	public String getContent() {
		return content;
	}

	/**
	 * content.
	 *
	 * @param   content    the content to set
	 * @since   JDK 1.6
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
}

