/**
 * Project Name:demo
 * File Name:CorpBaseMsg.java
 * Package Name:com.cloud.demo.wechat
 * Date:2016年4月19日下午6:49:11
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.wechat;
/**
 * ClassName:CorpBaseMsg <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月19日 下午6:49:11 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class CorpBaseMsg {
	private String touser;
	private String toparty;
	private String totag;
	private String msgtype;
	private int agentid;
	private String safe="0";
	/**
	 * touser.
	 *
	 * @return  the touser
	 * @since   JDK 1.6
	 */
	public String getTouser() {
		return touser;
	}
	/**
	 * toparty.
	 *
	 * @return  the toparty
	 * @since   JDK 1.6
	 */
	public String getToparty() {
		return toparty;
	}
	/**
	 * totag.
	 *
	 * @return  the totag
	 * @since   JDK 1.6
	 */
	public String getTotag() {
		return totag;
	}
	/**
	 * msgtype.
	 *
	 * @return  the msgtype
	 * @since   JDK 1.6
	 */
	public String getMsgtype() {
		return msgtype;
	}
	/**
	 * agentid.
	 *
	 * @return  the agentid
	 * @since   JDK 1.6
	 */
	public int getAgentid() {
		return agentid;
	}
	/**
	 * safe.
	 *
	 * @return  the safe
	 * @since   JDK 1.6
	 */
	public String getSafe() {
		return safe;
	}
	/**
	 * touser.
	 *
	 * @param   touser    the touser to set
	 * @since   JDK 1.6
	 */
	public void setTouser(String touser) {
		this.touser = touser;
	}
	/**
	 * toparty.
	 *
	 * @param   toparty    the toparty to set
	 * @since   JDK 1.6
	 */
	public void setToparty(String toparty) {
		this.toparty = toparty;
	}
	/**
	 * totag.
	 *
	 * @param   totag    the totag to set
	 * @since   JDK 1.6
	 */
	public void setTotag(String totag) {
		this.totag = totag;
	}
	/**
	 * msgtype.
	 *
	 * @param   msgtype    the msgtype to set
	 * @since   JDK 1.6
	 */
	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}
	/**
	 * agentid.
	 *
	 * @param   agentid    the agentid to set
	 * @since   JDK 1.6
	 */
	public void setAgentid(int agentid) {
		this.agentid = agentid;
	}
	/**
	 * safe.
	 *
	 * @param   safe    the safe to set
	 * @since   JDK 1.6
	 */
	public void setSafe(String safe) {
		this.safe = safe;
	}
	
}

