/**
 * Project Name:demo
 * File Name:CorpTextMsg.java
 * Package Name:com.cloud.demo.wechat
 * Date:2016年4月19日下午6:48:48
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.wechat;
/**
 * ClassName:CorpTextMsg <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月19日 下午6:48:48 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class CorpTextMsg extends CorpBaseMsg{
	private CorpTextContent text;

	/**
	 * text.
	 *
	 * @return  the text
	 * @since   JDK 1.6
	 */
	public CorpTextContent getText() {
		return text;
	}

	/**
	 * text.
	 *
	 * @param   text    the text to set
	 * @since   JDK 1.6
	 */
	public void setText(CorpTextContent text) {
		this.text = text;
	}

	
	
}

