/**
 * Project Name:demo
 * File Name:Wechat.java
 * Package Name:com.cloud.demo.wechat
 * Date:2016年4月19日下午6:35:29
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.wechat;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.cloud.demo.httpclient.HttpClient;
import com.cloud.demo.utils.JacksonUtil;

/**
 * ClassName:Wechat <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月19日 下午6:35:29 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class Wechat {
	private static Logger logger = LoggerFactory.getLogger(Wechat.class);
	public void getToken(){
		//需定时获取1小时1次
		
	}
	public static String requestToken(String corpid,String corpsecret) {
		String token="";
		String url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=" + corpid+"&corpsecret="+corpsecret;
		String result=HttpClient.postDataJson(url, null, "wechat");
		logger.info("请求获取jsapi_ticket,result:" + result);
		if(StringUtils.isNotBlank(result)){
			JSONObject jsonObject = JSONObject.parseObject(result);
			try {
				token = jsonObject.get("access_token").toString();
			} catch (Exception e) {
				logger.error("access_token 结果解析失败,access_token请求结果:" + result);
				e.printStackTrace();
			}
		}
		return token;
	}
	public static void sendMsg(String token){
		CorpTextContent content=new CorpTextContent("再次测试告警推送消息");
		CorpTextMsg textMsg=new CorpTextMsg();
		textMsg.setTouser("@all");
		textMsg.setToparty("");
		textMsg.setTotag("");
		textMsg.setMsgtype("text");
		textMsg.setAgentid(1);
		textMsg.setText(content);
//		String token="JDidDyc3ZLD3JA5gU7B9RbjQJfxcX6vAYDoIqoHlU2sQ_8YmT4iDVvWKkJGj5Row";
		String url="https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token="+token;
		String jsonData=JacksonUtil.writeValueAsString(textMsg);
		HttpClient.postDataJson(url, jsonData, "wechat");
	}
	public static void main(String[] args) {
		
		String token=Wechat.requestToken("wx5e91acd160094719","oZH1-nrKSRBR-sg4QHByktDnNrlNxYTLBcvAlXBzS7Ze-3cWgQPDrBoGmLvPr5Xq");
		Wechat.sendMsg(token);
		

	}

}

