package com.cloud.demo.servlet;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cloud.demo.cmppsh2.socket.CmppSh2Container;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloud.demo.cmppsh.socket.CmppShContainer;
import com.cloud.demo.smpp.socket.SmppContainer;



public class ApplicationServlet extends HttpServlet {
	private static final Logger log = LoggerFactory.getLogger(ApplicationServlet.class);
	private static final long serialVersionUID = 1L;
	public void init() throws ServletException {
		ExecutorService pool = Executors.newFixedThreadPool(2); 
		pool.submit(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				//SmppContainer.getInstance();
				//CmppShContainer.getInstance();
				CmppSh2Container.getInstance();
			}
		});
		
		
		log.info("【直连通道】短信通道直连网关系统启动成功");
	}
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApplicationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
}
