/**
 * Project Name:ccopsgw
 * File Name:SmgpController.java
 * Package Name:com.ccop.sgw.controller
 * Date:2016年2月27日下午11:12:07
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloud.demo.bean.SendMsgReq;
import com.cloud.demo.bean.SendMsgResp;
import com.cloud.demo.smpp.socket.SmppContainer;
import com.cloud.demo.utils.JacksonUtil;


/**
 * ClassName:SmgpController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年2月27日 下午11:12:07 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
@Controller
@RequestMapping("/sgw")
public class SmppController {
	public final Logger logger = LoggerFactory.getLogger(SmppController.class);
	@RequestMapping("/smpp/sendMsg")
	@ResponseBody
	public String sendMsg(@RequestBody SendMsgReq sendMsgReq) throws Exception{
		String req=JacksonUtil.writeValueAsString(sendMsgReq);
		logger.info("【smpp接收到短信发送请求】:"+req);
		int result=-3;
		String msgId=null;
		
				Map<String, String> map=SmppContainer.getInstance().putSendMsg(sendMsgReq);
				if(map!=null){
					msgId=map.get("respId");
					result=Integer.parseInt(map.get("result"));
				}else{
					result=-6;
				}
			
		SendMsgResp sendMsgResp =new SendMsgResp();
		sendMsgResp.setResult(result);
		sendMsgResp.setMsgId(msgId);
		String rs=JacksonUtil.writeValueAsString(sendMsgResp);
		logger.info("【smpp回响应】tel:{},rs:{}",new Object[]{sendMsgReq.getTel(),rs});
		return rs;
	}
	
}

