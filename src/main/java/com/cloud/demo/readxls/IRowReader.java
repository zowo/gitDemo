/**
 * Project Name:ccop.callout
 * File Name:IRowReader.java
 * Package Name:com.ccop.common.util
 * Date:2015年9月2日下午5:50:10
 * Copyright (c) 2015, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.readxls;

import java.util.List;

/**
 * ClassName:IRowReader <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2015年9月2日 下午5:50:10 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public interface IRowReader {
	/**业务逻辑实现方法
	 * @param rowlist
	 */
	public void getRows(List<String[]> rowlist);

}

