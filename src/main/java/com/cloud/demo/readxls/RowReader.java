/**
 * Project Name:demo
 * File Name:RowReader.java
 * Package Name:com.cloud.demo.readxls
 * Date:2016年3月18日上午11:07:57
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.readxls;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cloud.demo.readlog.ParseLog;

/**
 * ClassName:RowReader <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年3月18日 上午11:07:57 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class RowReader implements IRowReader {
	private static Logger logger = LoggerFactory.getLogger(RowReader.class);
	@Override
	public void getRows(List<String[]> rowlist) {
		logger.debug("调用处理");
		File file = new File("E:\\log\\031718\\sgw.2016-03-17_18.log"); 
        File filew =new File("E:\\log\\031718\\data.txt");
		InputStreamReader isr=null;
//		BufferedReader reader =null;
		FileOutputStream fop=null;
		try {
//			isr = new InputStreamReader(new FileInputStream(file), "UTF-8");
//			 reader = new BufferedReader(isr);
			fop = new FileOutputStream(filew,true);
		
		
    	for(String[] row:rowlist){
    		
//    		for(int i = 0;i<row.length;i++){
    			logger.debug(row[0]);
//    			ParseLog.readFileByLines(fileName, num);
//    			ParseLog.readFileByLines("E:\\log\\031718\\sgw.2016-03-17_18.log",row[0]);  
    			
//    		}
    			readFileByLines(file,fop,row[0]);
  	    }
//    	 reader.close();  
    	fop.flush();
    	fop.close();
} catch (Exception e) {
			
	logger.error(e.getMessage());
			e.printStackTrace();
			
		}  
	}
	public void readFileByLines(File file ,FileOutputStream fop,String num) {  
       boolean flag=false;
       BufferedReader reader =null;
        try {  
        	 InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "UTF-8");  
        	  reader = new BufferedReader(isr); 
            String tempString = null;  
            // 一次读入一行，直到读入null为文件结束  
            while ((tempString = reader.readLine()) != null) {  
                if (tempString.contains("\"tel\":\""+num+"\",\"stat\":\"")&&tempString.contains("/shzl/report")) {  
                	logger.info(tempString);
                	String content = tempString.split("msg:")[1];
                	logger.info(content);
                	byte[] contentInBytes = (content+"\n").getBytes();
                	fop.write(contentInBytes);
                	flag=true;
                	break;
                }
            }  
            if(!flag){
            	logger.warn("未找到"+num);
            }
//            reader.close();  
//            Iterator<Entry<String, Object>> it = map.entrySet().iterator();  
//            while (it.hasNext()) {  
//                Entry<String, Object> entry = it.next();  
//                System.out.println(entry.getKey() + "-----------" + entry.getValue());  
//            }  
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            if (reader != null) {  
                try {  
                    reader.close();  
                } catch (IOException e1) {  
                }  
            }  
        }  
    }

}

