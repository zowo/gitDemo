package com.cloud.demo.sgip;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FEncoder extends ProtocolEncoderAdapter {
	private static Logger log = LoggerFactory.getLogger(FEncoder.class);

	@Override
	public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception {
//		log.debug("开始编码"+message);
		 byte[] bytes = (byte[]) message;
		 IoBuffer buffer = IoBuffer.allocate(256);
		 buffer.setAutoExpand(true);
		 buffer.put(bytes);
		 buffer.flip();
		 out.write(buffer);
		 out.flush();
		 buffer.free();
	}

}
