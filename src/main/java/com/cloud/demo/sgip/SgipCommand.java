/**
 * Project Name:ccopsgw
 * File Name:SgipCommon.java
 * Package Name:com.ccop.common.sgip
 * Date:2016年3月28日下午4:41:17
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.sgip;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cloud.demo.utils.ByteConvert;


/**
 * ClassName:SgipCommon <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年3月28日 下午4:41:17 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SgipCommand {
	/**
	 * 包头长度。（协议采用定长包头，长度为4+4+4)
	 * 
     *Total_Length	4	Unsigned Integer	消息总长度(含消息头及消息体)
     *Request_Id	4	Unsigned Integer	命令或响应类型
     *Sequence_Id	4	Unsigned Integer	消息流水号,顺序累加,步长为1,循环使用（一对请求和应答消息的流水号必须相同）
	 */
	public static final int MsgHeaderLength = 20;
	public static final int MsgTotalLength=4;
	public static final int MsgCommandId=4;
	public static final int MsgSequenceId=12;
	
	
	public static final int SGIP_BIND        = 0x1;
	public static final int SGIP_BIND_RESP   = 0x80000001;
	public static final int SGIP_UNBIND      = 0x2;
	public static final int SGIP_UNBIND_RESP = 0x80000002;
	public static final int SGIP_SUBMIT         = 0x3;
	public static final int SGIP_SUBMIT_RESP    = 0x80000003;
	public static final int SGIP_DELIVER        = 0x4;
	public static final int SGIP_DELIVER_RESP   = 0x80000004;
	public static final int SGIP_REPORT         = 0x5;
	public static final int SGIP_REPORT_RESP    = 0x80000005;
	
	/**
	 * 命令产生时间
	 * @return int
	 */
	public static int getDateCmd() {
		SimpleDateFormat formate = new SimpleDateFormat("MMddHHmmss");
		String currTime = formate.format(new Date());
		int cmd = new BigInteger(currTime).intValue();
		return cmd;
	}
	public static String getSeqStr(byte[] seqByte){
		byte[] tempbytes = new byte[4];
		System.arraycopy(seqByte, 0,tempbytes, 0, 4);
		byte[] srcnodeByte = new byte[5];
		System.arraycopy(tempbytes, 0, srcnodeByte, 1, 4);
		BigInteger src = new BigInteger(srcnodeByte);
		System.arraycopy(seqByte, 4,tempbytes, 0, 4);
		String date = ByteConvert.bytesToInt(tempbytes)+"";
		System.arraycopy(seqByte, 8,tempbytes, 0, 4);
		String seqnum = ByteConvert.bytesToInt(tempbytes)+"";
		return src+date+seqnum;
	}
}

