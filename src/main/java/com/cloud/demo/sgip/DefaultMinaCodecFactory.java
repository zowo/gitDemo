package com.cloud.demo.sgip;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

public class DefaultMinaCodecFactory implements ProtocolCodecFactory {
	ProtocolEncoder encoder;
	ProtocolDecoder decoder;

	public DefaultMinaCodecFactory() {

	}

	public DefaultMinaCodecFactory(ProtocolEncoder encoder, ProtocolDecoder decoder) {
		super();
		this.encoder = encoder;
		this.decoder = decoder;
	}

	@Override
    public ProtocolDecoder getDecoder(IoSession session) throws Exception {
        return decoder;
    }

    @Override
    public ProtocolEncoder getEncoder(IoSession session) throws Exception {
        return encoder;
    }

}