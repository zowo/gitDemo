package com.cloud.demo.sgip.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.cloud.demo.utils.MsgUtils;


public class SgipMsgBindResp extends SgipMsgHead {
	private static Logger logger=Logger.getLogger(SgipMsgBindResp.class);
	private byte result; 
	private String reserve="";
	public SgipMsgBindResp() {
		
	}
	public SgipMsgBindResp(int totalLength,int commandId,byte[] seqByte,byte[] data){
		if(data.length==1+8){
			this.setTotalLength(totalLength);
			this.setCommandId(commandId);
			this.setSeqByte(seqByte);
			result=data[0];
		}
	}
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			dous.writeInt(this.getTotalLength());
			dous.writeInt(this.getCommandId());
			dous.write(this.getSeqByte());
			dous.writeByte(this.result);
			MsgUtils.writeString(dous,this.reserve,8);
			
			dous.close();
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("封装短信发送二进制数组失败。");
		}
		return bous.toByteArray();
	}
	/**
	 * result.
	 *
	 * @return  the result
	 * @since   JDK 1.6
	 */
	public byte getResult() {
		return result;
	}
	/**
	 * reserve.
	 *
	 * @return  the reserve
	 * @since   JDK 1.6
	 */
	public String getReserve() {
		return reserve;
	}
	/**
	 * result.
	 *
	 * @param   result    the result to set
	 * @since   JDK 1.6
	 */
	public void setResult(byte result) {
		this.result = result;
	}
	/**
	 * reserve.
	 *
	 * @param   reserve    the reserve to set
	 * @since   JDK 1.6
	 */
	public void setReserve(String reserve) {
		this.reserve = reserve;
	}
	
	
}
