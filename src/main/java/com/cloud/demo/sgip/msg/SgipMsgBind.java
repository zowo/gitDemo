package com.cloud.demo.sgip.msg;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.cloud.demo.sgip.SgipCommand;
import com.cloud.demo.utils.MsgUtils;


public class SgipMsgBind extends SgipMsgHead{
	private static Logger logger=Logger.getLogger(SgipMsgBind.class);
	private byte loginType =1;
	private String loginName;
	private String loginPassword;
	private String reserve="";
	private int result;//解析结果
	public SgipMsgBind(){
	}
	public SgipMsgBind(int totalLength,int commandId,byte[] seqByte,byte[] data){
		if(data.length>=1+16+16+8){
			this.setTotalLength(totalLength);
			this.setCommandId(commandId);
			this.setSeqByte(seqByte);
			this.loginType=data[0];
			byte[] loginNameByte=new byte[16];
			System.arraycopy(data, 1, loginNameByte, 0, 16);
			this.loginName = new String(loginNameByte).trim();
			byte[] loginPasswordByte=new byte[16];
			System.arraycopy(data, 17, loginPasswordByte, 0, 16);
			this.loginPassword = new String(loginPasswordByte).trim();
			this.setSeqStr(SgipCommand.getSeqStr(seqByte));
			this.result=0;
		}else{
			this.result=1;//消息结构错
		}
	}
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			dous.writeInt(this.getTotalLength());
			dous.writeInt(this.getCommandId());
			dous.write(this.getSeqByte());
			dous.writeByte(this.loginType);
			MsgUtils.writeString(dous,this.loginName,16);
			MsgUtils.writeString(dous,this.loginPassword,16);
			MsgUtils.writeString(dous,this.reserve,8);
			
			dous.close();
		} catch (IOException e) {
			logger.error("封装短信发送二进制数组失败。");
		}
		return bous.toByteArray();
	}
	/**
	 * loginType.
	 *
	 * @return  the loginType
	 * @since   JDK 1.6
	 */
	public byte getLoginType() {
		return loginType;
	}
	/**
	 * loginName.
	 *
	 * @return  the loginName
	 * @since   JDK 1.6
	 */
	public String getLoginName() {
		return loginName;
	}
	/**
	 * loginPassword.
	 *
	 * @return  the loginPassword
	 * @since   JDK 1.6
	 */
	public String getLoginPassword() {
		return loginPassword;
	}
	/**
	 * reserve.
	 *
	 * @return  the reserve
	 * @since   JDK 1.6
	 */
	public String getReserve() {
		return reserve;
	}
	
	/**
	 * result.
	 *
	 * @return  the result
	 * @since   JDK 1.6
	 */
	public int getResult() {
		return result;
	}
	/**
	 * result.
	 *
	 * @param   result    the result to set
	 * @since   JDK 1.6
	 */
	public void setResult(int result) {
		this.result = result;
	}
	/**
	 * loginType.
	 *
	 * @param   loginType    the loginType to set
	 * @since   JDK 1.6
	 */
	public void setLoginType(byte loginType) {
		this.loginType = loginType;
	}
	/**
	 * loginName.
	 *
	 * @param   loginName    the loginName to set
	 * @since   JDK 1.6
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	/**
	 * loginPassword.
	 *
	 * @param   loginPassword    the loginPassword to set
	 * @since   JDK 1.6
	 */
	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}
	/**
	 * reserve.
	 *
	 * @param   reserve    the reserve to set
	 * @since   JDK 1.6
	 */
	public void setReserve(String reserve) {
		this.reserve = reserve;
	}
	
	
}
