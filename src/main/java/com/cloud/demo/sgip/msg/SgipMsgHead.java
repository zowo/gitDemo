/**
 * Project Name:ccopsgw
 * File Name:SgipMsgHead.java
 * Package Name:com.ccop.common.sgip.msg
 * Date:2016年3月30日下午5:55:03
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.sgip.msg;

import org.apache.log4j.Logger;

/**
 * ClassName:SgipMsgHead <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年3月30日 下午5:55:03 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SgipMsgHead {
	private Logger logger=Logger.getLogger(SgipMsgHead.class);
	private int totalLength;//Unsigned Integer 消息总长度
	private int commandId;//Unsigned Integer 命令类型
	///源节点编号+mmddhhmmss+序号
	private String seqStr; 
	private byte[] seqByte;
	/**
	 * totalLength.
	 *
	 * @return  the totalLength
	 * @since   JDK 1.6
	 */
	public int getTotalLength() {
		return totalLength;
	}
	/**
	 * commandId.
	 *
	 * @return  the commandId
	 * @since   JDK 1.6
	 */
	public int getCommandId() {
		return commandId;
	}
	
	/**
	 * seqByte.
	 *
	 * @return  the seqByte
	 * @since   JDK 1.6
	 */
	public byte[] getSeqByte() {
		return seqByte;
	}
	
	/**
	 * seqStr.
	 *
	 * @return  the seqStr
	 * @since   JDK 1.6
	 */
	public String getSeqStr() {
		return seqStr;
	}
	/**
	 * seqStr.
	 *
	 * @param   seqStr    the seqStr to set
	 * @since   JDK 1.6
	 */
	public void setSeqStr(String seqStr) {
		this.seqStr = seqStr;
	}
	/**
	 * seqByte.
	 *
	 * @param   seqByte    the seqByte to set
	 * @since   JDK 1.6
	 */
	public void setSeqByte(byte[] seqByte) {
		this.seqByte = seqByte;
	}
	/**
	 * totalLength.
	 *
	 * @param   totalLength    the totalLength to set
	 * @since   JDK 1.6
	 */
	public void setTotalLength(int totalLength) {
		this.totalLength = totalLength;
	}
	/**
	 * commandId.
	 *
	 * @param   commandId    the commandId to set
	 * @since   JDK 1.6
	 */
	public void setCommandId(int commandId) {
		this.commandId = commandId;
	}
	
	
}

