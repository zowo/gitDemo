/**
 * Project Name:ccopsgw
 * File Name:SgipMsgReport.java
 * Package Name:com.ccop.common.sgip.msg
 * Date:2016年3月30日下午6:53:43
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.sgip.msg;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.math.BigInteger;

import org.omg.CORBA.INTERNAL;

import com.cloud.demo.sgip.SgipCommand;
import com.cloud.demo.utils.ByteConvert;


/**
 * ClassName:SgipMsgReport <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年3月30日 下午6:53:43 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SgipMsgReport extends SgipMsgHead{
	private String submitSequenceNumber;  //该命令所涉及的Submit或deliver命令的弃列号
	private byte reportType;           //0:对先前的一条Submit命令的状态报告
		                               //1:对先前一条前转Deliver命令的状态报告
	private String userNumber;         //接收短消息的手机号码
	private byte state;                //0:发送成功　　1:等待发送　2:发送失败
	private byte errorCode;            //当state=2时为错误码值，否则为0
	private String reverse;            //保留
	private int result;//解析结果
	public SgipMsgReport (int totalLength,int commandId,byte[] seqByte,byte[] data){
		if(data.length>=1+21+1+1+8){//+Msg_length+
				this.setTotalLength(totalLength);
				this.setCommandId(commandId);
				this.setSeqByte(seqByte);
				byte[] tmpBytes = null;
				int curr_index=0;
				//源节点编码
				tmpBytes = new byte[5];
				System.arraycopy(data, curr_index, tmpBytes, 1, 4);
				submitSequenceNumber = new BigInteger(tmpBytes).toString();
				curr_index +=4;
				//日期时间
				tmpBytes = new byte[4];
				System.arraycopy(data, curr_index, tmpBytes, 0, 4);
				submitSequenceNumber +=ByteConvert.bytesToInt(tmpBytes);
				curr_index +=4;
				//序号
				tmpBytes = new byte[4];
				System.arraycopy(data, curr_index, tmpBytes, 0, 4);
				submitSequenceNumber += ByteConvert.bytesToInt(tmpBytes);
				curr_index +=4;
				this.reportType=data[curr_index];
				curr_index ++;
				byte[] userNumberByte=new byte[21];
				System.arraycopy(data, curr_index, userNumberByte, 0, 21);
				this.userNumber=new String(userNumberByte).trim();
				curr_index += 21;
				this.state = data[curr_index];
				curr_index ++;
				//-----------------ErrorCode
				this.errorCode = data[curr_index];
				this.setSeqStr(SgipCommand.getSeqStr(seqByte));
				this.result=0;//正确
			
		}else{
			this.result=1;//消息结构错
		}
	}
	/**
	 * submitSequenceNumber.
	 *
	 * @return  the submitSequenceNumber
	 * @since   JDK 1.6
	 */
	public String getSubmitSequenceNumber() {
		return submitSequenceNumber;
	}
	/**
	 * reportType.
	 *
	 * @return  the reportType
	 * @since   JDK 1.6
	 */
	public byte getReportType() {
		return reportType;
	}
	/**
	 * userNumber.
	 *
	 * @return  the userNumber
	 * @since   JDK 1.6
	 */
	public String getUserNumber() {
		return userNumber;
	}
	/**
	 * state.
	 *
	 * @return  the state
	 * @since   JDK 1.6
	 */
	public byte getState() {
		return state;
	}
	/**
	 * errorCode.
	 *
	 * @return  the errorCode
	 * @since   JDK 1.6
	 */
	public byte getErrorCode() {
		return errorCode;
	}
	/**
	 * reverse.
	 *
	 * @return  the reverse
	 * @since   JDK 1.6
	 */
	public String getReverse() {
		return reverse;
	}
	
	/**
	 * result.
	 *
	 * @return  the result
	 * @since   JDK 1.6
	 */
	public int getResult() {
		return result;
	}
	/**
	 * result.
	 *
	 * @param   result    the result to set
	 * @since   JDK 1.6
	 */
	public void setResult(int result) {
		this.result = result;
	}
	/**
	 * submitSequenceNumber.
	 *
	 * @param   submitSequenceNumber    the submitSequenceNumber to set
	 * @since   JDK 1.6
	 */
	public void setSubmitSequenceNumber(String submitSequenceNumber) {
		this.submitSequenceNumber = submitSequenceNumber;
	}
	/**
	 * reportType.
	 *
	 * @param   reportType    the reportType to set
	 * @since   JDK 1.6
	 */
	public void setReportType(byte reportType) {
		this.reportType = reportType;
	}
	/**
	 * userNumber.
	 *
	 * @param   userNumber    the userNumber to set
	 * @since   JDK 1.6
	 */
	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}
	/**
	 * state.
	 *
	 * @param   state    the state to set
	 * @since   JDK 1.6
	 */
	public void setState(byte state) {
		this.state = state;
	}
	/**
	 * errorCode.
	 *
	 * @param   errorCode    the errorCode to set
	 * @since   JDK 1.6
	 */
	public void setErrorCode(byte errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * reverse.
	 *
	 * @param   reverse    the reverse to set
	 * @since   JDK 1.6
	 */
	public void setReverse(String reverse) {
		this.reverse = reverse;
	}
	
}

