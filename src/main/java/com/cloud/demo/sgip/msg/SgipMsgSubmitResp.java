/**
 * Project Name:ccopsgw
 * File Name:SgipMsgSubmitResp.java
 * Package Name:com.ccop.common.sgip.msg
 * Date:2016年3月30日下午6:52:51
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.sgip.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.cloud.demo.sgip.SgipCommand;
import com.cloud.demo.utils.MsgUtils;


/**
 * ClassName:SgipMsgSubmitResp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年3月30日 下午6:52:51 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SgipMsgSubmitResp extends SgipMsgHead {
	private static Logger logger=Logger.getLogger(SgipMsgSubmitResp.class);
	private byte result;
	private String reserve="";
	public SgipMsgSubmitResp(){
		
	}
	public SgipMsgSubmitResp(int totalLength,int commandId,byte[] seqByte,byte[] data){
		if(data.length==1+8){
			this.setTotalLength(totalLength);
			this.setCommandId(commandId);
			this.setSeqByte(seqByte);
			this.setSeqStr(SgipCommand.getSeqStr(seqByte));
			result=data[0];
		}
	}
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			dous.writeInt(this.getTotalLength());
			dous.writeInt(this.getCommandId());
			dous.write(this.getSeqByte());
			dous.writeByte(this.result);
			MsgUtils.writeString(dous,this.reserve,8);
			
			dous.close();
		} catch (IOException e) {
			logger.error("封装短信发送二进制数组失败。");
		}
		return bous.toByteArray();
	}
	/**
	 * result.
	 *
	 * @return  the result
	 * @since   JDK 1.6
	 */
	public byte getResult() {
		return result;
	}
	/**
	 * reserve.
	 *
	 * @return  the reserve
	 * @since   JDK 1.6
	 */
	public String getReserve() {
		return reserve;
	}
	/**
	 * result.
	 *
	 * @param   result    the result to set
	 * @since   JDK 1.6
	 */
	public void setResult(byte result) {
		this.result = result;
	}
	/**
	 * reserve.
	 *
	 * @param   reserve    the reserve to set
	 * @since   JDK 1.6
	 */
	public void setReserve(String reserve) {
		this.reserve = reserve;
	}
	
}

