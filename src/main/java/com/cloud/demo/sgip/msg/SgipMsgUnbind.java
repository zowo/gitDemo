/**
 * Project Name:ccopsgw
 * File Name:SgipMsgUnbind.java
 * Package Name:com.ccop.common.sgip.msg
 * Date:2016年4月2日上午10:09:13
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.sgip.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.cloud.demo.sgip.SgipCommand;


/**
 * ClassName:SgipMsgUnbind <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月2日 上午10:09:13 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SgipMsgUnbind extends SgipMsgHead {
	private static Logger logger=Logger.getLogger(SgipMsgUnbind.class);
	public SgipMsgUnbind(){
		
	}
	public SgipMsgUnbind(int totalLength,int commandId,byte[] seqByte){
		this.setTotalLength(totalLength);
		this.setCommandId(commandId);
		this.setSeqByte(seqByte);		
		this.setSeqStr(SgipCommand.getSeqStr(seqByte));
	}
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			dous.writeInt(this.getTotalLength());
			dous.writeInt(this.getCommandId());
			dous.write(this.getSeqByte());
			
			dous.close();
		} catch (IOException e) {
			logger.error("封装短信发送二进制数组失败。");
		}
		return bous.toByteArray();
	}
}

