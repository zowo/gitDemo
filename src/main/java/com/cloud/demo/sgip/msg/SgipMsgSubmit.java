/**
 * Project Name:ccopsgw
 * File Name:SgipMsgSubmit.java
 * Package Name:com.ccop.common.sgip.msg
 * Date:2016年3月30日上午10:39:05
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.sgip.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.cloud.demo.utils.MsgUtils;


/**
 * ClassName:SgipMsgSubmit <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年3月30日 上午10:39:05 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SgipMsgSubmit extends SgipMsgHead{
	private static Logger logger=Logger.getLogger(SgipMsgSubmit.class);
	private String spNumber;         //SP接入号码(21字节)
	
	private String chargeNumber;     //付费号码,手机号码前加"86"国别标志,当且仅当群发且对用户收费是为空;如
	                                 //果为空，则该条短信消息产生的费用由UserNumber代表的用户支付;如果为全字符串
	                                 //"000000000000000000000",表示该条消息产生的费用由SP支付 (21字节)
	private byte userCount;           //接收短消息的手机数量，取值 范围1至100(1字节)
	private String[] userNumber;       //接收该短消息的手机号，该字段重复UserCount指定的次数，手机号加86国别标志(21字节)
	private String corpId;           //企业代码(5字节)
	private String serviceType;      //业务代码(10字节)
	private byte feeType;			 //计费类型(6字节)
	private String feeValue;           //该条短信的收费值，由SP定义，对于包月制收费的用户，该值的为月租费的值　
	private String givenValue;       //赠送话费
	private byte agentFlag;          //代收费标标志(0应收 1实收)
	private byte morelatetoMTFlag;   //引起MT 消息的原因
	                                 //0-MO点播引起的第一条MT消息
	                                 //1-MO点播引起的非第一条 MT消息 
	                                 //2-非MO点播 引起的MT消息
	                                 //3-系统反馈引起的MT消息
	private byte priority;           //优先级(0-9),从低到高，默认为0
	private String expireTime;		 //短消息寿命终止时间  ，格式为"yymmddhhmmsstnnp"
	private String scheduleTime;     //定时发送时间,如果为空发示立即发送，格式化"yymmddhhmmsstnnp"
	private byte reportFlag;		 //状态报告标记
	                                 //0-该条消息只有最后出错时要返回状态报告
	                                 //1-该条消息无论最后是否成功都要返回状态报告
	                                 //2-该条消息不需要返回状态报告
	                                 //3-该条消息仅携带包月计费信息，不下发给用户
	                                 //其它保留，缺省为0
    private byte tpPid;             //GSM协议类型
	private byte tpUdhi;            //GSM协议类型
	private byte messageCoding;      //短消息的编码格式
                                     //0:纯ASCII字符串
                                     //3:写卡操作
                                     //4:二进制编码
                                     //8:UCS2编码
                                     //15:GBK编码
    private byte messageType;        //信息类型
                                     //0-短消息信息  其它 :待定
    private int messageLength;       //短消息的长度 
    private byte[] messageContent;   //短消息的内容
    private String reserve;           //保留 8字节
    
    public byte[] toByteArry(){
    	ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			dous.writeInt(this.getTotalLength());
			dous.writeInt(this.getCommandId());
			dous.write(this.getSeqByte());
			MsgUtils.writeString(dous,spNumber,21);
			MsgUtils.writeString(dous,chargeNumber,21);
			dous.writeByte(this.userCount);
			for (int i = 0; i < userNumber.length; i++) {
				MsgUtils.writeString(dous,this.userNumber[i],21);
			}
			MsgUtils.writeString(dous,this.corpId,5);
			MsgUtils.writeString(dous,this.serviceType,10);
			dous.writeByte(this.feeType);
			MsgUtils.writeString(dous,this.feeValue,6);
			MsgUtils.writeString(dous,this.givenValue,6);
			dous.writeByte(this.agentFlag);
			dous.writeByte(this.morelatetoMTFlag);
			dous.writeByte(this.priority);
			MsgUtils.writeString(dous,this.expireTime,16);
			MsgUtils.writeString(dous,this.scheduleTime,16);
			dous.writeByte(this.reportFlag);
			dous.writeByte(this.tpPid);
			dous.writeByte(this.tpUdhi);
			dous.writeByte(this.messageCoding);
			dous.writeByte(this.messageType);
			dous.writeInt(this.messageLength);
			dous.write(this.messageContent);//信息内容	
			MsgUtils.writeString(dous,this.reserve,8);
			dous.close();
		}catch (IOException e) {
			logger.error("封装短信发送二进制数组失败。");
		}
		return bous.toByteArray();
    }
    
	/**
	 * spNumber.
	 *
	 * @return  the spNumber
	 * @since   JDK 1.6
	 */
	public String getSpNumber() {
		return spNumber;
	}
	/**
	 * chargeNumber.
	 *
	 * @return  the chargeNumber
	 * @since   JDK 1.6
	 */
	public String getChargeNumber() {
		return chargeNumber;
	}
	/**
	 * userCount.
	 *
	 * @return  the userCount
	 * @since   JDK 1.6
	 */
	public byte getUserCount() {
		return userCount;
	}
	/**
	 * userNumber.
	 *
	 * @return  the userNumber
	 * @since   JDK 1.6
	 */
	public String[] getUserNumber() {
		return userNumber;
	}
	/**
	 * corpId.
	 *
	 * @return  the corpId
	 * @since   JDK 1.6
	 */
	public String getCorpId() {
		return corpId;
	}
	/**
	 * serviceType.
	 *
	 * @return  the serviceType
	 * @since   JDK 1.6
	 */
	public String getServiceType() {
		return serviceType;
	}
	/**
	 * feeType.
	 *
	 * @return  the feeType
	 * @since   JDK 1.6
	 */
	public byte getFeeType() {
		return feeType;
	}
	/**
	 * feeValue.
	 *
	 * @return  the feeValue
	 * @since   JDK 1.6
	 */
	public String getFeeValue() {
		return feeValue;
	}
	/**
	 * givenValue.
	 *
	 * @return  the givenValue
	 * @since   JDK 1.6
	 */
	public String getGivenValue() {
		return givenValue;
	}
	/**
	 * agentFlag.
	 *
	 * @return  the agentFlag
	 * @since   JDK 1.6
	 */
	public byte getAgentFlag() {
		return agentFlag;
	}
	/**
	 * morelatetoMTFlag.
	 *
	 * @return  the morelatetoMTFlag
	 * @since   JDK 1.6
	 */
	public byte getMorelatetoMTFlag() {
		return morelatetoMTFlag;
	}
	/**
	 * priority.
	 *
	 * @return  the priority
	 * @since   JDK 1.6
	 */
	public byte getPriority() {
		return priority;
	}
	/**
	 * expireTime.
	 *
	 * @return  the expireTime
	 * @since   JDK 1.6
	 */
	public String getExpireTime() {
		return expireTime;
	}
	/**
	 * scheduleTime.
	 *
	 * @return  the scheduleTime
	 * @since   JDK 1.6
	 */
	public String getScheduleTime() {
		return scheduleTime;
	}
	/**
	 * reportFlag.
	 *
	 * @return  the reportFlag
	 * @since   JDK 1.6
	 */
	public byte getReportFlag() {
		return reportFlag;
	}
	/**
	 * tpPid.
	 *
	 * @return  the tpPid
	 * @since   JDK 1.6
	 */
	public byte getTpPid() {
		return tpPid;
	}
	/**
	 * tpUdhi.
	 *
	 * @return  the tpUdhi
	 * @since   JDK 1.6
	 */
	public byte getTpUdhi() {
		return tpUdhi;
	}
	/**
	 * messageCoding.
	 *
	 * @return  the messageCoding
	 * @since   JDK 1.6
	 */
	public byte getMessageCoding() {
		return messageCoding;
	}
	/**
	 * messageType.
	 *
	 * @return  the messageType
	 * @since   JDK 1.6
	 */
	public byte getMessageType() {
		return messageType;
	}
	/**
	 * messageLength.
	 *
	 * @return  the messageLength
	 * @since   JDK 1.6
	 */
	public int getMessageLength() {
		return messageLength;
	}
	/**
	 * messageContent.
	 *
	 * @return  the messageContent
	 * @since   JDK 1.6
	 */
	public byte[] getMessageContent() {
		return messageContent;
	}
	/**
	 * reserve.
	 *
	 * @return  the reserve
	 * @since   JDK 1.6
	 */
	public String getReserve() {
		return reserve;
	}
	/**
	 * spNumber.
	 *
	 * @param   spNumber    the spNumber to set
	 * @since   JDK 1.6
	 */
	public void setSpNumber(String spNumber) {
		this.spNumber = spNumber;
	}
	/**
	 * chargeNumber.
	 *
	 * @param   chargeNumber    the chargeNumber to set
	 * @since   JDK 1.6
	 */
	public void setChargeNumber(String chargeNumber) {
		this.chargeNumber = chargeNumber;
	}
	/**
	 * userCount.
	 *
	 * @param   userCount    the userCount to set
	 * @since   JDK 1.6
	 */
	public void setUserCount(byte userCount) {
		this.userCount = userCount;
	}
	/**
	 * userNumber.
	 *
	 * @param   userNumber    the userNumber to set
	 * @since   JDK 1.6
	 */
	public void setUserNumber(String[] userNumber) {
		this.userNumber = userNumber;
	}
	/**
	 * corpId.
	 *
	 * @param   corpId    the corpId to set
	 * @since   JDK 1.6
	 */
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	/**
	 * serviceType.
	 *
	 * @param   serviceType    the serviceType to set
	 * @since   JDK 1.6
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	/**
	 * feeType.
	 *
	 * @param   feeType    the feeType to set
	 * @since   JDK 1.6
	 */
	public void setFeeType(byte feeType) {
		this.feeType = feeType;
	}
	/**
	 * feeValue.
	 *
	 * @param   feeValue    the feeValue to set
	 * @since   JDK 1.6
	 */
	public void setFeeValue(String feeValue) {
		this.feeValue = feeValue;
	}
	/**
	 * givenValue.
	 *
	 * @param   givenValue    the givenValue to set
	 * @since   JDK 1.6
	 */
	public void setGivenValue(String givenValue) {
		this.givenValue = givenValue;
	}
	/**
	 * agentFlag.
	 *
	 * @param   agentFlag    the agentFlag to set
	 * @since   JDK 1.6
	 */
	public void setAgentFlag(byte agentFlag) {
		this.agentFlag = agentFlag;
	}
	/**
	 * morelatetoMTFlag.
	 *
	 * @param   morelatetoMTFlag    the morelatetoMTFlag to set
	 * @since   JDK 1.6
	 */
	public void setMorelatetoMTFlag(byte morelatetoMTFlag) {
		this.morelatetoMTFlag = morelatetoMTFlag;
	}
	/**
	 * priority.
	 *
	 * @param   priority    the priority to set
	 * @since   JDK 1.6
	 */
	public void setPriority(byte priority) {
		this.priority = priority;
	}
	/**
	 * expireTime.
	 *
	 * @param   expireTime    the expireTime to set
	 * @since   JDK 1.6
	 */
	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}
	/**
	 * scheduleTime.
	 *
	 * @param   scheduleTime    the scheduleTime to set
	 * @since   JDK 1.6
	 */
	public void setScheduleTime(String scheduleTime) {
		this.scheduleTime = scheduleTime;
	}
	/**
	 * reportFlag.
	 *
	 * @param   reportFlag    the reportFlag to set
	 * @since   JDK 1.6
	 */
	public void setReportFlag(byte reportFlag) {
		this.reportFlag = reportFlag;
	}
	/**
	 * tpPid.
	 *
	 * @param   tpPid    the tpPid to set
	 * @since   JDK 1.6
	 */
	public void setTpPid(byte tpPid) {
		this.tpPid = tpPid;
	}
	/**
	 * tpUdhi.
	 *
	 * @param   tpUdhi    the tpUdhi to set
	 * @since   JDK 1.6
	 */
	public void setTpUdhi(byte tpUdhi) {
		this.tpUdhi = tpUdhi;
	}
	/**
	 * messageCoding.
	 *
	 * @param   messageCoding    the messageCoding to set
	 * @since   JDK 1.6
	 */
	public void setMessageCoding(byte messageCoding) {
		this.messageCoding = messageCoding;
	}
	/**
	 * messageType.
	 *
	 * @param   messageType    the messageType to set
	 * @since   JDK 1.6
	 */
	public void setMessageType(byte messageType) {
		this.messageType = messageType;
	}
	/**
	 * messageLength.
	 *
	 * @param   messageLength    the messageLength to set
	 * @since   JDK 1.6
	 */
	public void setMessageLength(int messageLength) {
		this.messageLength = messageLength;
	}
	/**
	 * messageContent.
	 *
	 * @param   messageContent    the messageContent to set
	 * @since   JDK 1.6
	 */
	public void setMessageContent(byte[] messageContent) {
		this.messageContent = messageContent;
	}
	/**
	 * reserve.
	 *
	 * @param   reserve    the reserve to set
	 * @since   JDK 1.6
	 */
	public void setReserve(String reserve) {
		this.reserve = reserve;
	}
    
}

