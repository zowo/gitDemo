/**
 * Project Name:ccopsgw
 * File Name:SgipMsgUnbindResp.java
 * Package Name:com.ccop.common.sgip.msg
 * Date:2016年4月2日上午10:11:08
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.sgip.msg;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;


/**
 * ClassName:SgipMsgUnbindResp <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月2日 上午10:11:08 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SgipMsgUnbindResp extends SgipMsgHead {
	private static Logger logger=Logger.getLogger(SgipMsgUnbindResp.class);
	public SgipMsgUnbindResp(){
		
	}
	public SgipMsgUnbindResp(int totalLength,int commandId,byte[] seqByte,byte[] data){
		if(data.length==1+8){
			this.setTotalLength(totalLength);
			this.setCommandId(commandId);
			this.setSeqByte(seqByte);
		}
	}
	public byte[] toByteArry(){
		ByteArrayOutputStream bous=new ByteArrayOutputStream();
		DataOutputStream dous=new DataOutputStream(bous);
		try {
			dous.writeInt(this.getTotalLength());
			dous.writeInt(this.getCommandId());
			dous.write(this.getSeqByte());
			
			dous.close();
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("封装短信发送二进制数组失败。");
		}
		return bous.toByteArray();
	}
}

