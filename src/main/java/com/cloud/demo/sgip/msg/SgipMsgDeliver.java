/**
 * Project Name:ccopsgw
 * File Name:SgipMsgDeliver.java
 * Package Name:com.ccop.common.sgip.msg
 * Date:2016年3月30日下午6:52:19
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.sgip.msg;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import com.cloud.demo.sgip.SgipCommand;


/**
 * ClassName:SgipMsgDeliver <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年3月30日 下午6:52:19 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SgipMsgDeliver extends SgipMsgHead {
	private String userNumber;     //发送短消息的用户手机号,手机号码必须前加"86"国别标志
	private String spNumber;       //SP的接入号
	private byte tpPid;		   //GSM协议类型
	private byte tpUdhi;	       //GSM协议类型
	private byte messageCoding;	   //短消息的编码方式
	                               //0:纯ASCII字符串
		                           //3:写卡操作
	                               //4:二进制编码
		                           //8: UCS2编码
	                               //15:GBK编码
	private int messageLength;     //短消息的长度
	private String messageContent; //短消息的内容 
	private String reserve="";	   //保留，扩展用
	private int result;//解析结果
	public SgipMsgDeliver (int totalLength,int commandId,byte[] seqByte,byte[] data){
		if(data.length>=21+21+1+1+1+4+8){//+Msg_length+
			try {
				String fmtStr="gb2312";
				ByteArrayInputStream bins=new ByteArrayInputStream(data);
				DataInputStream dins=new DataInputStream(bins);
				this.setTotalLength(totalLength);
				this.setCommandId(commandId);
				this.setSeqByte(seqByte);
				byte[] userNumberByte=new byte[21];
				dins.read(userNumberByte);
				this.userNumber=new String(userNumberByte).trim();
				byte[] spNumberByte=new byte[21];
				dins.read(spNumberByte);
				this.spNumber=new String(spNumberByte).trim();
				this.tpPid=dins.readByte();
				this.tpUdhi=dins.readByte();
				this.messageCoding=dins.readByte();
				fmtStr=this.messageCoding==8?"utf-8":"gb2312";
				this.messageLength=dins.readInt();
				byte[] messageContentByte=new byte[messageLength];
				dins.read(messageContentByte);
				
				if(messageCoding==8){
					this.messageContent=new String(messageContentByte, "UTF-16BE");
				}else{
					this.messageContent=new String(messageContentByte,fmtStr);//消息长度
				}
				this.setSeqStr(SgipCommand.getSeqStr(seqByte));
				this.result=0;//正确
			} catch (IOException e) {
				this.result=8;//消息结构错
				e.printStackTrace();
			}
		}else{
			this.result=1;//消息结构错
		}
	}
	/**
	 * userNumber.
	 *
	 * @return  the userNumber
	 * @since   JDK 1.6
	 */
	public String getUserNumber() {
		return userNumber;
	}
	/**
	 * spNumber.
	 *
	 * @return  the spNumber
	 * @since   JDK 1.6
	 */
	public String getSpNumber() {
		return spNumber;
	}
	/**
	 * tpPid.
	 *
	 * @return  the tpPid
	 * @since   JDK 1.6
	 */
	public byte getTpPid() {
		return tpPid;
	}
	/**
	 * tpUdhi.
	 *
	 * @return  the tpUdhi
	 * @since   JDK 1.6
	 */
	public byte getTpUdhi() {
		return tpUdhi;
	}
	/**
	 * messageCoding.
	 *
	 * @return  the messageCoding
	 * @since   JDK 1.6
	 */
	public byte getMessageCoding() {
		return messageCoding;
	}
	/**
	 * messageLength.
	 *
	 * @return  the messageLength
	 * @since   JDK 1.6
	 */
	public int getMessageLength() {
		return messageLength;
	}
	/**
	 * messageContent.
	 *
	 * @return  the messageContent
	 * @since   JDK 1.6
	 */
	public String getMessageContent() {
		return messageContent;
	}
	/**
	 * reserve.
	 *
	 * @return  the reserve
	 * @since   JDK 1.6
	 */
	public String getReserve() {
		return reserve;
	}
	
	/**
	 * result.
	 *
	 * @return  the result
	 * @since   JDK 1.6
	 */
	public int getResult() {
		return result;
	}
	/**
	 * result.
	 *
	 * @param   result    the result to set
	 * @since   JDK 1.6
	 */
	public void setResult(int result) {
		this.result = result;
	}
	/**
	 * userNumber.
	 *
	 * @param   userNumber    the userNumber to set
	 * @since   JDK 1.6
	 */
	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}
	/**
	 * spNumber.
	 *
	 * @param   spNumber    the spNumber to set
	 * @since   JDK 1.6
	 */
	public void setSpNumber(String spNumber) {
		this.spNumber = spNumber;
	}
	/**
	 * tpPid.
	 *
	 * @param   tpPid    the tpPid to set
	 * @since   JDK 1.6
	 */
	public void setTpPid(byte tpPid) {
		this.tpPid = tpPid;
	}
	/**
	 * tpUdhi.
	 *
	 * @param   tpUdhi    the tpUdhi to set
	 * @since   JDK 1.6
	 */
	public void setTpUdhi(byte tpUdhi) {
		this.tpUdhi = tpUdhi;
	}
	/**
	 * messageCoding.
	 *
	 * @param   messageCoding    the messageCoding to set
	 * @since   JDK 1.6
	 */
	public void setMessageCoding(byte messageCoding) {
		this.messageCoding = messageCoding;
	}
	/**
	 * messageLength.
	 *
	 * @param   messageLength    the messageLength to set
	 * @since   JDK 1.6
	 */
	public void setMessageLength(int messageLength) {
		this.messageLength = messageLength;
	}
	/**
	 * messageContent.
	 *
	 * @param   messageContent    the messageContent to set
	 * @since   JDK 1.6
	 */
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}
	/**
	 * reserve.
	 *
	 * @param   reserve    the reserve to set
	 * @since   JDK 1.6
	 */
	public void setReserve(String reserve) {
		this.reserve = reserve;
	}
	
}

