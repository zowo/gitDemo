/**
 * Project Name:ccopsms
 * File Name:BaseRedisDaoImpl.java
 * Package Name:com.ccop.core.dao.impl
 * Date:2015年11月10日下午6:48:39
 * Copyright (c) 2015, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.redis;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * ClassName:BaseRedisDaoImpl <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON. <br/>
 * Date: 2015年11月10日 下午6:48:39 <br/>
 * 
 * @author LiHao
 * @version
 * @since JDK 1.6
 * @see
 */
public class BaseRedisDaoImpl<K extends Serializable, V extends Serializable> {
	public final Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	protected RedisTemplate<K, V> redisTemplate;

	public void setRedisTemplate(RedisTemplate<K, V> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	protected RedisSerializer<String> getRedisSerializer() {
		return redisTemplate.getStringSerializer();
	}
}
