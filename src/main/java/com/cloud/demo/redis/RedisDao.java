package com.cloud.demo.redis;

import java.util.Map;

/**
 * ClassName: RedisDao <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason: TODO ADD REASON(可选). <br/>
 * date: 2016年3月16日 下午2:39:13 <br/>
 *
 * @author LiHao
 * @version 
 * @since JDK 1.6
 */
public interface RedisDao {
	
	/**
	 * existsKey:判断一个key是否存在<br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br/>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br/>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br/>
	 *
	 * @author LiHao
	 * @param keyId
	 * @return
	 * @since JDK 1.6
	 */
	public boolean existsKey(String keyId);
	
	/**
	 * set:设置单个值 <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br/>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br/>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br/>
	 *
	 * @author LiHao
	 * @param msgId
	 * @param seqId
	 * @return
	 * @since JDK 1.6
	 */
	public boolean set(String key,String value);
	
	public boolean setMap(String key,Map<String,String> map);
	public boolean setMapExpire(String key,Map<String,String> map,long seconds);
	public Map<String, String> getMap(String key);
	/**
	 * setExpire:设置单个值 并设置有效时间. <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br/>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br/>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br/>
	 *
	 * @author LiHao
	 * @param key
	 * @param value
	 * @param seconds 秒
	 * @return
	 * @since JDK 1.6
	 */
	public boolean setExpire(String key,String value,long seconds);
	
	/**
	 * get:获取单个值. <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br/>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br/>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br/>
	 *
	 * @author LiHao
	 * @param key
	 * @return
	 * @since JDK 1.6
	 */
	public String get(String key);
	
	/**
	 * getDel:获取单个值并删除 <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br/>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br/>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br/>
	 *
	 * @author LiHao
	 * @param key
	 * @return
	 * @since JDK 1.6
	 */
	public String getDel(String key);
	
	/**
	 * incr:自增计数 <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * TODO(这里描述这个方法的执行流程 – 可选).<br/>
	 * TODO(这里描述这个方法的使用方法 – 可选).<br/>
	 * TODO(这里描述这个方法的注意事项 – 可选).<br/>
	 *
	 * @author LiHao
	 * @param key
	 * @return
	 * @since JDK 1.6
	 */
	public Long incr(String key);
	
	public Long incrRes(String key,long chkNum, String resVal);
}

