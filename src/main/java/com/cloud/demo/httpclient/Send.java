/**
 * Project Name:demo
 * File Name:Send.java
 * Package Name:com.cloud.demo.httpclient
 * Date:2016年4月26日下午3:12:59
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.httpclient;

import java.util.Date;

import com.cloud.demo.bean.TestSendMsgReq;
import com.cloud.demo.utils.DateUtils;
import com.cloud.demo.utils.Encrypt;
import com.cloud.demo.utils.JacksonUtil;

/**
 * ClassName:Send <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月26日 下午3:12:59 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class Send {
	public static void testQD() {
		TestSendMsgReq req =new TestSendMsgReq();
		req.setOutId("outId");
		req.setTel("18510543438");
//		req.setTel("13520007311");
		req.setMsg("[杏仁医生]青岛测试");
//		req.setMsg("浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送");
		req.setAppendCode("1120");
		req.setSignature("【测试】");
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		req.setTimestamp(timestamp);
		//'cmpp'+key+secret+timestamp
		String encrypted="cmpp"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		req.setEncrypted(Encrypt.encryptMD5(encrypted));
//		req.setSignature("【智慧树】");
		String rs=JacksonUtil.writeValueAsString(req);
//		HttpClient.postDataJson("http://127.0.0.1:7080/ccopsgw/sgw/sgipqd/sendMsg", rs,"sgipqd");
//		http://10.47.134.139/sgw/cmppzj/sendMsg
	}
	public static void testZj() {
		TestSendMsgReq req =new TestSendMsgReq();
		req.setOutId("outId");
		req.setTel("13520007311");
		req.setMsg("浙江台州测试推送请求发送浙江台");
//		req.setMsg("浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送");
		req.setAppendCode("1198");
		req.setSignature("【测试】");
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		req.setTimestamp(timestamp);
		//'cmpp'+key+secret+timestamp
		String encrypted="cmpp"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		req.setEncrypted(Encrypt.encryptMD5(encrypted));
//		req.setSignature("【智慧树】");
		String rs=JacksonUtil.writeValueAsString(req);
//		HttpClient.postDataJson("http://127.0.0.1:7080/ccopsgw/sgw/cmppzj/sendMsg", rs,"cmppzj");
//		http://10.47.134.139/sgw/cmppzj/sendMsg
	}
	public static void testSh() {
		TestSendMsgReq req =new TestSendMsgReq();
		req.setOutId("outId");
		req.setTel("13520007311");
		req.setMsg("[杏仁医生]上海移动测试");
//		req.setMsg("上海移动测试试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试上海移动测试推送请求发送");
		req.setAppendCode("1120");
		req.setSignature("【测试】");
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		req.setTimestamp(timestamp);
		//'cmpp'+key+secret+timestamp
		String encrypted="cmpp"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		req.setEncrypted(Encrypt.encryptMD5(encrypted));
//		req.setSignature("【智慧树】");
		String rs=JacksonUtil.writeValueAsString(req);
//		HttpClient.postDataJson("http://127.0.0.1:7080/ccopsgw/sgw/cmpp/sendMsg", rs,"cmppzj");
//		http://10.47.134.139/sgw/cmppzj/sendMsg
	}
	public static void testjshb() {
		TestSendMsgReq req =new TestSendMsgReq();
		req.setOutId("outId");
		req.setTel("13391823846");
		req.setMsg("江苏号百测试");
//		req.setMsg("江苏号百测试送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试江苏号百测试推送请求发送");
		req.setAppendCode("2055");
		req.setSignature("【测试】");
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		req.setTimestamp(timestamp);
		//'cmpp'+key+secret+timestamp
		String encrypted="cmpp"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		req.setEncrypted(Encrypt.encryptMD5(encrypted));
//		req.setSignature("【智慧树】");
		String rs=JacksonUtil.writeValueAsString(req);
//		HttpClient.postDataJson("http://127.0.0.1:7080/ccopsgw/sgw/smgp/sendMsg", rs,"cmppzj");
//		http://10.47.134.139/sgw/cmppzj/sendMsg
	}
	public static void testBjdx() {
		TestSendMsgReq req =new TestSendMsgReq();
		req.setOutId("outId");
		req.setTel("13391823846");
		req.setMsg("[杏仁医生]北京电信测试2");
//		req.setMsg("北京电信测试送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试江苏号百测试推送请求发送");
		req.setAppendCode("1120");
		req.setSignature("【测试】");
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		req.setTimestamp(timestamp);
		//'cmpp'+key+secret+timestamp
		String encrypted="cmpp"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		req.setEncrypted(Encrypt.encryptMD5(encrypted));
//		req.setSignature("【智慧树】");
		String rs=JacksonUtil.writeValueAsString(req);
//		HttpClient.postDataJson("http://127.0.0.1:7080/ccopsgw/sgw/smgpbj/sendMsg", rs,"cmppzj");
//		http://10.47.134.139/sgw/cmppzj/sendMsg
	}
	public static void testSmpp(String tel){
		TestSendMsgReq req =new TestSendMsgReq();
		req.setOutId("outId");
		req.setTel(tel);
		req.setMsg("Smpp测试测试测试送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送，浙江台州测试推送请求发送浙江台州测试江苏号百测试推送请求结束【你好】");
		req.setMsg("Smpp测试测试测试送请求发送浙江台州测试推送请求发【哈哈哈哈】");
		//req.setMsg("Smpp测试测试送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送，浙江台州测试推送请求发送浙江台州测试江苏号百测试推送请求结束【你好】");
		//req.setMsg("Smpp测试测试送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送，浙江台州测试推送请求发送浙江台州测试江苏号百测试推送请求结束【销售易】");
		//req.setMsg("Smpp测试测试测试送请求发送浙江台州测试推【流量充值】Owner:[Non-Prod][RMA]送请你好【流量充值】"); // 流量充值  【流量充值】
		//req.setMsg("Hi everyone, This article for the test message!");
		//req.setMsg("TEST3 TO EVYI - ils:ILS: SR Close,680802532,P3,Owner:[Non-Prod][RMA] -1/3[GSMS]");
		//req.setMsg("Smpp测试测试测试送请求发送浙江台州测试推送请【CN】");//CN
	//	req.setMsg("Smpp测试测试测试送请求发送浙江台州测试推送请");//【TLSG】
	//	req.setMsg("Smpp测试测试测试送请求发送浙江台州测试推送请【流量充值】");
	//	req.setMsg("[CtripTest][424]");
		//req.setMsg("您好,[company name] 推出了一个名为 合协 的新沟通系统来回应您的问题和投诉。您可以轻松地向此号码发送短信。【流量充值】");
		req.setAppendCode("319937");// 305510  308647 313475 1234
		req.setSignature("【你好】");
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		req.setTimestamp(timestamp);
		//'cmpp'+key+secret+timestamp
		String encrypted="cmpp"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		req.setEncrypted(Encrypt.encryptMD5(encrypted));
//		req.setSignature("【智慧树】");
		String rs=JacksonUtil.writeValueAsString(req);
		HttpClient.postDataJson("http://127.0.0.1:8082/sgw/smpp/sendMsg", rs,"smpp");
//		http://10.47.134.139/sgw/cmppzj/sendMsg
	}

	public static void testCmpp(){
		HttpClient.postDataJson("http://127.0.0.1:8082/sgw/cmpp/sendMsg",null,"cmpp");
	}
	public static void testCmpp2(){
		for(int i=0;i<20;i++){
			HttpClient.postDataJson("http://127.0.0.1:8000/sgw/cmpp/sendMsg2",null,"cmpp");
		}

	}

	public static void main(String[] args) {
//testQD();
//		testBjdx();
//		testSh();
//		testSmpp("18601322881");//13810101372 18686995297　　　18910715685
//		testCmpp();

		testCmpp2();


//		testjshb();
	}

}

