/**
 * Project Name:demo
 * File Name:Close.java
 * Package Name:com.cloud.demo.httpclient
 * Date:2016年4月26日下午2:56:37
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.httpclient;

import java.util.Date;

import com.cloud.demo.utils.DateUtils;
import com.cloud.demo.utils.Encrypt;

/**
 * ClassName:Close <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月26日 下午2:56:37 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class Close {
	public static void main(String[] args) {
		testBjdx();
	}
	public static void testZj(){
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		String str="socket"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		String encrypt=Encrypt.encryptMD5(str);
		String url="http://127.0.0.1:7080/ccopsgw/admin/socketClose?qType=cmppZjyd&timestamp="+timestamp+"&encrypt="+encrypt;
		HttpClient.postDataJson(url, "", "close");
	}
	public static void testSh(){
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		String str="socket"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		String encrypt=Encrypt.encryptMD5(str);
		String url="http://127.0.0.1:7080/ccopsgw/admin/socketClose?qType=cmppShyd&timestamp="+timestamp+"&encrypt="+encrypt;
		HttpClient.postDataJson(url, "", "close");
	}
	public static void testjsHb(){
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		String str="socket"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		String encrypt=Encrypt.encryptMD5(str);
		String url="http://127.0.0.1:7080/ccopsgw/admin/socketClose?qType=smgpJshb&timestamp="+timestamp+"&encrypt="+encrypt;
		HttpClient.postDataJson(url, "", "close");
	}
	public static void testBjdx(){
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		String str="socket"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		String encrypt=Encrypt.encryptMD5(str);
		String url="http://127.0.0.1:7080/ccopsgw/admin/socketClose?qType=smgpBjdx&timestamp="+timestamp+"&encrypt="+encrypt;
		HttpClient.postDataJson(url, "", "close");
	}
}

