/**
 * Project Name:demo
 * File Name:Send.java
 * Package Name:com.cloud.demo.httpclient
 * Date:2016年4月26日下午3:12:59
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.httpclient;

import java.util.Date;

import com.cloud.demo.bean.TestSendMsgReq;
import com.cloud.demo.utils.DateUtils;
import com.cloud.demo.utils.Encrypt;
import com.cloud.demo.utils.JacksonUtil;

/**
 * ClassName:Send <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月26日 下午3:12:59 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class Send1 {
	public static void testQD() {
		TestSendMsgReq req =new TestSendMsgReq();
		req.setOutId("outId");
		req.setTel("13520007311");
		req.setMsg("青岛测试");
//		req.setMsg("浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送");
		req.setAppendCode("1198");
		req.setSignature("【测试】");
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		req.setTimestamp(timestamp);
		//'cmpp'+key+secret+timestamp
		String encrypted="cmpp"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		req.setEncrypted(Encrypt.encryptMD5(encrypted));
//		req.setSignature("【智慧树】");
		String rs=JacksonUtil.writeValueAsString(req);
		HttpClient.postDataJson("http://127.0.0.1:7080/ccopsgw/sgw/sgipqd/sendMsg", rs,"sgipqd");
//		HttpClient.postDataJson("http://114.215.237.36:9080/sgw/cmppzj/sendMsg", rs,"cmppzj");
//		http://10.47.134.139/sgw/cmppzj/sendMsg
	}
	public static void testZj() {
		TestSendMsgReq req =new TestSendMsgReq();
		req.setOutId("outId");
		req.setTel("13520007311");
		req.setMsg("浙江台州测试推送请求发送浙江台");
//		req.setMsg("浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送");
		req.setAppendCode("1198");
		req.setSignature("【测试】");
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		req.setTimestamp(timestamp);
		//'cmpp'+key+secret+timestamp
		String encrypted="cmpp"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		req.setEncrypted(Encrypt.encryptMD5(encrypted));
//		req.setSignature("【智慧树】");
		String rs=JacksonUtil.writeValueAsString(req);
//		HttpClient.postDataJson("http://127.0.0.1:7080/ccopsgw/sgw/cmppzj/sendMsg", rs,"cmppzj");
		HttpClient.postDataJson("http://114.215.237.36:9080/sgw/cmppzj/sendMsg", rs,"cmppzj");
//		http://10.47.134.139/sgw/cmppzj/sendMsg
	}
	public static void testSh() {
		TestSendMsgReq req =new TestSendMsgReq();
		req.setOutId("outId");
		req.setTel("13520007311");
		req.setMsg("上海移动测试");
//		req.setMsg("上海移动测试试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试上海移动测试推送请求发送");
		req.setAppendCode("2055");
		req.setSignature("【测试】");
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		req.setTimestamp(timestamp);
		//'cmpp'+key+secret+timestamp
		String encrypted="cmpp"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		req.setEncrypted(Encrypt.encryptMD5(encrypted));
//		req.setSignature("【智慧树】");
		String rs=JacksonUtil.writeValueAsString(req);
//		HttpClient.postDataJson("http://127.0.0.1:7080/ccopsgw/sgw/cmpp/sendMsg", rs,"cmppzj");
		HttpClient.postDataJson("http://120.55.112.241:9080/sgw/cmpp/sendMsg", rs,"cmppsh");
//		http://10.47.134.139/sgw/cmppzj/sendMsg
	}
	public static void testjshb() {
		TestSendMsgReq req =new TestSendMsgReq();
		req.setOutId("outId");
		req.setTel("13391823846");
//		req.setMsg("江苏号百测试");
		req.setMsg("江苏号百测试送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试江苏号百测试推送请求发送");
		req.setAppendCode("2055");
		req.setSignature("【测试】");
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		req.setTimestamp(timestamp);
		//'cmpp'+key+secret+timestamp
		String encrypted="cmpp"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		req.setEncrypted(Encrypt.encryptMD5(encrypted));
//		req.setSignature("【智慧树】");
		String rs=JacksonUtil.writeValueAsString(req);
//		HttpClient.postDataJson("http://127.0.0.1:7080/ccopsgw/sgw/smgp/sendMsg", rs,"cmppzj");
//		HttpClient.postDataJson("http://114.215.237.36:9080/sgw/cmppzj/sendMsg", rs,"cmppzj");
		HttpClient.postDataJson("http://120.55.112.241:9080/sgw/smgp/sendMsg", rs,"jshb");
//		http://10.47.134.139/sgw/cmppzj/sendMsg
	}
	public static void testBjdx() {
		TestSendMsgReq req =new TestSendMsgReq();
		req.setOutId("outId");
		req.setTel("13391823846");
		req.setMsg("北京电信测试");
//		req.setMsg("北京电信测试送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试江苏号百测试推送请求发送");
		req.setAppendCode("2055");
		req.setSignature("【测试】");
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		req.setTimestamp(timestamp);
		//'cmpp'+key+secret+timestamp
		String encrypted="cmpp"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		req.setEncrypted(Encrypt.encryptMD5(encrypted));
//		req.setSignature("【智慧树】");
		String rs=JacksonUtil.writeValueAsString(req);
//		HttpClient.postDataJson("http://127.0.0.1:7080/ccopsgw/sgw/smgpbj/sendMsg", rs,"cmppzj");
		HttpClient.postDataJson("http://112.124.118.251:9080/sgw/smgpbj/sendMsg", rs,"bjdx");
//		HttpClient.postDataJson("http://120.55.181.172:9080/sgw/smgpbj/sendMsg", rs,"jshb");
//		http://10.47.134.139/sgw/cmppzj/sendMsg
	}
	public static void testSmpp(){
		long start=System.currentTimeMillis();
		TestSendMsgReq req =new TestSendMsgReq();
		req.setOutId("outId");
		req.setTel("18601322881");//18601322881
		req.setMsg("Marie, 52362 is your Facebook confirmation code 【云喇叭】");
		//req.setMsg("Smpp测试测试送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送浙江台州测试推送请求发送，浙江台州测试推送请求发送浙江台州测试江苏号百测试台州测试江苏号百测推送浙江台州测试江苏号百测试推送请求结束【云喇叭】");
		req.setAppendCode("300006");//304015
		//req.setSignature("[CN]");
		String timestamp=DateUtils.formatMdhmsDate(new Date());
		req.setTimestamp(timestamp);
		//'cmpp'+key+secret+timestamp
		String encrypted="cmpp"+"cloopen_Cmpp"+"cmpp_ytx_321"+timestamp;
		req.setEncrypted(Encrypt.encryptMD5(encrypted));
//		req.setSignature("【智慧树】");
		String rs=JacksonUtil.writeValueAsString(req);
//		HttpClient.postDataJson("http://127.0.0.1:7080/ccopsgw/sgw/smgpbj/sendMsg", rs,"cmppzj");
//		HttpClient.postDataJson("http://114.215.237.36:9080/sgw/cmppzj/sendMsg", rs,"cmppzj");
		HttpClient.postDataJson("http://127.0.0.1:80/demo/sgw/smpp/sendMsg", rs,"smpp");
//		http://10.47.134.139/sgw/cmppzj/sendMsg
		long end=System.currentTimeMillis();
		System.out.println("total spend time:"+(end-start));
	}
	
	public static void main(String[] args) {
//testQD();
//		testBjdx();
//		testSh();
		testSmpp();
	}

}

