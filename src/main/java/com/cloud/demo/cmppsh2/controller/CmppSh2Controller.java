package com.cloud.demo.cmppsh2.controller; /**
 * Project Name:ccopsgw
 * File Name:CmppController.java
 * Package Name:com.ccop.sgw.controller
 * Date:2016年2月19日下午1:33:57
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/



import com.cloud.demo.bean.SendMsgReq;
import com.cloud.demo.bean.SendMsgResp;
import com.cloud.demo.cmppsh2.socket.CmppSh2Container;
import com.cloud.demo.utils.JacksonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * ClassName:CmppController <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年2月19日 下午1:33:57 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
@Controller
@RequestMapping("/sgw")
public class CmppSh2Controller {
	public final Logger logger = LoggerFactory.getLogger(CmppSh2Controller.class);
	@RequestMapping("/cmpp/sendMsg2")
	@ResponseBody
	//@RequestBody
	public String sendMsg( SendMsgReq sendMsgReq) throws Exception{
		sendMsgReq =new SendMsgReq();
		String req=JacksonUtil.writeValueAsString(sendMsgReq);
		logger.info("【cmpp接收到短信发送请求】:"+req);
		int result=-3;
		String msgId=null;
		//if(StringUtils.isNotBlank(sendMsgReq.getMsg())&&StringUtils.isNotBlank(sendMsgReq.getTel())&&StringUtils.isNotBlank(sendMsgReq.getAppendCode())
		//		&&StringUtils.isNotBlank(sendMsgReq.getTimestamp())){
			//if(StringUtils.isNotBlank(sendMsgReq.getEncrypted())&&encryptStr.equals(sendMsgReq.getEncrypted())){
//				String [] telArr=sendMsgReq.getTel().split(","); 
//				MsgSubmitResp resp=CmppContainer.sendMsg(sendMsgReq.getMsg(), telArr,sendMsgReq.getAppendCode());
//				MsgSubmitResp resp=CmppShContainer.sendMsg(sendMsgReq);
//				if(resp!=null){
//					result=resp.getResult();
//					msgId=resp.getMsgId()+"";
//				}else{
//					result=-5;
//				}
				/*String sign_white=ReadConfigation.getConfigItem("sign_white");
				String[] signArray=sign_white.split(",");
				boolean inFlag=Utils.isIn(sendMsgReq.getSignature(), signArray);
				boolean checkFlag=true;
				if(!inFlag){
					checkFlag=Utils.checkContent(sendMsgReq.getMsg());
				}*///容联云通讯2
				sendMsgReq.setMsg("【容联云通讯2】快点来贷款啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊你好成功未来嗯嗯嗯!");
		sendMsgReq.setMsg("【哈哈哈哈】快点来贷款啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊你好成功未来嗯嗯嗯!");
				//sendMsgReq.setMsg("【云融天下】快点来贷款啊啊啊啊啊啊啊");
				//sendMsgReq.setMsg("【容联云通讯2】快点来贷款啊啊啊啊啊啊");
				sendMsgReq.setMsg("【贺家庄大洪拳】快点来贷款啊啊啊啊啊啊");
				//sendMsgReq.setMsg("【金票通】测试");
				sendMsgReq.setAppendCode("123456319937");//300009 308810
				sendMsgReq.setAppendCode("106904476120016");
				sendMsgReq.setAppendCode("300002");
				sendMsgReq.setSignature("证大速贷");
				sendMsgReq.setSignature("贺家庄大洪拳");
				sendMsgReq.setTel("18601322881");//18601322881 1234567890
				sendMsgReq.setOutId("1234567a");
		//for (int i=0;i<300000;i++) {
			if(true){
                Map<String, String> map= CmppSh2Container.putSendMsg(sendMsgReq);
                if(map!=null){
                    msgId=map.get("respId");
                    result=Integer.parseInt(map.get("result"));
                }else{
                    result=-6;
                }
            }else{
                logger.warn("【cmpp 检测短信含有http】"+req);
                result=-7;
            }
			/*}else{
				logger.warn("【cmpp 接收到短信发送验证失败】");
				result=-4;
			}*/
			//}

		//}
		SendMsgResp sendMsgResp =new SendMsgResp();
		sendMsgResp.setResult(result);
		sendMsgResp.setMsgId(msgId);
		String rs=JacksonUtil.writeValueAsString(sendMsgResp);
		logger.info("【cmpp回响应】tel:{},rs:{}",new Object[]{sendMsgReq.getTel(),rs});
		return rs;
	}
}

