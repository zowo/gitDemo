/**
 * Project Name:demo
 * File Name:ScheduleJob.java
 * Package Name:com.cloud.demo.quartz
 * Date:2016年3月9日下午6:36:46
 * Copyright (c) 2016, LiHao All Rights Reserved.
 *
*/

package com.cloud.demo.quartz;
/**
 * ClassName:ScheduleJob <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年3月9日 下午6:36:46 <br/>
 * @author   LiHao
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class ScheduleJob {
	/** 任务id */
    private String jobId;
    /** 任务名称 */
    private String jobName;
    /** 任务分组 */
    private String jobGroup;
    /** 任务状态 0禁用 1启用 2删除*/
    private String jobStatus;
    /** 任务运行时间表达式 */
    private String cronExpression;
    /** 任务描述 */
    private String desc;
    
}

